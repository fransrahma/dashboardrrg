<?php
	Class M_Nasabah3Pilar extends CI_Model {

	
	   	function getNasabah3Pilar($regional="",$area="")
	   	{
			$this->db->select('V_Nasabah3Pilar.*, t_reviewhistory.Prospek_value,t_reviewhistory.Kinerja_value,t_reviewhistory.KemampuanBayar_value');
			$this->db->from('V_Nasabah3Pilar');    
			$this->db->join('(select DISTINCT loanid,Prospek_value,Kinerja_value,KemampuanBayar_value from RetailRiskGroup.dbo.t_reviewhistory) t_reviewhistory','t_reviewhistory.loanid = v_nasabah3pilar.NoLoan','left');   
			
			if( isset($regional) || isset($area) ){
				$this->db->where("V_Nasabah3Pilar.area = '".$area."' and V_Nasabah3Pilar.regional = '".$regional."'");
			}
			
			$Data = $this->db->get();
			
			if($Data->num_rows() > 0)
			{
				return $Data->result();
			}
			else 
			{
			return array();
			}
		}

		function ReviewNasabah3Pilar($noloan)
	   	{
			$this->db->select('*');
			$this->db->from('V_Nasabah3Pilar');  
			$this->db->where('NoLoan', $noloan);     
			$Data = $this->db->get();
			
			if($Data->num_rows() > 0)
			{
				return $Data->row();
			}
			else 
			{
			return array();
			}
		}

		function GetReviewRecord($noloan){
			$this->db->select('*');
			$this->db->from('t_reviewhistory');  
			$this->db->where('loanid', $noloan);     
			$Data = $this->db->get();
			
			if($Data->num_rows() > 0)
			{
				return $Data->row();
			}
			else 
			{
			return array();
			}			
		}

		private function get_factorpoint2values($id){
			$sql = "select * from t_factorpoint2values where id = '".$id."'";
			$query = $this->db->query($sql);
			
			return $query->row();
		}

		private function get_reviewnotes($key,$loanid){
			$sql = "select * from t_reviewnotes where [key] = '".$key."' and loanid = '".$loanid."'";
			$query = $this->db->query($sql);

			if($query->num_rows()>0) return $query->row();
			else{
				return (object)[
					'notes' => 'Tidak ada catatan'
				];
			}
		}


		/*private function get_factorpoint2values($id){
			$sql = "select * from t_factorpoint2values where id = '".$id."'";
			$query = $this->db->query($sql);
			return $query->row();
		}
		*/

		public function get_scheme2factorpoint($id){
			$sql = "select * from t_scheme2factorpoint where id = '".$id."'";
			$query = $this->db->query($sql);
			return $query->row();
		}

		function getKolValue($noloan){

			$this->db->select('*');
			$this->db->from('t_reviewhistory');  
			$this->db->where('loanid', $noloan);     
			$result = $this->db->get();
			$Data = $result->row();

			$prospek = explode('#',$Data->Prospek_value);
			$kinerja = explode('#',$Data->Kinerja_value);
			$bayar = explode('#',$Data->KemampuanBayar_value);

			$result = get_kol_summary($prospek,$kinerja,$bayar);

			return $result;

		}

		function GetReviewSummary($noloan){

			$this->db->select('*');
			$this->db->from('t_reviewhistory');  
			$this->db->where('loanid', $noloan);     
			$result = $this->db->get();
			$Data = $result->row();

			$prospek_arr = explode('#',$Data->Prospek_value);
			$kinerja_arr = explode('#',$Data->Kinerja_value);
			$kbayar_arr = explode('#',$Data->KemampuanBayar_value);

			$prospek = array();
			$kinerja = array();
			$bayar = array();
			$ctrl = $this;

			array_walk($prospek_arr,function($value,$key) use(&$prospek,&$ctrl,$noloan){
				$temp =explode("_",$value);
				//print_r($temp);
				$factorid = $temp[1];
				$nilai = $temp[0];
				$label_nilai = $ctrl->get_factorpoint2values($temp[2])->label; 
				$notes = $ctrl->get_reviewnotes($value,$noloan)->notes;
				$prospek[$factorid] = array(
					'value' => $nilai,
					'label' => $label_nilai,
					'notes' => $notes
				);
			});


			array_walk($kinerja_arr,function($value,$key) use(&$kinerja,&$ctrl,$noloan){
				$temp =explode("_",$value);
				$factorid = $temp[1];
				$nilai = $temp[0];
				$label_nilai = $ctrl->get_factorpoint2values($temp[2])->label; 
				$notes = $ctrl->get_reviewnotes($value,$noloan)->notes;
				$kinerja[$factorid] = array(
					'value' => $nilai,
					'label' => $label_nilai,
					'notes' => $notes
				);
			});

			array_walk($kbayar_arr,function($value,$key) use(&$bayar,&$ctrl,$noloan){
				$temp =explode("_",$value);
				$factorid = $temp[1];
				$nilai = $temp[0];
				$label_nilai = $ctrl->get_factorpoint2values($temp[2])->label; 
				$notes = $ctrl->get_reviewnotes($value,$noloan)->notes;
				$bayar[$factorid] = array(
					'value' => $nilai,
					'label' => $label_nilai,
					'notes' => $notes
				);
			});


			return compact('prospek','kinerja','bayar');

		}
			
}