<?php

class M_OjkConfiguration extends CI_Model{
    
    public function __construct(){
        parent::__construct();
        $this->load->helper(array('arraygroup_helper'));  
    }

    public function get_value_labels(){
        $this->db->from('t_ojk_labels2value');
        $this->db->order_by('id','asc');
        $query = $this->db->get();
        return $query->result();
    }

    private function get_scheme($jenispiutang){
        $sql = "select scheme_id,scheme_name from dbo.t_ojk_scheme where LOWER(scheme_to_jenispiutang) like LOWER('%".$jenispiutang."%')";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function get_factor_scheme($scheme, $factor_id){
        $schObj = $this->get_scheme($scheme);
        $this->db->from('t_scheme2factorpoint');
        $this->db->where('scheme_id',$schObj->scheme_id);
        $this->db->where('factor_id',$factor_id);
        $this->db->order_by('id','asc');
        $query = $this->db->get();
        return $query->result();        
    }

    private function get_factorpoint_values($factorpoint_id){
        $this->db->from('t_factorpoint2values');
        $this->db->where('factorpoint_id',$factorpoint_id);
        $this->db->order_by('id','asc');
        $this->db->order_by('idx','asc');
        $query = $this->db->get();
        return $query->result_array();        
    }

    public function get_assesment_values($scheme, $factor_id){
        $values = $this->get_factor_scheme($scheme,$factor_id);
        
        $optionslabel = "options"; 
        foreach ($values as $key => &$value) {
            $options = $this->get_factorpoint_values($value->id);
            $grouped = array_group_by($options,"value");
            $value->$optionslabel = $grouped;
        }

        return $values;
    }

    public function insert_review($no_loan, $prospek,$kinerja,$kemampuanbayar){
        
        $data = array(
            'LoanId' => $no_loan,
            'Prospek_value' => $prospek,
            'Kinerja_value' => $kinerja,
            'KemampuanBayar_value' => $kemampuanbayar
        );
    
        return $this->db->insert('t_reviewhistory', $data);
    }

    public function insert_reviewnotes($loanid,$prospek,$kinerja,$kemampuanbayar){
    
        $this->db->trans_start();
        

        foreach ($prospek as $key => $value) {

            $data = array(
                'LoanId' => $loanid,
                'key' => $value['key'],
                'notes' => $value['notes']
            );

            $this->db->insert('t_reviewnotes', $data);
        }

        foreach ($kinerja as $key => $value) {

            $data = array(
                'LoanId' => $loanid,
                'key' => $value['key'],
                'notes' => $value['notes']
            );

            $this->db->insert('t_reviewnotes', $data);
        }

        foreach ($kemampuanbayar as $key => $value) {

            $data = array(
                'LoanId' => $loanid,
                'key' => $value['key'],
                'notes' => $value['notes']
            );

            $this->db->insert('t_reviewnotes', $data);
        }

        $this->db->trans_complete();


    }

}