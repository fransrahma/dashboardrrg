<?php
	Class M_data extends CI_Model {
		
		function GrafikPorto(){
			$this->db->select('*');
            $this->db->from('V_GrafikPorto');       
            $this->db->order_by('FicMisDate','asc');
			$q = $this->db->get();
			return $q;
			//$sp="SP_GrafikPorto";  
			//$q = $this->db->query($sp);
			//return $q;
	    }
	    function GrafikPortoWholesale(){
			$this->db->select('*');
            $this->db->from('V_GrafikPortoWholesale');       
            $this->db->order_by('FicMisDate','asc');
			$q = $this->db->get();
			return $q;
	    }
	    function GrafikPortoRetail(){
			$this->db->select('*');
            $this->db->from('V_GrafikPortoRetail');       
            $this->db->order_by('FicMisDate','asc');
			$q = $this->db->get();
			return $q;
	    }
	    function GrafikPortoRO1(){
			$this->db->select('*');
            $this->db->from('V_GrafikPortoRegional');   
            $this->db->where('Region','Sumatra 1 (Medan)');    
            $this->db->order_by('FicMisDate','asc');
			$q = $this->db->get();
			return $q;
	    }
	    function GrafikPortoRO2(){
			$this->db->select('*');
            $this->db->from('V_GrafikPortoRegional');   
            $this->db->where('Region','Sumatra 2 (Palembang)');   
            $this->db->order_by('FicMisDate','asc');
			$q = $this->db->get();
			return $q;
	    }
	    function GrafikPortoRO3(){
			$this->db->select('*');
            $this->db->from('V_GrafikPortoRegional');   
            $this->db->where('Region','Jakarta');   
            $this->db->order_by('FicMisDate','asc');
			$q = $this->db->get();
			return $q;
	    }
	    function GrafikPortoRO4(){
			$this->db->select('*');
            $this->db->from('V_GrafikPortoRegional');   
            $this->db->where('Region','Jawa 1 (Bandung)');    
            $this->db->order_by('FicMisDate','asc');
			$q = $this->db->get();
			return $q;
	    }
	    function GrafikPortoRO5(){
			$this->db->select('*');
            $this->db->from('V_GrafikPortoRegional');   
            $this->db->where('Region','Jawa 2 (Surabaya)');    
            $this->db->order_by('FicMisDate','asc');
			$q = $this->db->get();
			return $q;
	    }
	    function GrafikPortoRO6(){
			$this->db->select('*');
            $this->db->from('V_GrafikPortoRegional');   
            $this->db->where('Region','Kalimantan (Banjarmasin)');    
            $this->db->order_by('FicMisDate','asc');
			$q = $this->db->get();
			return $q;
	    }
	    function GrafikPortoRO7(){
			$this->db->select('*');
            $this->db->from('V_GrafikPortoRegional');   
            $this->db->where('Region','Indonesia Timur (Makassar)');
            $this->db->order_by('FicMisDate','asc');
			$q = $this->db->get();
			return $q;
	    }
	    function Portobulanan(){
			$this->db->select('*');
            $this->db->from('V_PortoBulanan');       
			$q = $this->db->get();
			return $q;
	    }function Portoharian(){
			$this->db->select('*');
            $this->db->from('V_PortoHarian');       
			$q = $this->db->get();
			return $q;
	    }
	    Function TargetSegmen(){
			$this->db->order_by('no','asc');
			$q = $this->db->get('V_TargetSegmen');
			return $q;
		}
		Function TargetCMGRegional(){
			$this->db->order_by('no','asc');
			$this->db->where('segmen','comercial');
			$q = $this->db->get('V_TargetSegmenRegional');
			return $q;
		}
		Function TargetRetailRegional(){
			$this->db->order_by('no','asc');
			$q = $this->db->get('V_TargetRegionalRetail');
			return $q;
		}
		Function TargetBBGRegional(){
			$this->db->order_by('no','asc');
			$this->db->where('segmen','Business Banking');
			$q = $this->db->get('V_TargetSegmenRegional');
			return $q;
		}
		Function TargetMBGRegional(){
			$this->db->order_by('no','asc');
			$this->db->where('segmen','Mikro Banking');
			$q = $this->db->get('V_TargetSegmenRegional');
			return $q;
		}
		Function TargetPWGRegional(){
			$this->db->order_by('no','asc');
			$this->db->where('segmen','Pawning');
			$q = $this->db->get('V_TargetSegmenRegional');
			return $q;
		}
		Function TargetCHGRegional(){
			$this->db->order_by('no','asc');
			$this->db->where('segmen','Consumer and Hajj');
			$q = $this->db->get('V_TargetSegmenRegional');
			return $q;
		}

        Function TargetRegional(){
			$this->db->order_by('no','asc');
			$q = $this->db->get('V_TargetRegional');
			return $q;
		}
		Function TargetRegionalRO1(){
			$this->db->order_by('no','asc');
			$this->db->where('Region','Sumatra 1 (Medan)');
			$q = $this->db->get('V_TargetRegionalArea');
			return $q;
		}
		Function TargetRegionalRO2(){
			$this->db->order_by('no','asc');
			$this->db->where('Region','Sumatra 2 (Palembang)');
			$q = $this->db->get('V_TargetRegionalArea');
			return $q;
		}
		Function TargetRegionalRO3(){
			$this->db->order_by('no','asc');
			$this->db->where('Region','Jakarta');
			$q = $this->db->get('V_TargetRegionalArea');
			return $q;
		}
		Function TargetRegionalRO4(){
			$this->db->order_by('no','asc');
			$this->db->where('Region','Jawa 1 (Bandung)');
			$q = $this->db->get('V_TargetRegionalArea');
			return $q;
		}
		Function TargetRegionalRO5(){
			$this->db->order_by('no','asc');
			$this->db->where('Region','Jawa 2 (Surabaya)');
			$q = $this->db->get('V_TargetRegionalArea');
			return $q;
		}
		Function TargetRegionalRO6(){
			$this->db->order_by('no','asc');
			$this->db->where('Region','Kalimantan (Banjarmasin)');
			$q = $this->db->get('V_TargetRegionalArea');
			return $q;
		}
		Function TargetRegionalRO7(){
			$this->db->order_by('no','asc');
			$this->db->where('Region','Indonesia Timur (Makassar)');
			$q = $this->db->get('V_TargetRegionalArea');
			return $q;
		}	
		function cekData ($table, $whereData){
			return $this->db->get_where($table, $whereData);
		}
		/*function getPFAndNPF($date){
			$q = $this->db->query("select [PF/NPF] as type ,
			SUM(OS) as value
			from V_PF_NPF  where FICMISDATE = ?
			group by [PF/NPF]", array($date));
			return $q->result();
		}*/

		function getPFAndNPF($date){
			$q = $this->db->query("select [PF/NPF] as type ,
			SUM(OS) as value
			from V_PF_NPF
			group by [PF/NPF]", array($date));
			return $q->result();
		}
		function getPortoBankwide(){
			$q = $this->db->query("select [SEGMENT] as type,
			((SUM(OS)) / 1000000) as value 
			from V_Bankwide  
			group by [SEGMENT]");
			return $q->result();
		}
		function getPortoRetail(){
			$q = $this->db->query("select [Divisi_Retail] as type,
			((SUM(OS)) / 1000000) as value 
			from V_PortoDivisi  
			group by [Divisi_Retail]");
			return $q->result();
		}
		function getPortoRegional(){
			$q = $this->db->query("select [Regional] as type,
			((SUM(OS)) / 1000000) as value 
			from V_GrafikPortoRegional 
			group by [Regional]");
			return $q->result();
		}
		function getUpgradeDowngrade(){
			$q = $this->db->query("select [Keterangan_mutasi] as type,
			((SUM(OS)) / 1000000) as value 
			from V_Mutasi 
			group by [Keterangan_mutasi]");
			return $q->result();
		}



}
