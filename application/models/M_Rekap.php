<?php
	Class M_Rekap extends CI_Model {
	
	    Function MRekapSegmen(){
			$this->db->order_by('no','asc');
			$q = $this->db->get('V_ReportSegmen');
			return $q;
		}
        Function MRekapRegional(){
			$this->db->order_by('no','asc');
			$q = $this->db->get('V_ReportRegional');
			return $q;
		}
		Function SumKol1BSMacct(){
			$this->db->select_sum('acct');
			$this->db->from('PortoUpdate');
            $this->db->where_in('KolCIF','1');
            $q = $this->db->get();
			return $q;
		}
		Function SumKol2BSMacct(){
			$Kol= array('2A','2B','2C');
			$this->db->select_sum('acct');
			$this->db->from('PortoUpdate');
            $this->db->where_in('KolCIF',$Kol);
            $q = $this->db->get();
			return $q;
		}	
		Function SumNPFBSMacct(){
			$Kol= array('3','4','5');
			$this->db->select_sum('acct');
			$this->db->from('PortoUpdate');
            $this->db->where_in('KolCIF',$Kol);
            $q = $this->db->get();
			return $q;
		}	
		Function SumBSMacct(){
			$this->db->select_sum('acct');
			$this->db->from('PortoUpdate');
            $q = $this->db->get();
			return $q;
		}
		Function SumOSBSM(){
			$this->db->select_sum('OSPokok_M');
			$q = $this->db->get('PortoUpdate');
			return $q;
		}
		Function SumKol1BSM(){
			$this->db->select_sum('OSPokok_M');
			$this->db->from('PortoUpdate');
            $this->db->where_in('KolCIF','1');
            $q = $this->db->get();
			return $q;
		}
		Function SumKol2BSM(){
			$Kol= array('2A','2B','2C');
			$this->db->select_sum('OSPokok_M');
			$this->db->from('PortoUpdate');
            $this->db->where_in('KolCIF',$Kol);
            $q = $this->db->get();
			return $q;
		}	
		Function SumNPFBSM(){
			$Kol= array('3','4','5');
			$this->db->select_sum('OSPokok_M');
			$this->db->from('PortoUpdate');
            $this->db->where_in('KolCIF',$Kol);
            $q = $this->db->get();
			return $q;
		}	
	}