<?php
	Class M_Report extends CI_Model {
	
	    Function file_list()
	    {
			$this->db->select('*');
            $this->db->from('t_report_files');       
			$this->db->order_by('upload_time','desc');
			//$this->db->limit(10);
			$q = $this->db->get();
			//var_dump($q);
			//var_dump($q);
			if($q->num_rows() > 0)
				return $q->result();
			return false;
		}

		public function persist($data){
			
			return $this->db->insert('t_report_files', $data);
		}
		/*	
		function updatePost($postData,$id){
			
			$_CI = $this;
			//escape all value 
			array_walk($postData, function(&$value, $key) use(&$_CI){
				$value = $_CI->db->escape_str($value);
			});

			$this->db->set($postData);
			$this->db->where('id_post',$this->db->escape_str($id));
			$this->db->update('post');
		}

		function deletePost($id){
			
			$this->db->delete('post', array('id_post' => $this->db->escape_str($id))); 
		}*/

}