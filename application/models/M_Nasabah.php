<?php
	Class M_Nasabah extends CI_Model {
		
		Function Daftar2A($table, $where){
			$this->db->limit(100);
            $this->db->where('KolCIF','2A');
            $this->db->from('DaftarNasabahLoan');
            $this->db->order_by('OSPokokConversion','desc');
		return $this->db->get_where($table, $where);
		}
		Function MNasabahKol2B(){
			$Divisi= array('BBG-1','BBG-2','MBG','PWG','CHG-C','CHG-H');
			$this->db->limit(100);
            $this->db->where('KolCIF','2B');
            $this->db->where_in('Divisi',$Divisi);
            $this->db->from('DaftarNasabahLoan');
            $this->db->order_by('OSPokokConversion','desc');
			$query = $this->db->get();
			return $query;
		}
		Function MNasabahKol2C(){
			$Divisi= array('BBG-1','BBG-2','MBG','PWG','CHG-C','CHG-H');
			$this->db->limit(100);
            $this->db->where('KolCIF','2C');
            $this->db->where_in('Divisi',$Divisi);
            $this->db->from('DaftarNasabahLoan');
            $this->db->order_by('OSPokokConversion','desc');
			$query = $this->db->get();
			return $query;
		}
		Function MNasabahKol3(){
			$Divisi= array('BBG-1','BBG-2','MBG','PWG','CHG-C','CHG-H');
			$this->db->limit(100);
            $this->db->where('KolCIF','3');
            $this->db->where_in('Divisi',$Divisi);
            $this->db->from('DaftarNasabahLoan');
            $this->db->order_by('OSPokokConversion','desc');
			$query = $this->db->get();
			return $query;
		}
		Function MNasabahKol4(){
			$Divisi= array('BBG-1','BBG-2','MBG','PWG','CHG-C','CHG-H');
			$this->db->limit(100);
            $this->db->where('KolCIF','4');
            $this->db->where_in('Divisi',$Divisi);
            $this->db->from('DaftarNasabahLoan');
            $this->db->order_by('OSPokokConversion','desc');
			$query = $this->db->get();
			return $query;
		}
		Function MNasabahKol5(){
			$Divisi= array('BBG-1','BBG-2','MBG','PWG','CHG-C','CHG-H');
			$this->db->limit(100);
            $this->db->where('KolCIF','5');
            $this->db->where_in('Divisi',$Divisi);
            $this->db->from('DaftarNasabahLoan');
            $this->db->order_by('OSPokokConversion','desc');
			$query = $this->db->get();
			return $query;
		}
		Function MNasabahDown(){
			$Divisi= array('BBG-1','BBG-2','MBG','PWG','CHG-C','CHG-H');
			$this->db->limit(100);
            $this->db->where('KetMutasi','Down. PF ke NPF');
            $this->db->where_in('Divisi',$Divisi);
            $this->db->from('DaftarNasabahLoan');
            $this->db->order_by('OSPokokConversion','desc');
			$query = $this->db->get();
			return $query;
		}
		Function MNasabahUpg(){
			$Divisi= array('BBG-1','BBG-2','MBG','PWG','CHG-C','CHG-H');
			$this->db->limit(100);
            $this->db->where('KetMutasi','Upg. NPF ke PF');
            $this->db->where_in('Divisi',$Divisi);
            $this->db->from('DaftarNasabahLoan');
            $this->db->order_by('OSPokokConversion','desc');
			$query = $this->db->get();
			return $query;
		}
		function Daftar($table, $where){
			   $this->db->limit(100);
		return $this->db->get_where($table, $where);
		}
		function DaftarArea(){
			$this->db->limit(100);
			$this->db->select('*');
            $this->db->from('ListRegionArea');       
            $this->db->order_by('Area','asc');
			$q = $this->db->get();
			return $q;
		}
		function DaftarReg(){
			$Reg= array('Sumatra 1 (Medan)','Sumatra 2 (Palembang)','Jakarta','Jawa 1 (Bandung)','Jawa 2 (Surabaya)','Kalimantan (Banjarmasin)','Indonesia Timur (Makassar)');
			$this->db->limit(100);
			$this->db->select('*');
            $this->db->where_in('Regional',$Reg);
            $this->db->from('ListRegional');       
            $this->db->order_by('no','asc');
			$q = $this->db->get();
			return $q;
		}
		function DaftarOutlet(){
			$this->db->limit(100);
			$this->db->select('*');
            $this->db->from('V_ListCabang');       
            $this->db->order_by('Outlet','asc');
			$q = $this->db->get();
			return $q;
		}
		function Portoharian(){
			$this->db->select('*');
            $this->db->from('V_PortoHarian');       
			$q = $this->db->get();
			return $q;
	    }
	    function DaftarRegsis(){
			$Reg = array($this->session->userdata("regional"),'');
			$this->db->select('*');
            $this->db->where('Regional',$Reg);
            $this->db->from('ListRegional');       
            $this->db->order_by('no','asc');
			$q = $this->db->get();
			return $q;
		}
	    function DaftarAreasis(){
			$Reg = array($this->session->userdata("regional"),'');
			$this->db->select('*');
            $this->db->where('Regional',$Reg);
            $this->db->from('ListRegionArea');       
            $this->db->order_by('Area','asc');
			$q = $this->db->get();
			return $q;
		}
		 function DaftarOuletsis(){
			$Reg = array($this->session->userdata("area"),'');
			$this->db->select('*');
            $this->db->where('Regional',$Reg);
            $this->db->from('V_ListCabang');       
            $this->db->order_by('Area','asc');
			$q = $this->db->get();
			return $q;
		}
}