<?php 
class M_login extends CI_Model{
	
	function cek_login ($table, $where){
		$this->db->from($table);
		$this->db->where($where);
		

		return $this->db->get();
	}

	
	function user_akses ()
	{
		$this->db->select('*');
    	$this->db->from('admin1');       
		$q = $this->db->get();
		return $q;
	}
	
	function updatePass($postData,$username){
			
			$_CI = $this;
			//escape all value 
			array_walk($postData, function(&$value, $key) use(&$_CI){
				$value = $_CI->db->escape_str($value);
			});

			$this->db->set($postData);
			$this->db->where('username',$username);
			$this->db->update('admin1');
		}
function DataAccount(){
			$this->db->select('*');
            $this->db->from('admin1');      
            $this->db->where('username',$this->session->userdata("nama")); 
			$q = $this->db->get();
			return $q;
		} 

		function getAccount($data){
			$this->db->select('*');
            $this->db->from('admin1');       
			$q = $this->db->get();
			//var_dump($q);
			if($q->num_rows() > 0)
				return $q->result();
			return false;
		}
}

?>