<?php 
class M_editor extends CI_Model
{
	function post_baru ($data)
	{
		$this->db->insert('post',$data);

	}
	function update ($judul_post)
	{
		$judul_post = $this->input->post('judul_post');
		$isi_post = $this->input->post('isi_post');
		$data = array (
				'judul_post' => $judul_post,
				'isi_post' => $isi_post
				);
		$this->db->where('judul_post',$judul_post);
		$this->db->update('post',$data);
	}
	function hapus_post($id_post)
	{
		$this->load->database();
		$this->db->where('id_post',$id_post);
		$this->db->delete('post');
		return true;

	}
	
}