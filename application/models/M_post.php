<?php
	Class M_post extends CI_Model {
	
	    Function Manage_post()
	    {
			$this->db->select('*');
            $this->db->from('post');       
            $this->db->order_by('tgl_post','desc');
			$q = $this->db->get();
			//var_dump($q);
			if($q->num_rows() > 0)
				return $q;
			return false;
		}


		function getBlogPostByID($id){
			$this->db->select('*');
            $this->db->from('post');       
            $this->db->where('id_post',$this->db->escape_str($id));
			$q = $this->db->get();
			//var_dump($q);
			if($q->num_rows() > 0)
				return $q->result();
			return false;
		}

		function updatePost($postData,$id){
			
			$_CI = $this;
			//escape all value 
			array_walk($postData, function(&$value, $key) use(&$_CI){
				$value = $_CI->db->escape_str($value);
			});

			$this->db->set($postData);
			$this->db->where('id_post',$this->db->escape_str($id));
			$this->db->update('post');
		}

		function deletePost($id){
			
			$this->db->delete('post', array('id_post' => $this->db->escape_str($id))); 
		}

}