<?php 
class M_account extends CI_Model
{
	

	function daftar_login($data)
	{
		$this->db->insert('admin1',$data);
	}
	function getUserInfo($data)
	{
		$q = $this->db->get_where ('admin1', array ('nip' =>$data),1);
		if ($this->db->affected_rows()>0)
		{
			$row = $q->row();
			return $row;
		}else{
			error_log('tidak ada user yang ditemukan getUserInfo('.$data.')');
			return false;
		}
	}
	function getUserInfobyEmail($email)
	{
		$query = $this->db->query('select * from admin1 where email = ? ', $email);
		return $query->row();
	}
	function insertToken ($nip)
	{
		$token = substr (sha1(rand()),0,30);
		$date = date ('Y-m-d');
		$string = array (
			'token' => $token,
			'nip' =>$nip,
			'created' => $date);
		$query = $this->db->insert_string('tokens',$string);
		$this->db->query($query);
		return $token.$nip;
	}
	function isTokenValid ($token)
	{
		$tkn = substrs($token,0,30);
		$uid = substr ($token,30);
		$q = $this->db->get_where('tokens',array(
			'tokens.token' => $tkn,
			'tokens.nip' => $uid),1);
		if($this->db->affected_rows()>0)
		{
			$row = $q->row();
			$created = $row->created;
			$createdTS = strtotime($created);
			$today = date('Y-m-d');
			$todayTS = strtotime($today);
			if($createdTS != $today );{
				return false;
			}
			$user_info = $this->getUserInfo($row->nip);
		}else{
			return false;
		}
	}
	function updatePassword ($post)
	{
		$this->db->where('nip',$post['nip']);
		$this->db->update('admin1',array('password' => $post ['password']));
		return true;
	}//End:metode tambahan untuk reset kode
}