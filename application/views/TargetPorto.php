<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Dashboard | RRG</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #5 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/morris/morris.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/fullcalendar/fullcalendar.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/jqvmap.css') ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url('assets/global/css/components-rounded.min.css'); ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url('assets/global/css/plugins.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url('assets/layouts/layout5/css/layout.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/layouts/layout/css/themes/darkblue.min.css'); ?>" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url('assets/layouts/layout5/css/custom.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 

        </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN CONTAINER -->
        <div class="wrapper">
            <!-- BEGIN HEADER -->
             <?php echo $header; ?>
            <!-- END HEADER -->
            <div class="container-fluid">
                <div class="page-content">
                    <!-- BEGIN BREADCRUMBS -->
                    <div class="breadcrumbs">
                        <h1>Tren Portofolio Pembiayaan</h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">Home</a>
                            </li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                    <!-- END BREADCRUMBS -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                     <div class="row">                       
                       <div class="col-lg-6 col-xs-12 col-sm-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption">
                                        <i class="icon-bubbles font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Segmen</span>
                                    </div>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#portlet_GrafikWholesale" data-toggle="tab"> Wholesale </a></li>
                                        <li><a href="#portlet_GrafikRetail" data-toggle="tab"> Retail </a></li>
                                    </ul>
                                </div>
                                <div class="portlet-body">
                                <div class="tab-content">
                                <div class="tab-pane active" id="portlet_GrafikWholesale">
                                    <div id="GrafikPembiayaanWholesale" class="CSSAnimationChart"></div>
                                </div>
                                <div class="tab-pane" id="portlet_GrafikRetail">
                                    <div id="GrafikPembiayaanRetail" class="CSSAnimationChart"></div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="col-lg-6 col-xs-12 col-sm-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption">
                                        <i class="icon-bubbles font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Regional</span>
                                    </div>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#portlet_GrafikRO1" data-toggle="tab"> RO1 </a></li>
                                        <li><a href="#portlet_GrafikRO2" data-toggle="tab"> RO2</a></li>
                                        <li><a href="#portlet_GrafikRO3" data-toggle="tab"> RO3 </a></li>
                                        <li><a href="#portlet_GrafikRO4" data-toggle="tab"> RO4 </a></li>
                                        <li><a href="#portlet_GrafikRO5" data-toggle="tab"> RO5 </a></li>
                                        <li><a href="#portlet_GrafikRO6" data-toggle="tab"> RO6 </a></li>
                                        <li><a href="#portlet_GrafikRO7" data-toggle="tab"> RO7 </a></li>
                                    </ul>
                                </div>
                                <div class="portlet-body">
                                <div class="tab-content">
                                <div class="tab-pane active" id="portlet_GrafikRO1">
                                    <div id="GrafikPembiayaanRO1" class="CSSAnimationChart"></div>
                                </div>
                                <div class="tab-pane" id="portlet_GrafikRO2">
                                    <div id="GrafikPembiayaanRO2" class="CSSAnimationChart"></div>
                                </div>
                                <div class="tab-pane" id="portlet_GrafikRO3">
                                    <div id="GrafikPembiayaanRO3" class="CSSAnimationChart"></div>
                                </div>
                                <div class="tab-pane" id="portlet_GrafikRO4">
                                    <div id="GrafikPembiayaanRO4" class="CSSAnimationChart"></div>
                                </div>
                                <div class="tab-pane" id="portlet_GrafikRO5">
                                    <div id="GrafikPembiayaanRO5" class="CSSAnimationChart"></div>
                                </div>
                                <div class="tab-pane" id="portlet_GrafikRO6">
                                    <div id="GrafikPembiayaanRO6" class="CSSAnimationChart"></div>
                                </div>
                                <div class="tab-pane" id="portlet_GrafikRO7">
                                    <div id="GrafikPembiayaanRO7" class="CSSAnimationChart"></div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <h3>Tren Portofolio Pembiayaan</h3>
                    </div>
                    <div class="row">
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio terhadap Target a.d. Segmen </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Segmen </th>
                                                        <th style='text-align: right'> OS Pokok 30-04-17 </th>
                                                        <th style='text-align: right'> OS Pokok 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status Pencapaian </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php foreach ($TargetSegmen as $p){
                                                    echo "<tr><td>$p->Segmen</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <!-- BEGIN CONDENSED TABLE PORTLET-->
                                <div class="portlet box purple">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio terhadap Target a.d. Regional </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                     <tr>
                                                        <th> Regional </th>
                                                        <th style='text-align: right'> OS Pokok 30-04-17 </th>
                                                        <th style='text-align: right'> OS Pokok 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status Pencapaian </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php foreach ($TargetRegion as $p){
                                                    echo "<tr><td>$p->Region</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                <div class="row">
                    <div class="col-lg-6 col-xs-12 col-sm-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption">
                                        <i class="icon-bubbles font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Segmen</span>
                                    </div>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#portlet_commercial" data-toggle="tab"> Commercial </a></li>
                                        <li><a href="#portlet_Retail" data-toggle="tab"> Retail </a></li>
                                        <li><a href="#portlet_BBG" data-toggle="tab"> BBG </a></li>
                                        <li><a href="#portlet_MBG" data-toggle="tab"> MBG </a></li>
                                        <li><a href="#portlet_PWG" data-toggle="tab"> PWG </a></li>
                                        <li><a href="#portlet_CHG" data-toggle="tab"> CHG </a></li>
                                    </ul>
                                </div>
                                <div class="portlet-body">
                            <div class="tab-content">
                            <div class="tab-pane active" id="portlet_commercial">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio Commercial terhadap Target a.d. Regional </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Regional </th>
                                                        <th style='text-align: right'> OS 30-04-17 </th>
                                                        <th style='text-align: right'> OS 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php
                                                    foreach ($TargetCMGReg as $p){
                                                    echo "<tr><td>$p->Region</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            <div class="tab-pane" id="portlet_Retail">
                                <!-- BEGIN CONDENSED TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio Retail terhadap Target a.d. Regional  </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                     <tr>
                                                        <th> Regional </th>
                                                        <th style='text-align: right'> OS 30-04-17 </th>
                                                        <th style='text-align: right'> OS 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php foreach ($TargetRetailReg as $p){
                                                    echo "<tr><td>$p->Region</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="portlet_BBG">
                                <!-- BEGIN CONDENSED TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio Business Banking terhadap Target a.d. Regional  </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                     <tr>
                                                        <th> Regional </th>
                                                        <th style='text-align: right'> OS 30-04-17 </th>
                                                        <th style='text-align: right'> OS 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php foreach ($TargetBBGReg as $p){
                                                    echo "<tr><td>$p->Region</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="portlet_MBG">
                                <!-- BEGIN CONDENSED TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio Mikro Banking terhadap Target a.d. Regional  </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                     <tr>
                                                        <th> Regional </th>
                                                        <th style='text-align: right'> OS 30-04-17 </th>
                                                        <th style='text-align: right'> OS 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php foreach ($TargetMBGReg as $p){
                                                    echo "<tr><td>$p->Region</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="portlet_PWG">
                                <!-- BEGIN CONDENSED TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio Pawning terhadap Target a.d. Regional  </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                     <tr>
                                                        <th> Regional </th>
                                                        <th style='text-align: right'> OS 30-04-17 </th>
                                                        <th style='text-align: right'> OS 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php foreach ($TargetPWGReg as $p){
                                                    echo "<tr><td>$p->Region</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="portlet_CHG">
                                <!-- BEGIN CONDENSED TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio Consumer and Hajj terhadap Target a.d. Regional  </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                     <tr>
                                                        <th> Regional </th>
                                                        <th style='text-align: right'> OS 30-04-17 </th>
                                                        <th style='text-align: right'> OS 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php foreach ($TargetCHGReg as $p){
                                                    echo "<tr><td>$p->Region</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                      <div class="col-lg-6 col-xs-12 col-sm-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption">
                                        <i class="icon-bubbles font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Regional</span>
                                    </div>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#portlet_RO1" data-toggle="tab"> RO1 </a></li>
                                        <li><a href="#portlet_RO2" data-toggle="tab"> RO2 </a></li>
                                        <li><a href="#portlet_RO3" data-toggle="tab"> RO3 </a></li>
                                        <li><a href="#portlet_RO4" data-toggle="tab"> RO4 </a></li>
                                        <li><a href="#portlet_RO5" data-toggle="tab"> RO5 </a></li>
                                        <li><a href="#portlet_RO6" data-toggle="tab"> RO6 </a></li>
                                        <li><a href="#portlet_RO7" data-toggle="tab"> RO7 </a></li>
                                    </ul>
                                </div>
                                <div class="portlet-body">
                            <div class="tab-content">
                            <div class="tab-pane active" id="portlet_RO1">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio Regional 1 terhadap Target a.d. Area </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Area </th>
                                                        <th style='text-align: right'> OS 30-04-17 </th>
                                                        <th style='text-align: right'> OS 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php
                                                    foreach ($TargetRegionRO1 as $p){
                                                    echo "<tr><td>$p->Area</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            <div class="tab-pane" id="portlet_RO2">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio Regional 2 terhadap Target a.d. Area </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Area </th>
                                                        <th style='text-align: right'> OS 30-04-17 </th>
                                                        <th style='text-align: right'> OS 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php
                                                    foreach ($TargetRegionRO2 as $p){
                                                    echo "<tr><td>$p->Area</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            <div class="tab-pane" id="portlet_RO3">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio Regional 3 terhadap Target a.d. Area </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Area </th>
                                                        <th style='text-align: right'> OS 30-04-17 </th>
                                                        <th style='text-align: right'> OS 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php
                                                    foreach ($TargetRegionRO3 as $p){
                                                    echo "<tr><td>$p->Area</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            <div class="tab-pane" id="portlet_RO4">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio Regional 4 terhadap Target a.d. Area </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Area </th>
                                                        <th style='text-align: right'> OS 30-04-17 </th>
                                                        <th style='text-align: right'> OS 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php
                                                    foreach ($TargetRegionRO4 as $p){
                                                    echo "<tr><td>$p->Area</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            <div class="tab-pane" id="portlet_RO5">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio Regional 5 terhadap Target a.d. Area </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Area </th>
                                                        <th style='text-align: right'> OS 30-04-17 </th>
                                                        <th style='text-align: right'> OS 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php
                                                    foreach ($TargetRegionRO5 as $p){
                                                    echo "<tr><td>$p->Area</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            <div class="tab-pane" id="portlet_RO6">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio Regional 6 terhadap Target a.d. Area </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Area </th>
                                                        <th style='text-align: right'> OS 30-04-17 </th>
                                                        <th style='text-align: right'> OS 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php
                                                    foreach ($TargetRegionRO6 as $p){
                                                    echo "<tr><td>$p->Area</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            <div class="tab-pane" id="portlet_RO7">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Pencapaian Portofolio Regional 7 terhadap Target a.d. Area </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="javascript:;" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Area </th>
                                                        <th style='text-align: right'> OS 30-04-17 </th>
                                                        <th style='text-align: right'> OS 22-05-17 </th>
                                                        <th style='text-align: right'> Target 31-12-17 </th>
                                                        <th style='text-align: right'> Status </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> <?php
                                                    foreach ($TargetRegionRO7 as $p){
                                                    echo "<tr><td>$p->Area</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalHarian,2); echo " M</td>
                                                          <td style='text-align: right'>"; echo number_format($p->OSTotalBulan,2); echo " M</td> 
                                                          <td style='text-align: right'>"; $Hasil=($p->OSTotalHarian/$p->OSTotalBulan)*100; echo number_format($Hasil,02); echo " %</td></tr>";
                                                          ;}?>
                                                    </tr>  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <!-- BEGIN FOOTER -->
                <p class="copyright"> 2016 &copy; Metronic Theme By
                    <a target="_blank" href="http://keenthemes.com">Keenthemes</a> &nbsp;|&nbsp;
                    <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
                </p>
                <a href="#index" class="go2top">
                    <i class="icon-arrow-up"></i>
                </a>
                <!-- END FOOTER -->
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN QUICK SIDEBAR -->
        <a href="javascript:;" class="page-quick-sidebar-toggler">
            <i class="icon-login"></i>
        </a>
        <div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
            <div class="page-quick-sidebar">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="javascript:;" data-target="#quick_sidebar_tab_1" data-toggle="tab"> Users
                            <span class="badge badge-danger">2</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-target="#quick_sidebar_tab_2" data-toggle="tab"> Alerts
                            <span class="badge badge-success">7</span>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> More
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                    <i class="icon-bell"></i> Alerts </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                    <i class="icon-info"></i> Notifications </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                    <i class="icon-speech"></i> Activities </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                    <i class="icon-settings"></i> Settings </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active page-quick-sidebar-chat" id="quick_sidebar_tab_1">
                        <div class="page-quick-sidebar-chat-users" data-rail-color="#ddd" data-wrapper-class="page-quick-sidebar-list">
                            <h3 class="list-heading">Staff</h3>
                            <ul class="media-list list-items">
                                <li class="media">
                                    <div class="media-status">
                                        <span class="badge badge-success">8</span>
                                    </div>
                                    <img class="media-object" src="../assets/layouts/layout/img/avatar3.jpg" alt="...">
                                    <div class="media-body">
                                        <h4 class="media-heading">Bob Nilson</h4>
                                        <div class="media-heading-sub"> Project Manager </div>
                                    </div>
                                </li>
                                <li class="media">
                                    <img class="media-object" src="../assets/layouts/layout/img/avatar1.jpg" alt="...">
                                    <div class="media-body">
                                        <h4 class="media-heading">Nick Larson</h4>
                                        <div class="media-heading-sub"> Art Director </div>
                                    </div>
                                </li>
                                <li class="media">
                                    <div class="media-status">
                                        <span class="badge badge-danger">3</span>
                                    </div>
                                    <img class="media-object" src="../assets/layouts/layout/img/avatar4.jpg" alt="...">
                                    <div class="media-body">
                                        <h4 class="media-heading">Deon Hubert</h4>
                                        <div class="media-heading-sub"> CTO </div>
                                    </div>
                                </li>
                                <li class="media">
                                    <img class="media-object" src="../assets/layouts/layout/img/avatar2.jpg" alt="...">
                                    <div class="media-body">
                                        <h4 class="media-heading">Ella Wong</h4>
                                        <div class="media-heading-sub"> CEO </div>
                                    </div>
                                </li>
                            </ul>
                            <h3 class="list-heading">Customers</h3>
                            <ul class="media-list list-items">
                                <li class="media">
                                    <div class="media-status">
                                        <span class="badge badge-warning">2</span>
                                    </div>
                                    <img class="media-object" src="../assets/layouts/layout/img/avatar6.jpg" alt="...">
                                    <div class="media-body">
                                        <h4 class="media-heading">Lara Kunis</h4>
                                        <div class="media-heading-sub"> CEO, Loop Inc </div>
                                        <div class="media-heading-small"> Last seen 03:10 AM </div>
                                    </div>
                                </li>
                                <li class="media">
                                    <div class="media-status">
                                        <span class="label label-sm label-success">new</span>
                                    </div>
                                    <img class="media-object" src="../assets/layouts/layout/img/avatar7.jpg" alt="...">
                                    <div class="media-body">
                                        <h4 class="media-heading">Ernie Kyllonen</h4>
                                        <div class="media-heading-sub"> Project Manager,
                                            <br> SmartBizz PTL </div>
                                    </div>
                                </li>
                                <li class="media">
                                    <img class="media-object" src="../assets/layouts/layout/img/avatar8.jpg" alt="...">
                                    <div class="media-body">
                                        <h4 class="media-heading">Lisa Stone</h4>
                                        <div class="media-heading-sub"> CTO, Keort Inc </div>
                                        <div class="media-heading-small"> Last seen 13:10 PM </div>
                                    </div>
                                </li>
                                <li class="media">
                                    <div class="media-status">
                                        <span class="badge badge-success">7</span>
                                    </div>
                                    <img class="media-object" src="../assets/layouts/layout/img/avatar9.jpg" alt="...">
                                    <div class="media-body">
                                        <h4 class="media-heading">Deon Portalatin</h4>
                                        <div class="media-heading-sub"> CFO, H&D LTD </div>
                                    </div>
                                </li>
                                <li class="media">
                                    <img class="media-object" src="../assets/layouts/layout/img/avatar10.jpg" alt="...">
                                    <div class="media-body">
                                        <h4 class="media-heading">Irina Savikova</h4>
                                        <div class="media-heading-sub"> CEO, Tizda Motors Inc </div>
                                    </div>
                                </li>
                                <li class="media">
                                    <div class="media-status">
                                        <span class="badge badge-danger">4</span>
                                    </div>
                                    <img class="media-object" src="../assets/layouts/layout/img/avatar11.jpg" alt="...">
                                    <div class="media-body">
                                        <h4 class="media-heading">Maria Gomez</h4>
                                        <div class="media-heading-sub"> Manager, Infomatic Inc </div>
                                        <div class="media-heading-small"> Last seen 03:10 AM </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="page-quick-sidebar-item">
                            <div class="page-quick-sidebar-chat-user">
                                <div class="page-quick-sidebar-nav">
                                    <a href="javascript:;" class="page-quick-sidebar-back-to-list">
                                        <i class="icon-arrow-left"></i>Back</a>
                                </div>
                                <div class="page-quick-sidebar-chat-user-messages">
                                    <div class="post out">
                                        <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar3.jpg" />
                                        <div class="message">
                                            <span class="arrow"></span>
                                            <a href="javascript:;" class="name">Bob Nilson</a>
                                            <span class="datetime">20:15</span>
                                            <span class="body"> When could you send me the report ? </span>
                                        </div>
                                    </div>
                                    <div class="post in">
                                        <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar2.jpg" />
                                        <div class="message">
                                            <span class="arrow"></span>
                                            <a href="javascript:;" class="name">Ella Wong</a>
                                            <span class="datetime">20:15</span>
                                            <span class="body"> Its almost done. I will be sending it shortly </span>
                                        </div>
                                    </div>
                                    <div class="post out">
                                        <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar3.jpg" />
                                        <div class="message">
                                            <span class="arrow"></span>
                                            <a href="javascript:;" class="name">Bob Nilson</a>
                                            <span class="datetime">20:15</span>
                                            <span class="body"> Alright. Thanks! :) </span>
                                        </div>
                                    </div>
                                    <div class="post in">
                                        <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar2.jpg" />
                                        <div class="message">
                                            <span class="arrow"></span>
                                            <a href="javascript:;" class="name">Ella Wong</a>
                                            <span class="datetime">20:16</span>
                                            <span class="body"> You are most welcome. Sorry for the delay. </span>
                                        </div>
                                    </div>
                                    <div class="post out">
                                        <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar3.jpg" />
                                        <div class="message">
                                            <span class="arrow"></span>
                                            <a href="javascript:;" class="name">Bob Nilson</a>
                                            <span class="datetime">20:17</span>
                                            <span class="body"> No probs. Just take your time :) </span>
                                        </div>
                                    </div>
                                    <div class="post in">
                                        <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar2.jpg" />
                                        <div class="message">
                                            <span class="arrow"></span>
                                            <a href="javascript:;" class="name">Ella Wong</a>
                                            <span class="datetime">20:40</span>
                                            <span class="body"> Alright. I just emailed it to you. </span>
                                        </div>
                                    </div>
                                    <div class="post out">
                                        <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar3.jpg" />
                                        <div class="message">
                                            <span class="arrow"></span>
                                            <a href="javascript:;" class="name">Bob Nilson</a>
                                            <span class="datetime">20:17</span>
                                            <span class="body"> Great! Thanks. Will check it right away. </span>
                                        </div>
                                    </div>
                                    <div class="post in">
                                        <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar2.jpg" />
                                        <div class="message">
                                            <span class="arrow"></span>
                                            <a href="javascript:;" class="name">Ella Wong</a>
                                            <span class="datetime">20:40</span>
                                            <span class="body"> Please let me know if you have any comment. </span>
                                        </div>
                                    </div>
                                    <div class="post out">
                                        <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar3.jpg" />
                                        <div class="message">
                                            <span class="arrow"></span>
                                            <a href="javascript:;" class="name">Bob Nilson</a>
                                            <span class="datetime">20:17</span>
                                            <span class="body"> Sure. I will check and buzz you if anything needs to be corrected. </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="page-quick-sidebar-chat-user-form">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Type a message here...">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn green">
                                                <i class="icon-paper-clip"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane page-quick-sidebar-alerts" id="quick_sidebar_tab_2">
                        <div class="page-quick-sidebar-alerts-list">
                            <h3 class="list-heading">General</h3>
                            <ul class="feeds list-items">
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-info">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc"> You have 4 pending tasks.
                                                    <span class="label label-sm label-warning "> Take action
                                                        <i class="fa fa-share"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <div class="date"> Just now </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-success">
                                                        <i class="fa fa-bar-chart-o"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc"> Finance Report for year 2013 has been released. </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> 20 mins </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-danger">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc"> You have 5 pending membership that requires a quick review. </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <div class="date"> 24 mins </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-info">
                                                    <i class="fa fa-shopping-cart"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc"> New order received with
                                                    <span class="label label-sm label-success"> Reference Number: DR23923 </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <div class="date"> 30 mins </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-success">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc"> You have 5 pending membership that requires a quick review. </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <div class="date"> 24 mins </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-info">
                                                    <i class="fa fa-bell-o"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc"> Web server hardware needs to be upgraded.
                                                    <span class="label label-sm label-warning"> Overdue </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <div class="date"> 2 hours </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-default">
                                                        <i class="fa fa-briefcase"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc"> IPO Report for year 2013 has been released. </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> 20 mins </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <h3 class="list-heading">System</h3>
                            <ul class="feeds list-items">
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-info">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc"> You have 4 pending tasks.
                                                    <span class="label label-sm label-warning "> Take action
                                                        <i class="fa fa-share"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <div class="date"> Just now </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-danger">
                                                        <i class="fa fa-bar-chart-o"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc"> Finance Report for year 2013 has been released. </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> 20 mins </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-default">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc"> You have 5 pending membership that requires a quick review. </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <div class="date"> 24 mins </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-info">
                                                    <i class="fa fa-shopping-cart"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc"> New order received with
                                                    <span class="label label-sm label-success"> Reference Number: DR23923 </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <div class="date"> 30 mins </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-success">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc"> You have 5 pending membership that requires a quick review. </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <div class="date"> 24 mins </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-warning">
                                                    <i class="fa fa-bell-o"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc"> Web server hardware needs to be upgraded.
                                                    <span class="label label-sm label-default "> Overdue </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <div class="date"> 2 hours </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-info">
                                                        <i class="fa fa-briefcase"></i>
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc"> IPO Report for year 2013 has been released. </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> 20 mins </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane page-quick-sidebar-settings" id="quick_sidebar_tab_3">
                        <div class="page-quick-sidebar-settings-list">
                            <h3 class="list-heading">General Settings</h3>
                            <ul class="list-items borderless">
                                <li> Enable Notifications
                                    <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="success" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                <li> Allow Tracking
                                    <input type="checkbox" class="make-switch" data-size="small" data-on-color="info" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                <li> Log Errors
                                    <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="danger" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                <li> Auto Sumbit Issues
                                    <input type="checkbox" class="make-switch" data-size="small" data-on-color="warning" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                <li> Enable SMS Alerts
                                    <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="success" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                            </ul>
                            <h3 class="list-heading">System Settings</h3>
                            <ul class="list-items borderless">
                                <li> Security Level
                                    <select class="form-control input-inline input-sm input-small">
                                        <option value="1">Normal</option>
                                        <option value="2" selected>Medium</option>
                                        <option value="e">High</option>
                                    </select>
                                </li>
                                <li> Failed Email Attempts
                                    <input class="form-control input-inline input-sm input-small" value="5" /> </li>
                                <li> Secondary SMTP Port
                                    <input class="form-control input-inline input-sm input-small" value="3560" /> </li>
                                <li> Notify On System Error
                                    <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="danger" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                                <li> Notify On SMTP Error
                                    <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="warning" data-on-text="ON" data-off-color="default" data-off-text="OFF"> </li>
                            </ul>
                            <div class="inner-content">
                                <button class="btn btn-success">
                                    <i class="icon-settings"></i> Save Changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END QUICK SIDEBAR -->
        <!-- BEGIN QUICK NAV --
        <nav class="quick-nav">
            <a class="quick-nav-trigger" href="#0">
                <span aria-hidden="true"></span>
            </a>
            <ul>
                <li>
                    <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" target="_blank" class="active">
                        <span>Purchase Metronic</span>
                        <i class="icon-basket"></i>
                    </a>
                </li>
                <li>
                    <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/reviews/4021469?ref=keenthemes" target="_blank">
                        <span>Customer Reviews</span>
                        <i class="icon-users"></i>
                    </a>
                </li>
                <li>
                    <a href="http://keenthemes.com/showcast/" target="_blank">
                        <span>Showcase</span>
                        <i class="icon-user"></i>
                    </a>
                </li>
                <li>
                    <a href="http://keenthemes.com/metronic-theme/changelog/" target="_blank">
                        <span>Changelog</span>
                        <i class="icon-graph"></i>
                    </a>
                </li>
            </ul>
            <span aria-hidden="true" class="quick-nav-bg"></span>
        </nav-->
        <div class="quick-nav-overlay"></div>
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="<?php echo base_url('assets/global/plugins/respond.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/global/plugins/excanvas.min.js'); ?>"></script> 
<script src="<?php echo base_url('assets/global/plugins/ie8.fix.min.js'); ?>"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/js.cookie.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/moment.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('/assets/global/plugins/morris/morris.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/morris/raphael-min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/counterup/jquery.waypoints.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/counterup/jquery.counterup.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/amcharts.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/serial.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/pie.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/radar.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/themes/light.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/themes/patterns.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/themes/chalk.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/ammap/ammap.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amstockcharts/amstock.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/fullcalendar/fullcalendar.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/horizontal-timeline/horizontal-timeline.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/flot/jquery.flot.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/flot/jquery.flot.resize.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/flot/jquery.flot.categories.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery.sparkline.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url('assets/global/scripts/app.min.js'); ?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url('assets/pages/scripts/dashboard.min.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url('assets/layouts/layout5/scripts/layout.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/layouts/global/scripts/quick-sidebar.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/layouts/global/scripts/quick-nav.min.js'); ?>" type="text/javascript"></script>
<?php

foreach ($nilaiW as $key) {
    $data[]=array('Bulan'=>$key->Bulan,'OSTotal'=>(float)$key->OSTotal);
}

foreach ($nilaiR as $key) {
    $data2[]=array('Bulan'=>$key->Bulan,'OSTotal'=>(float)$key->OSTotal);
}
foreach ($nilairo1 as $key) {
    $dataRO1[]=array('Bulan'=>$key->Bulan,'OSTotal'=>(float)$key->OSTotal);
}
foreach ($nilairo2 as $key) {
    $dataRO2[]=array('Bulan'=>$key->Bulan,'OSTotal'=>(float)$key->OSTotal);
}
foreach ($nilairo3 as $key) {
    $dataRO3[]=array('Bulan'=>$key->Bulan,'OSTotal'=>(float)$key->OSTotal);
} 
foreach ($nilairo4 as $key) {
    $dataRO4[]=array('Bulan'=>$key->Bulan,'OSTotal'=>(float)$key->OSTotal);
}
foreach ($nilairo5 as $key) {
    $dataRO5[]=array('Bulan'=>$key->Bulan,'OSTotal'=>(float)$key->OSTotal);
}
foreach ($nilairo6 as $key) {
    $dataRO6[]=array('Bulan'=>$key->Bulan,'OSTotal'=>(float)$key->OSTotal);
}
foreach ($nilairo7 as $key) {
    $dataRO7[]=array('Bulan'=>$key->Bulan,'OSTotal'=>(float)$key->OSTotal);
} 
?>



        <script>
       var Dashboard = function() {

    return {

        initJQVMAP: function() {
            if (!jQuery().vectorMap) {
                return;
            }

            var showMap = function(name) {
                jQuery('.vmaps').hide();
                jQuery('#vmap_' + name).show();
            }

            var setMap = function(name) {
                var map = jQuery('#vmap_' + name);
                
                if (map.size() !== 1) {
                    return;
                }

                var data = {
                    map: 'world_en',
                    backgroundColor: null,
                    borderColor: '#333333',
                    borderOpacity: 0.5,
                    borderWidth: 1,
                    color: '#c6c6c6',
                    enableZoom: true,
                    hoverColor: '#c9dfaf',
                    hoverOpacity: null,
                    values: sample_data,
                    normalizeFunction: 'linear',
                    scaleColors: ['#b6da93', '#909cae'],
                    selectedColor: '#c9dfaf',
                    selectedRegion: null,
                    showTooltip: true,
                    onLabelShow: function(event, label, code) {

                    },
                    onRegionOver: function(event, code) {
                        if (code == 'ca') {
                            event.preventDefault();
                        }
                    },
                    onRegionClick: function(element, code, region) {
                        var message = 'You clicked "' + region + '" which has the code: ' + code.toUpperCase();
                        alert(message);
                    }
                };

                data.map = name + '_en';
              
                map.width(map.parent().parent().width());
                map.show();
                map.vectorMap(data);
                map.hide();
            }

            setMap("world");
            setMap("usa");
            setMap("europe");
            setMap("russia");
            setMap("germany");
            showMap("world");

            jQuery('#regional_stat_world').click(function() {
                showMap("world");
            });

            jQuery('#regional_stat_usa').click(function() {
                showMap("usa");
            });

            jQuery('#regional_stat_europe').click(function() {
                showMap("europe");
            });
            jQuery('#regional_stat_russia').click(function() {
                showMap("russia");
            });
            jQuery('#regional_stat_germany').click(function() {
                showMap("germany");
            });

            $('#region_statistics_loading').hide();
            $('#region_statistics_content').show();

            App.addResizeHandler(function() {
                jQuery('.vmaps').each(function() {
                    var map = jQuery(this);
                    map.width(map.parent().width());
                });
            });
        },

        initCalendar: function() {
            if (!jQuery().fullCalendar) {
                return;
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var h = {};

            if ($('#calendar').width() <= 400) {
                $('#calendar').addClass("mobile");
                h = {
                    left: 'title, prev, next',
                    center: '',
                    right: 'today,month,agendaWeek,agendaDay'
                };
            } else {
                $('#calendar').removeClass("mobile");
                if (App.isRTL()) {
                    h = {
                        right: 'title',
                        center: '',
                        left: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                } else {
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                }
            }



            $('#calendar').fullCalendar('destroy'); // destroy the calendar
            $('#calendar').fullCalendar({ //re-initialize the calendar
                disableDragging: false,
                header: h,
                editable: true,
                events: [{
                    title: 'All Day',
                    start: new Date(y, m, 1),
                    backgroundColor: App.getBrandColor('yellow')
                }, {
                    title: 'Long Event',
                    start: new Date(y, m, d - 5),
                    end: new Date(y, m, d - 2),
                    backgroundColor: App.getBrandColor('blue')
                }, {
                    title: 'Repeating Event',
                    start: new Date(y, m, d - 3, 16, 0),
                    allDay: false,
                    backgroundColor: App.getBrandColor('red')
                }, {
                    title: 'Repeating Event',
                    start: new Date(y, m, d + 6, 16, 0),
                    allDay: false,
                    backgroundColor: App.getBrandColor('green')
                }, {
                    title: 'Meeting',
                    start: new Date(y, m, d + 9, 10, 30),
                    allDay: false
                }, {
                    title: 'Lunch',
                    start: new Date(y, m, d, 14, 0),
                    end: new Date(y, m, d, 14, 0),
                    backgroundColor: App.getBrandColor('grey'),
                    allDay: false
                }, {
                    title: 'Birthday',
                    start: new Date(y, m, d + 1, 19, 0),
                    end: new Date(y, m, d + 1, 22, 30),
                    backgroundColor: App.getBrandColor('purple'),
                    allDay: false
                }, {
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    backgroundColor: App.getBrandColor('yellow'),
                    url: 'http://google.com/'
                }]
            });
        },

        initCharts: function() {
            if (!jQuery.plot) {
                return;
            }

            function showChartTooltip(x, y, xValue, yValue) {
                $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y - 40,
                    left: x - 40,
                    border: '0px solid #ccc',
                    padding: '2px 6px',
                    'background-color': '#fff'
                }).appendTo("body").fadeIn(200);
            }

            var data = [];
            var totalPoints = 250;

            // random data generator for plot charts

            function getRandomData() {
                if (data.length > 0) data = data.slice(1);
                // do a random walk
                while (data.length < totalPoints) {
                    var prev = data.length > 0 ? data[data.length - 1] : 50;
                    var y = prev + Math.random() * 10 - 5;
                    if (y < 0) y = 0;
                    if (y > 100) y = 100;
                    data.push(y);
                }
                // zip the generated y values with the x values
                var res = [];
                for (var i = 0; i < data.length; ++i) res.push([i, data[i]])
                return res;
            }

            function randValue() {
                return (Math.floor(Math.random() * (1 + 50 - 20))) + 10;
            }

            var visitors = [
                ['02/2013', 1500],
                ['03/2013', 2500],
                ['04/2013', 1700],
                ['05/2013', 800],
                ['06/2013', 1500],
                ['07/2013', 2350],
                ['08/2013', 1500],
                ['09/2013', 1300],
                ['10/2013', 4600]
            ];


            if ($('#site_statistics').size() != 0) {

                $('#site_statistics_loading').hide();
                $('#site_statistics_content').show();

                var plot_statistics = $.plot($("#site_statistics"), [{
                        data: visitors,
                        lines: {
                            fill: 0.6,
                            lineWidth: 0
                        },
                        color: ['#f89f9f']
                    }, {
                        data: visitors,
                        points: {
                            show: true,
                            fill: true,
                            radius: 5,
                            fillColor: "#f89f9f",
                            lineWidth: 3
                        },
                        color: '#fff',
                        shadowSize: 0
                    }],

                    {
                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

                var previousPoint = null;
                $("#site_statistics").bind("plothover", function(event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' visits');
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }


            if ($('#site_activities').size() != 0) {
                //site activities
                var previousPoint2 = null;
                $('#site_activities_loading').hide();
                $('#site_activities_content').show();

                var data1 = [
                    ['DEC', 300],
                    ['JAN', 600],
                    ['FEB', 1100],
                    ['MAR', 1200],
                    ['APR', 860],
                    ['MAY', 1200],
                    ['JUN', 1450],
                    ['JUL', 1800],
                    ['AUG', 1200],
                    ['SEP', 600]
                ];


                var plot_statistics = $.plot($("#site_activities"),

                    [{
                        data: data1,
                        lines: {
                            fill: 0.2,
                            lineWidth: 0,
                        },
                        color: ['#BAD9F5']
                    }, {
                        data: data1,
                        points: {
                            show: true,
                            fill: true,
                            radius: 4,
                            fillColor: "#9ACAE6",
                            lineWidth: 2
                        },
                        color: '#9ACAE6',
                        shadowSize: 1
                    }, {
                        data: data1,
                        lines: {
                            show: true,
                            fill: false,
                            lineWidth: 3
                        },
                        color: '#9ACAE6',
                        shadowSize: 0
                    }],

                    {

                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 18,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

                $("#site_activities").bind("plothover", function(event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint2 != item.dataIndex) {
                            previousPoint2 = item.dataIndex;
                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);
                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                        }
                    }
                });

                $('#site_activities').bind("mouseleave", function() {
                    $("#tooltip").remove();
                });
            }
        },

        initEasyPieCharts: function() {
            if (!jQuery().easyPieChart) {
                return;
            }

            $('.easy-pie-chart .number.transactions').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: App.getBrandColor('yellow')
            });

            $('.easy-pie-chart .number.visits').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: App.getBrandColor('green')
            });

            $('.easy-pie-chart .number.bounce').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: App.getBrandColor('red')
            });

            $('.easy-pie-chart-reload').click(function() {
                $('.easy-pie-chart .number').each(function() {
                    var newValue = Math.floor(100 * Math.random());
                    $(this).data('easyPieChart').update(newValue);
                    $('span', this).text(newValue);
                });
            });
        },

        initSparklineCharts: function() {
            if (!jQuery().sparkline) {
                return;
            }
            $("#GrafikKol2Wholsale").sparkline([2583.96,2828.84,2569.1,2663.82,3402.6,2712.15,2502.05,2963.03,2474.12,2286.78,2598.21,2322.76,2520.8], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '55',
                barColor: '#5c9bd1',
                negBarColor: '#e02222'
            });

            $("#GrafikKol2Retail").sparkline([1827.27,1784.04,1709.94,1763.27,1666.35,1678.3,1630.27,1689.06,1448.52,1723.25,1912.79,1710.34,1860.47] , {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '55',
                barColor: '#35aa47',
                negBarColor: '#e02222'
            });

            $("#GrafikNPFWholsale").sparkline([1660.7,1646.36,1478.12,1430.98,1454.8,1392.76,1429.43,1538.91,1580.67,1499.78,1397.71,1561.9,1537.03] , {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '55',
                barColor: '#f36a5b',
                negBarColor: '#e02222'
            });

            $("#GrafikNPFRetail").sparkline([1553.24,1613.31,1454.37,1530.62,1475.25,1489.34,1442.98,1409.25,1145.94,1262.05,1245.93,1152.11,1174.82] , {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '55',
                barColor: '#ffb848',
                negBarColor: '#e02222'
            });

            $("#sparkline_line").sparkline([9, 10, 9, 10, 10, 11, 12, 10, 10, 11, 11, 12, 11, 10, 12, 11, 10, 12], {
                type: 'line',
                width: '100',
                height: '55',
                lineColor: '#ffb848'
            });
        },

        initMorisCharts: function() {
            if (Morris.EventEmitter && $('#sales_statistics').size() > 0) {
                // Use Morris.Area instead of Morris.Line
                dashboardMainChart = Morris.Area({
                    element: 'sales_statistics',
                    padding: 0,
                    behaveLikeLine: false,
                    gridEnabled: false,
                    gridLineColor: false,
                    axes: false,
                    fillOpacity: 1,
                    data: [{
                        period: '2011 Q1',
                        sales: 1400,
                        profit: 400
                    }, {
                        period: '2011 Q2',
                        sales: 1100,
                        profit: 600
                    }, {
                        period: '2011 Q3',
                        sales: 1600,
                        profit: 500
                    }, {
                        period: '2011 Q4',
                        sales: 1200,
                        profit: 400
                    }, {
                        period: '2012 Q1',
                        sales: 1550,
                        profit: 800
                    }],
                    lineColors: ['#399a8c', '#92e9dc'],
                    xkey: 'period',
                    ykeys: ['sales', 'profit'],
                    labels: ['Sales', 'Profit'],
                    pointSize: 0,
                    lineWidth: 0,
                    hideHover: 'auto',
                    resize: true
                });

            }
        },

        initChat: function() {
            var cont = $('#chats');
            var list = $('.chats', cont);
            var form = $('.chat-form', cont);
            var input = $('input', form);
            var btn = $('.btn', form);

            var handleClick = function(e) {
                e.preventDefault();

                var text = input.val();
                if (text.length == 0) {
                    return;
                }

                var time = new Date();
                var time_str = (time.getHours() + ':' + time.getMinutes());
                var tpl = '';
                tpl += '<li class="out">';
                tpl += '<img class="avatar" alt="" src="' + Layout.getLayoutImgPath() + 'avatar1.jpg"/>';
                tpl += '<div class="message">';
                tpl += '<span class="arrow"></span>';
                tpl += '<a href="#" class="name">Bob Nilson</a>&nbsp;';
                tpl += '<span class="datetime">at ' + time_str + '</span>';
                tpl += '<span class="body">';
                tpl += text;
                tpl += '</span>';
                tpl += '</div>';
                tpl += '</li>';

                var msg = list.append(tpl);
                input.val("");

                var getLastPostPos = function() {
                    var height = 0;
                    cont.find("li.out, li.in").each(function() {
                        height = height + $(this).outerHeight();
                    });

                    return height;
                }

                cont.find('.scroller').slimScroll({
                    scrollTo: getLastPostPos()
                });
            }

            $('body').on('click', '.message .name', function(e) {
                e.preventDefault(); // prevent click event

                var name = $(this).text(); // get clicked user's full name
                input.val('@' + name + ':'); // set it into the input field
                App.scrollTo(input); // scroll to input if needed
            });

            btn.click(handleClick);

            input.keypress(function(e) {
                if (e.which == 13) {
                    handleClick(e);
                    return false; //<---- Add this line
                }
            });
        },

        initDashboardDaterange: function() {
            if (!jQuery().daterangepicker) {
                return;
            }

            $('#dashboard-report-range').daterangepicker({
                "ranges": {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    'Last 7 Days': [moment().subtract('days', 6), moment()],
                    'Last 30 Days': [moment().subtract('days', 29), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                "locale": {
                    "format": "MM/DD/YYYY",
                    "separator": " - ",
                    "applyLabel": "Apply",
                    "cancelLabel": "Cancel",
                    "fromLabel": "From",
                    "toLabel": "To",
                    "customRangeLabel": "Custom",
                    "daysOfWeek": [
                        "Su",
                        "Mo",
                        "Tu",
                        "We",
                        "Th",
                        "Fr",
                        "Sa"
                    ],
                    "monthNames": [
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                    ],
                    "firstDay": 1
                },
                //"startDate": "11/08/2015",
                //"endDate": "11/14/2015",
                opens: (App.isRTL() ? 'right' : 'left'),
            }, function(start, end, label) {
                if ($('#dashboard-report-range').attr('data-display-range') != '0') {
                    $('#dashboard-report-range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
            });
             if ($('#dashboard-report-range').attr('data-display-range') != '0') {
                $('#dashboard-report-range span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
            }
            $('#dashboard-report-range').show();
        },

        initAmChart1: function() {
            if (typeof(AmCharts) === 'undefined' || $('#GrafikPembiayaanWholesale').size() === 0) {
                return;
            }

            var chart = AmCharts.makeChart("GrafikPembiayaanWholesale", {
                "type": "serial",
                "addClassNames": true,
                "theme": "light",
                "path": "../assets/global/plugins/amcharts/ammap/images/",
                "autoMargins": false,
                "marginLeft": 60,
                "marginRight": 8,
                "marginTop": 10,
                "marginBottom": 26,
                "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#ffffff"
                },

                "dataProvider": <?php  echo json_encode($data); ?>,
               
                "valueAxes": [{
                    "axisAlpha": 0,
                    "position": "left"
                }],
                "startDuration": 1,
                "graphs": [{
                    "alphaField": "alpha",
                    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "fillAlphas": 1,
                    "title": "OSTotal",
                    "type": "column",
                    "valueField": "OSTotal",
                    "dashLengthField": "dashLengthColumn",
                    "labelText":"[[value]]",
                    "labelColor":"#dddddd"
                }],
                "categoryField": "Bulan",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "tickLength": 0,
                    "marginLeft":2
                },
                "export": {
                    "enabled": true
                }
            });
        },
        initAmChart2: function() {
            if (typeof(AmCharts) === 'undefined' || $('#GrafikPembiayaanRetail').size() === 0) {
                return;
            }

            var chart = AmCharts.makeChart("GrafikPembiayaanRetail", {
                "type": "serial",
                "addClassNames": true,
                "theme": "light",
                "path": "../assets/global/plugins/amcharts/ammap/images/",
                "autoMargins": true,
                "marginLeft": 50,
                "marginRight": 8,
                "marginTop": 10,
                "marginBottom": 26,
                "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#ffffff"
                },

                "dataProvider": <?php  echo json_encode($data2); ?>,
              
                "valueAxes": [{
                    "id":"p1",
                    "axisAlpha": 0,
                    "position": "left"
                    },{
                    "id":"p2",
                    "axisAlpha": 0,
                    "position": "right"
                    }],
                "startDuration": 1,
                "graphs": [{
                    "alphaField": "alpha",
                    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "fillAlphas": 1,
                    "title": "OSTotal",
                    "type": "column",
                    "valueField": "OSTotal",
                    "dashLengthField": "dashLengthColumn",
                    "valueAxes":"p1",
                    "labelText": "[[value]]"
                }],
                "categoryField": "Bulan",
                "categoryAxis": {
                    "axisAlpha": 0,
                    "tickLength": 0,
                    "marginLeft":2,
                    "marginRight":2,
                },
                "export": {
                    "enabled": true
                }
            });
        },
        initAmChart3: function() {
            if (typeof(AmCharts) === 'undefined' || $('#GrafikPembiayaanRO1').size() === 0) {
                return;
            }

            var chart = AmCharts.makeChart("GrafikPembiayaanRO1", {
                "type": "serial",
                "addClassNames": true,
                "theme": "light",
                "path": "../assets/global/plugins/amcharts/ammap/images/",
                "autoMargins": false,
                "marginLeft": 60,
                "marginRight": 8,
                "marginTop": 10,
                "marginBottom": 26,
                "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#ffffff"
                },

                "dataProvider": <?php  echo json_encode($dataRO1); ?>,
               
                "valueAxes": [{
                    "axisAlpha": 0,
                    "position": "left"
                }],
                "startDuration": 1,
                "graphs": [{
                    "alphaField": "alpha",
                    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "fillAlphas": 1,
                    "title": "OSTotal",
                    "type": "column",
                    "valueField": "OSTotal",
                    "dashLengthField": "dashLengthColumn",
                    "labelText":"[[value]]",
                    "labelColor":"#dddddd"
                }],
                "categoryField": "Bulan",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "tickLength": 0,
                    "marginLeft":2
                },
                "export": {
                    "enabled": true
                }
            });
        },
        initAmChart4: function() {
            if (typeof(AmCharts) === 'undefined' || $('#GrafikPembiayaanRO2').size() === 0) {
                return;
            }

            var chart = AmCharts.makeChart("GrafikPembiayaanRO2", {
                "type": "serial",
                "addClassNames": true,
                "theme": "light",
                "path": "../assets/global/plugins/amcharts/ammap/images/",
                "autoMargins": false,
                "marginLeft": 60,
                "marginRight": 8,
                "marginTop": 10,
                "marginBottom": 26,
                "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#ffffff"
                },

                "dataProvider": <?php  echo json_encode($dataRO2); ?>,
               
                "valueAxes": [{
                    "axisAlpha": 0,
                    "position": "left"
                }],
                "startDuration": 1,
                "graphs": [{
                    "alphaField": "alpha",
                    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "fillAlphas": 1,
                    "title": "OSTotal",
                    "type": "column",
                    "valueField": "OSTotal",
                    "dashLengthField": "dashLengthColumn",
                    "labelText":"[[value]]",
                    "labelColor":"#dddddd"
                }],
                "categoryField": "Bulan",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "tickLength": 0,
                    "marginLeft":2
                },
                "export": {
                    "enabled": true
                }
            });
        }, 
        initAmChart5: function() {
            if (typeof(AmCharts) === 'undefined' || $('#GrafikPembiayaanRO3').size() === 0) {
                return;
            }

            var chart = AmCharts.makeChart("GrafikPembiayaanRO3", {
                "type": "serial",
                "addClassNames": true,
                "theme": "light",
                "path": "../assets/global/plugins/amcharts/ammap/images/",
                "autoMargins": true,
                "marginLeft": 50,
                "marginRight": 8,
                "marginTop": 10,
                "marginBottom": 26,
                "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#ffffff"
                },

                "dataProvider": <?php  echo json_encode($dataRO3); ?>,
              
                "valueAxes": [{
                    "id":"p1",
                    "axisAlpha": 0,
                    "position": "left"
                    },{
                    "id":"p2",
                    "axisAlpha": 0,
                    "position": "right"
                    }],
                "startDuration": 1,
                "graphs": [{
                    "alphaField": "alpha",
                    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "fillAlphas": 1,
                    "title": "OSTotal",
                    "type": "column",
                    "valueField": "OSTotal",
                    "dashLengthField": "dashLengthColumn",
                    "valueAxes":"p1",
                    "labelText": "[[value]]"
                }],
                "categoryField": "Bulan",
                "categoryAxis": {
                    "axisAlpha": 0,
                    "tickLength": 0,
                    "marginLeft":2,
                    "marginRight":2,
                },
                "export": {
                    "enabled": true
                }
            });
        },
        initAmChart6: function() {
            if (typeof(AmCharts) === 'undefined' || $('#GrafikPembiayaanRO4').size() === 0) {
                return;
            }

            var chart = AmCharts.makeChart("GrafikPembiayaanRO4", {
                "type": "serial",
                "addClassNames": true,
                "theme": "light",
                "path": "../assets/global/plugins/amcharts/ammap/images/",
                "autoMargins": true,
                "marginLeft": 50,
                "marginRight": 8,
                "marginTop": 10,
                "marginBottom": 26,
                "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#ffffff"
                },

                "dataProvider": <?php  echo json_encode($dataRO4); ?>,
              
                "valueAxes": [{
                    "id":"p1",
                    "axisAlpha": 0,
                    "position": "left"
                    },{
                    "id":"p2",
                    "axisAlpha": 0,
                    "position": "right"
                    }],
                "startDuration": 1,
                "graphs": [{
                    "alphaField": "alpha",
                    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "fillAlphas": 1,
                    "title": "OSTotal",
                    "type": "column",
                    "valueField": "OSTotal",
                    "dashLengthField": "dashLengthColumn",
                    "valueAxes":"p1",
                    "labelText": "[[value]]"
                }],
                "categoryField": "Bulan",
                "categoryAxis": {
                    "axisAlpha": 0,
                    "tickLength": 0,
                    "marginLeft":2,
                    "marginRight":2,
                },
                "export": {
                    "enabled": true
                }
            });
        },
        initAmChart7: function() {
            if (typeof(AmCharts) === 'undefined' || $('#GrafikPembiayaanRO5').size() === 0) {
                return;
            }

            var chart = AmCharts.makeChart("GrafikPembiayaanRO5", {
                "type": "serial",
                "addClassNames": true,
                "theme": "light",
                "path": "../assets/global/plugins/amcharts/ammap/images/",
                "autoMargins": true,
                "marginLeft": 50,
                "marginRight": 8,
                "marginTop": 10,
                "marginBottom": 26,
                "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#ffffff"
                },

                "dataProvider": <?php  echo json_encode($dataRO5); ?>,
              
                "valueAxes": [{
                    "id":"p1",
                    "axisAlpha": 0,
                    "position": "left"
                    },{
                    "id":"p2",
                    "axisAlpha": 0,
                    "position": "right"
                    }],
                "startDuration": 1,
                "graphs": [{
                    "alphaField": "alpha",
                    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "fillAlphas": 1,
                    "title": "OSTotal",
                    "type": "column",
                    "valueField": "OSTotal",
                    "dashLengthField": "dashLengthColumn",
                    "valueAxes":"p1",
                    "labelText": "[[value]]"
                }],
                "categoryField": "Bulan",
                "categoryAxis": {
                    "axisAlpha": 0,
                    "tickLength": 0,
                    "marginLeft":2,
                    "marginRight":2,
                },
                "export": {
                    "enabled": true
                }
            });
        },
        initAmChart8: function() {
            if (typeof(AmCharts) === 'undefined' || $('#GrafikPembiayaanRO6').size() === 0) {
                return;
            }

            var chart = AmCharts.makeChart("GrafikPembiayaanRO6", {
                "type": "serial",
                "addClassNames": true,
                "theme": "light",
                "path": "../assets/global/plugins/amcharts/ammap/images/",
                "autoMargins": true,
                "marginLeft": 50,
                "marginRight": 8,
                "marginTop": 10,
                "marginBottom": 26,
                "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#ffffff"
                },

                "dataProvider": <?php  echo json_encode($dataRO6); ?>,
              
                "valueAxes": [{
                    "id":"p1",
                    "axisAlpha": 0,
                    "position": "left"
                    },{
                    "id":"p2",
                    "axisAlpha": 0,
                    "position": "right"
                    }],
                "startDuration": 1,
                "graphs": [{
                    "alphaField": "alpha",
                    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "fillAlphas": 1,
                    "title": "OSTotal",
                    "type": "column",
                    "valueField": "OSTotal",
                    "dashLengthField": "dashLengthColumn",
                    "valueAxes":"p1",
                    "labelText": "[[value]]"
                }],
                "categoryField": "Bulan",
                "categoryAxis": {
                    "axisAlpha": 0,
                    "tickLength": 0,
                    "marginLeft":2,
                    "marginRight":2,
                },
                "export": {
                    "enabled": true
                }
            });
        },
        initAmChart9: function() {
            if (typeof(AmCharts) === 'undefined' || $('#GrafikPembiayaanRO7').size() === 0) {
                return;
            }

            var chart = AmCharts.makeChart("GrafikPembiayaanRO7", {
                "type": "serial",
                "addClassNames": true,
                "theme": "light",
                "path": "../assets/global/plugins/amcharts/ammap/images/",
                "autoMargins": true,
                "marginLeft": 50,
                "marginRight": 8,
                "marginTop": 10,
                "marginBottom": 26,
                "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#ffffff"
                },

                "dataProvider": <?php  echo json_encode($dataRO7); ?>,
              
                "valueAxes": [{
                    "id":"p1",
                    "axisAlpha": 0,
                    "position": "left"
                    },{
                    "id":"p2",
                    "axisAlpha": 0,
                    "position": "right"
                    }],
                "startDuration": 1,
                "graphs": [{
                    "alphaField": "alpha",
                    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "fillAlphas": 1,
                    "title": "OSTotal",
                    "type": "column",
                    "valueField": "OSTotal",
                    "dashLengthField": "dashLengthColumn",
                    "valueAxes":"p1",
                    "labelText": "[[value]]"
                }],
                "categoryField": "Bulan",
                "categoryAxis": {
                    "axisAlpha": 0,
                    "tickLength": 0,
                    "marginLeft":2,
                    "marginRight":2,
                },
                "export": {
                    "enabled": true
                }
            });
        },

        initWorldMapStats: function() {
            if ($('#mapplic').size() === 0) {
                return;
            }

            $('#mapplic').mapplic({
                source: '../assets/global/plugins/mapplic/world.json',
                height: 265,
                animate: false,
                sidebar: false,
                minimap: false,
                locations: true,
                deeplinking: true,
                fullscreen: false,
                hovertip: true,
                zoombuttons: false,
                clearbutton: false,
                developer: false,
                maxscale: 2,
                skin: 'mapplic-dark',
                zoom: true
            });

            $("#widget_Targetkol2_bar").sparkline([8, 7, 9, 8.5, 8, 8.2, 8, 8.5, 9, 8, 9], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '30',
                barColor: '#4db3a4',
                negBarColor: '#e02222'
            });

            $("#widget_targetnpf_bar").sparkline([8, 7, 9, 8.5, 8, 8.2, 8, 8.5, 9, 8, 9], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '30',
                barColor: '#f36a5a',
                negBarColor: '#e02222'
            });

            $("#widget_sparkline_bar3").sparkline([8, 7, 9, 8.5, 8, 8.2, 8, 8.5, 9, 8, 9], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '30',
                barColor: '#5b9bd1',
                negBarColor: '#e02222'
            });

            $("#widget_sparkline_bar4").sparkline([8, 7, 9, 8.5, 8, 8.2, 8, 8.5, 9, 8, 9], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '30',
                barColor: '#9a7caf',
                negBarColor: '#e02222'
            });
        },

        init: function() {

            this.initJQVMAP();
            this.initCalendar();
            this.initCharts();
            this.initEasyPieCharts();
            this.initSparklineCharts();
            this.initChat();
            this.initDashboardDaterange();
            this.initMorisCharts();

            this.initAmChart1();
            this.initAmChart2();
            this.initAmChart3();
            this.initAmChart4();
            this.initAmChart5();
            this.initAmChart6();
            this.initAmChart7();
            this.initAmChart8();
            this.initAmChart9();

            this.initWorldMapStats();
        }
    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function() {
        Dashboard.init(); // init metronic core componets
    });
}     

        </script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>