<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Review Nasabah | RRG</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #5 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
         <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet'); ?>" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-summernote/summernote.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url('assets/global/css/components.min.css'); ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url('assets/global/css/plugins.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url('assets/layouts/layout5/css/layout.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/layouts/layout5/css/custom.min.css'); ?>" rel="stylesheet" type="text/css" />
       
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN CONTAINER -->
        <div class="wrapper">
            <!-- BEGIN HEADER -->
           <?php echo $header; ?>
            <!-- END HEADER MENU -->
                    <div class="container-fluid">
                <div class="page-content">
                    <!-- BEGIN BREADCRUMBS -->
                    <div class="breadcrumbs">
                        <h1>Nasabah 3 Pilar</h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">Daftar Nasabah</a>
                            </li>
                            <li>
                                <a href="#">Nasabah 3 Pilar</a>
                            </li>
                            <li class="active"> <a href ="<?php echo site_url('#') ?>" >Review </a></li>
                        </ol>
                    </div>

                    <!-- END BREADCRUMBS -->
                 <!-- BEGIN PAGE TITLE-->
                        
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabbable-line boxless tabbable-reversed">
                                  
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_0">
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i>Review Nasabah </div>
                                                    
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form class="form-horizontal" action="" method="">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">No Loan</label>
                                                                <div class="col-md-10">
                                                                <input class="form-control input-circle" disabled="disabled" placeholder="Noloan" name="NoLoan" type="text"  value="<?php echo $Data->NoLoan; ?>"/>
                                            
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Nama Lengkap</label>
                                                                <div class="col-md-10">
                                                                <input class="form-control input-circle" disabled="disabled" placeholder="NamaLengkap" name="NamaLengkap" type="text"  value="<?php echo $Data->NamaLengkap; ?>"/> 
                                                                </div>
                                                            </div>
                                                     
                                                        </div>
                                                        </div>
                                                    </form>


                                                    <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase"> Managed Nasabah</span>
                                        </div>
                                        
                                    </div>
                                    <!--<form class="" action="<?php //echo site_url('Editor/add_post'); ?>"method="post">-->
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="nasabah3pilar">
                                            <thead>
                                                <tr> 

                                                    <th> No Loan </th>
                                                    <th> No CIF </th>
                                                    <th> Nama Lengkap</th>
                                                    <th> Divisi </th>
                                                    <th> Nama Cabang </th>
                                                    <th> Area </th>
                                                    <th> Regional </th>
                                                    <th> Skema Pembiayaan </th>
                                                    <th> Outstanding </th>
                                                    <th> Action </th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php foreach ($Data as $key => $V_Nasabah3Pilar) { ?>
                                                       <tr>
                                                            <td> <?php echo $V_Nasabah3Pilar->NoLoan; ?></td>
                                                            <td> <?php echo $V_Nasabah3Pilar->NomorCIF; ?> </td>
                                                            <td> <?php echo $V_Nasabah3Pilar->NamaLengkap; ?> </td>
                                                            <td> <?php echo $V_Nasabah3Pilar->Divisi; ?> </td>
                                                            <td> <?php echo $V_Nasabah3Pilar->NamaCabang; ?> </td>
                                                            <td> <?php echo $V_Nasabah3Pilar->Area; ?> </td>
                                                            <td> <?php echo $V_Nasabah3Pilar->Regional; ?> </td>
                                                            <td> <?php echo $V_Nasabah3Pilar->JenisPiutangPembiayaan; ?></td>
                                                            <td> <?php echo $V_Nasabah3Pilar->OS; ?></td>
                                                            <td>
                                                               <div class="btn-group">
                                                                                    <a class="btn green" href="javascript:;" data-toggle="dropdown">
                                                                                        <i class="fa fa-pencil"></i> Actions
                                                                                        <i class="fa fa-angle-down"></i>
                                                                                    </a>
                                                                                    <ul class="dropdown-menu">
                                                                                        <li>
                                                                                            <a href ="<?php echo site_url('DaftarNasabah/Edit3Pilar/'.$V_Nasabah3Pilar->NoLoan) ?>">
                                                                                                <i class="fa fa-pencil"></i> Review </a>
                                                                                       </li>
                                                                                        
                                                                                    </ul>
                                                                                </div>
                                                            </td>
                                                        </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                </form>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                        <!-- END EXTRAS PORTLET-->
                                    </div>
                                </div>
                                <!-- END PAGE BASE CONTENT -->
                        
                    
  
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="<?php echo base_url('assets/global/plugins/respond.min.js'); ?>"</script>
<script src="<?php echo base_url('assets/global/plugins/excanvas.min.js'); ?>"</script> 
<script src="<?php echo base_url('assets/global/plugins/ie8.fix.min.js'); ?>"</script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/js.cookie.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-summernote/summernote.min.js');?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url('assets/global/scripts/app.min.js'); ?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url('assets/layouts/layout5/scripts/layout.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/layouts/global/scripts/quick-sidebar.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/layouts/global/scripts/quick-nav.min.js'); ?>" type="text/javascript"></script>

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
    </html>