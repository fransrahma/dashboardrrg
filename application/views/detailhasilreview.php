                                                                            <!-- END SAMPLE TABLE PORTLET-->
                                                                            <table class="table table-bordered table-hover">
                                                                                            <tbody>
                                                                                                <tr style="background:#eee;">
                                                                                                    <td width="20%">Final Kol</td>
                                                                                                    <td><?php echo $summary['final'] ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Prospek Usaha</td>
                                                                                                    <td><?php echo $summary['pilar1'] ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Kinerja (Performance) Nasabah</td>
                                                                                                    <td><?php echo $summary['pilar2'] ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>Kemampuan Bayar</td>
                                                                                                    <td><?php echo $summary['pilar3'] ?></td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                            <div class="portlet box blue">
                                                                    <div class="portlet-title">
                                                                                    <div class="caption">
                                                                                        <i class="fa fa-comments"></i>Prospek Usaha </div>
                                                                                    <div class="tools">
                                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="portlet-body">
                                                                                    <div class="table-scrollable">
                                                                                    <table class="table table-bordered table-hover">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th> # </th>
                                                                                                    <th> Faktor Penilaian </th>
                                                                                                    <th> Nilai </th>
                                                                                                    <th> Catatan </th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <?php foreach ($factor1 as $key => $value) { ?>
                                                                                            
                                                                                                <tr>
                                                                                                    <td> <?php echo $key+1 ?> </td>
                                                                                                    <td><?php echo $value->factor_point; ?></td>
                                                                                                    <td><?php echo $nilaireview['prospek'][$value->id]['label']."<b>(".$nilaireview['prospek'][$value->id]['value'].")</b>";  ?></td>
                                                                                                    <td><?php echo $nilaireview['prospek'][$value->id]['notes'] ?></td>
                                                                                            <?php } ?>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- END SAMPLE TABLE PORTLET-->
                                                                </div>

                                                                            <!-- END SAMPLE TABLE PORTLET-->
                                                                            <div class="portlet box blue">
                                                                    <div class="portlet-title">
                                                                                    <div class="caption">
                                                                                        <i class="fa fa-comments"></i>Kinerja (Performance) Nasabah </div>
                                                                                    <div class="tools">
                                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="portlet-body">
                                                                                    <div class="table-scrollable">
                                                                                    <table class="table table-bordered table-hover">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th> # </th>
                                                                                                    <th> Faktor Penilaian </th>
                                                                                                    <th> Nilai </th>
                                                                                                    <th>Catatan</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <?php foreach ($factor2 as $key => $value) { ?>
                                                                                                <tr>
                                                                                                    <td> <?php echo $key+1 ?> </td>
                                                                                                    <td><?php echo $value->factor_point; ?></td>
                                                                                                    <td><?php echo $nilaireview['kinerja'][$value->id]['label']."<b>(".$nilaireview['kinerja'][$value->id]['value'].")</b>";  ?></td>
                                                                                                    <td><?php echo $nilaireview['kinerja'][$value->id]['notes'] ?></td>
                                                                                            <?php } ?>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- END SAMPLE TABLE PORTLET-->
                                                                </div>

                                                                                                                                            <!-- END SAMPLE TABLE PORTLET-->
                                                                                                                                            <div class="portlet box blue">
                                                                    <div class="portlet-title">
                                                                                    <div class="caption">
                                                                                        <i class="fa fa-comments"></i>Kemampuan Bayar </div>
                                                                                    <div class="tools">
                                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="portlet-body">
                                                                                    <div class="table-scrollable">
                                                                                    <table class="table table-bordered table-hover">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th> # </th>
                                                                                                    <th> Faktor Penilaian </th>
                                                                                                    <th> Nilai </th>
                                                                                                    <th>Catatan</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <?php foreach ($factor3 as $key => $value) { ?>
                                                                                                <tr>
                                                                                                    <td> <?php echo $key+1 ?> </td>
                                                                                                    <td><?php echo $value->factor_point; ?></td>
                                                                                                    <td><?php echo $nilaireview['bayar'][$value->id]['label']."<b>(".$nilaireview['bayar'][$value->id]['value'].")</b>";  ?></td>
                                                                                                    <td><?php echo $nilaireview['bayar'][$value->id]['notes'] ?></td>
                                                                                            <?php } ?>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- END SAMPLE TABLE PORTLET-->
                                                                </div>