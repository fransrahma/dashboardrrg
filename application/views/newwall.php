<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Dashboard Retail Risk Group</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all") rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assets/global/plugins/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assets/global/plugins/simple-line-icons/simple-line-icons.min.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assets/global/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"); ?>" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url("assets/global/plugins/select2/css/select2.min.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assets/global/plugins/select2/css/select2-bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url("assets/global/css/components.min.css"); ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url("assets/global/css/plugins.min.css"); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo base_url("assets/pages/css/login-4.min.css"); ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="index.html">
                <img src="" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<?php echo site_url('login/aksi_login'); ?>" method="post">
                <h3 class="form-title">Login ke Akun Anda </h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                </div>
                <div class="form-actions">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" /> Ingat saya
                        <span></span>
                    </label>
                    <button type="submit" class="btn green pull-right"> Masuk </button>
                </div>
                <div class="login-options">
                    
                </div>
                <div class="forget-password">
                    <h4>Lupa password anda ?</h4>
                    <p> Jangan Khawatir, klik
                        <a href="javascript:;" id="forget-password"> disini </a> untuk reset password anda. </p>
                </div>
                <div class="create-account">
                    <p> Belum mempunyai akun  ?&nbsp;
                        <a href="javascript:;" id="register-btn"> Buat akun </a>
                    </p>
                </div>
            </form>

            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" action="index.html" method="post">
                <h3>Lupa password ?</h3>
                <p> Masukkan email anda untuk reset password. </p>
                <div class="form-group">
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn red btn-outline">Kembali </button>
                    <button type="submit" class="btn green pull-right"> Daftar </button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
            <!-- BEGIN REGISTRATION FORM -->
            <form class="register-form" action="<?php echo site_url('welcome/cek_regis'); ?>" method="post">

                <h3>Daftar</h3>
                <p> Masukan data pribadi anda: </p>
                
                 <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">NIP</label>
                    <div class="input-icon">
                        <i class="fa fa-font"></i>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="NIP" name="nip" type="text"  value="<?php echo set_value ('id');?>"/> </div>
                        
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Nama Lengkap</label>
                    <div class="input-icon">
                        <i class="fa fa-font"></i>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Nama Lengkap" name="fullname" type="text"  value="<?php echo set_value ('fullname');?>"/> </div>
                        
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" type="text"  value="<?php echo set_value ('email');?>" /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Nomor Telepon</label>
                    <div class="input-icon">
                        <i class="fa fa-location-arrow"></i>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Nomor Telepon" name="phone" type="text"  value="<?php echo set_value ('phone');?>"/> </div>
                </div>
                 <div class="form-group">
                <div class="margin-top-10">
                 <select class="form-control input-large" id="object_tagsinput_continent" name="Regional" > <label class="control-label ">Regional</label>

                                  <option value="Sumatra 1 (Medan)">Regional 1</option>
                                  <option value="Sumatra 2 (Palembang)">Regional 2</option>
                                  <option value="Jakarta">Regional 3</option>
                                  <option value="Jawa 1 (Bandung)">Regional 4</option>
                                  <option value="Jawa 2 (Surabaya)">Regional 5</option>
                                  <option value="Kalimantan (Banjarmasin)">Regional 6</option>
                                  <option value="Indonesia Timur (Makassar)">Regional 7</option></
                                </select></div></div>
                                <div class="form-group">
                                <div class="margin-top-10">

                                </select></div></div>
                                <div class="form-group">
                                <div class="margin-top-10">
                                <select class="form-control input-large" id="object_tagsinput_continent" name="Area"> <label class="control-label"> Area </label>
                               <option value= "Area Jayapura"> Jayapura </option>                   
                               <option value= "Area Makassar"> Makassar </option>
                               <option value= "Area Palu">Palu</option>
                               <option value= "Area Bekasi"> Bekasi </option>
                               <option value= "Area Bogor"> Bogor </option>
                               <option value= "Area Jakarta Hasanudin"> Hasanudin </option>
                               <option value= "Area Jakarta Kebon jeruk"> Kebon jeruk</option>
                               <option value= "Area Jakarta Kelapa Gading"> Kelapa Gading </option>
                               <option value= "Area Jakarta Mayestik"> Mayestik</option>
                               <option value= "Area Jakarta Pondok Kelapa">  Pondok Kelapa </option>
                               <option value= "Area Jakarta Thamrin"> Thamrin </option>
                               <option value= "Area Bandung Ahmad Yani"> Ahmad Yani </option>
                               <option value= "Area Bandung Dago"> Dago </option>
                               <option value= "Area Cirebon"> Cirebon </option>
                               <option value= "Area Semarang"> Semarang </option>
                               <option value= "Area Solo">Solo</option>
                               <option value= "Area Yogyakarta">Yogyakarta</option>
                               <option value= "Area Denpasar">Denpasar</option>
                               <option value= "Area Jember">Jember</option>
                               <option value= "Area Malang">Malang</option>
                               <option value= "Area Surabaya Darmo">Surabaya Darmo</option>
                               <option value= "Area Surabaya Jemur Handayani">Surabaya Jemur Handayani</option>
                               <option value= "Area Balikpapan">Balikpapan</option>
                               <option value= "Area Banjarmasin">Banjarmasin</option>
                               <option value= "Area Pontianak">Pontianak</option>
                               <option value= "Area Aceh">Aceh</option>
                               <option value= "Area Batam">Batam</option>
                               <option value= "Area Medan Ahmad Yani">Medan Ahmad Yani</option>
                               <option value= "Area Medan Gajah Mada">Medan Gajah Mada</option>
                               <option value= "Area Pekanbaru">Pekanbaru</option>
                               <option value= "Area Pematangsiantar">Pematangsiantar</option>
                               <option value= "Area Bandarlampung">Bandarlampung</option>
                               <option value= "Area Jambi">Jambi</option>
                               <option value= "Area Padang">Padang</option>
                               <option value= "Area Palembang">Palembang</option>
                            </select>
                        </div>
                                                                
                    </div>
                <p> Masukkan detail akun anda: </p>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" type="text"  value="<?php echo set_value ('username');?>"  /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" type="text"  value="<?php echo set_value ('password');?>"/> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Ketik ulang password anda</label>
                    <div class="controls">
                        <div class="input-icon">
                            <i class="fa fa-check"></i>
                            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Ketik ulang password anda" name="rpassword" /> </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="tnc" /> Saya menyetujui
                        <a href="javascript:;">Syarat & Ketentuan </a> 
                        <span></span>
                    </label>
                    <div id="register_tnc_error"> </div>
                </div>
                <div class="form-actions">
                    <button id="register-back-btn" type="button" class="btn red btn-outline"> Kembali </button>
                    <button type="submit" id="register-submit-btn" class="btn green pull-right" > Daftar </button>
                </div>
            </form>
            <!-- END REGISTRATION FORM -->
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright"> Retail Risk  Group 2017 </div>
        <!-- END COPYRIGHT -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/js.cookie.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/additional-methods.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/select2/js/select2.full.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/backstretch/jquery.backstretch.min.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url('assets/global/scripts/app.min.js'); ?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
       
        <script src="<?php echo base_url('assets/pages/scripts/login-4.min.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
       
    </body>

</html>