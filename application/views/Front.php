<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Dashboard | RRG</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #5 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/morris/morris.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/fullcalendar/fullcalendar.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/jqvmap.css') ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url('assets/global/css/components-rounded.min.css'); ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url('assets/global/css/plugins.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url('assets/layouts/layout5/css/layout.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/layouts/layout5/css/custom.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
        <style type="text/css">
            #mapChart {
                position: relative;
                box-sizing: border-box;
                min-height: 500px;
                min-width: 500px;
                padding : 0;
                margin : 0;
            }
        </style>
        </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN CONTAINER -->
        <div class="wrapper">
            <!-- BEGIN HEADER -->
             <?php echo $header; ?>
            <!-- END HEADER -->
            <div class="container-fluid"><!-- background putih -->
            <div class="page-content">
             <div class="row">
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption ">
                                         
                                            <span class="caption-subject font-dark bold uppercase"> Portfolio Bankwide Tanggal <?php   
                                            $kemarin = date('Y-m-d', strtotime("-1 day", strtotime(date("Y-m-d"))));
                                            Echo "$kemarin <br>";
                                            ?>  
                                            </span>
                                             <li>
                                             <a href="<?php echo site_url('Portofolio/Bankwide')?>"></a>
                                            </li>
        
                                        </div>
                        
                                    </div>
                                    <div class="portlet-body">
                                        <div id="chartporto1" class="mapChart"></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption ">
                                         
                                            <span class="caption-subject font-dark bold uppercase"> Portfolio per Segmen Tanggal <?php   
                                            $kemarin = date('Y-m-d', strtotime("-1 day", strtotime(date("Y-m-d"))));
                                            Echo "$kemarin <br>";
                                            ?>
                                                
                                            </span>
                                            <li>
                                             <a href="<?php echo site_url('Portofolio/Retail')?>"></a>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="chartporto2" class="mapChart"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption ">
                                        
                                            <span class="caption-subject font-dark bold uppercase">Portfolio PF & NPF
                                            Tanggal <?php   
                                            $kemarin = date('Y-m-d', strtotime("-1 day", strtotime(date("Y-m-d"))));
                                            Echo "$kemarin <br>";
                                            ?></span>
                                            <li>
                                             <a href="<?php echo site_url('Portofolio/PFNPF')?>"></a>
                                            </li>
                                        </div>
                                     
                                    </div>
                                    <div class="portlet-body">
                                        <div id="chartporto3" class="mapChart"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption ">
                                        
                                            <span class="caption-subject font-dark bold uppercase">Portfolio Regional
                                            Tanggal <?php   
                                            $kemarin = date('Y-m-d', strtotime("-1 day", strtotime(date("Y-m-d"))));
                                            Echo "$kemarin <br>";
                                            ?></span>
                                            <li>
                                             <a href="<?php echo site_url('Portofolio/Gra')?>"></a>
                                            </li>
                                        </div>
                                     
                                    </div>
                                    <div class="portlet-body">
                                        <div id="chartporto4" class="mapChart"></div>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption ">
                                        
                                            <span class="caption-subject font-dark bold uppercase">Portfolio Upgrade & Downgrade
                                            Tanggal <?php   
                                            $kemarin = date('Y-m-d', strtotime("-1 day", strtotime(date("Y-m-d"))));
                                            Echo "$kemarin <br>";
                                            ?></span>
                                            <li>
                                             <a href="<?php echo site_url('Portofolio/UpgradeDowngrade')?>"></a>
                                            </li>
                                        </div>
                                     
                                    </div>
                                    <div class="portlet-body">
                                        <div id="chartporto4" class="mapChart"></div>
                                    </div>
                                </div>
                            </div>


                            

</div>
</div>
<!-- BEGIN FOOTER -->
             
                <a href="#index" class="go2top">
                    <i class="icon-arrow-up"></i>
                </a>
                <!-- END FOOTER -->
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN QUICK SIDEBAR -->
        <a href="javascript:;" class="page-quick-sidebar-toggler">
            <i class="icon-login"></i>
        </a>
        <div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
            
        </div>
        
        <div class="quick-nav-overlay"></div>
        <script src="<?php echo base_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/js.cookie.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/moment.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('/assets/global/plugins/morris/morris.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/morris/raphael-min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/counterup/jquery.waypoints.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/counterup/jquery.counterup.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/amcharts.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/serial.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/pie.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/radar.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/themes/light.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/themes/patterns.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/themes/chalk.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/ammap/ammap.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amstockcharts/amstock.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/themes/newlight.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/fullcalendar/fullcalendar.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/horizontal-timeline/horizontal-timeline.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/flot/jquery.flot.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/flot/jquery.flot.resize.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/flot/jquery.flot.categories.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery.sparkline.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url('assets/global/scripts/app.min.js'); ?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url('assets/pages/scripts/dashboard.min.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url("assets/layouts/layout5/scripts/layout.min.js") ?>" type="text/javascript"></script>
        <script src="<?php echo base_url("assets/layouts/global/scripts/quick-sidebar.min.js") ?>" type="text/javascript"></script>
        <script src="<?php echo base_url("assets/layouts/global/scripts/quick-nav.min.js") ?>" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
<script type="text/javascript">



    $(document).ready(function(){
        var chart1 = AmCharts.makeChart( "chartporto1", {
             "type": "pie",
            "theme": "newlight",                    
            "dataProvider": <?php echo $Bankwide; ?>,
            "valueField": "value",
            "titleField": "type",
            //"url": "<?php echo site_url('Portofolio/PFNPF')?>"
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "export": {
                "enabled": true
            }
            } );



        var chart2 = AmCharts.makeChart( "chartporto2", {
             "type": "pie",
            "theme": "newlight",                    
            "dataProvider": <?php echo $Retail; ?>,
            "valueField": "value",
            "titleField": "type",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "export": {
                "enabled": true
            }
            } );


        var chart3 = AmCharts.makeChart( "chartporto3", {
            "type": "pie",
            "theme": "newlight",                    
            "dataProvider": <?php echo $PFAndNPF; ?>,
            "valueField": "value",
            "titleField": "type",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "export": {
                "enabled": true
            }
            } );

          var chart4 = AmCharts.makeChart( "chartporto4", {
            "type": "pie",
            "theme": "newlight",                    
            "dataProvider": <?php echo $Regional; ?>,
            "valueField": "value",
            "titleField": "type",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "export": {
                "enabled": true
            },
            "labelsEnabled": true,
            "autoMargins": false,
            "marginTop": 5,
            "marginBottom": 5,
            "marginLeft": 5,
            "marginRight": 5,
            "pullOutRadius": 0,
            } );


         
    });
</script>