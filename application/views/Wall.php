<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Dashboard | RRG</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #5 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/morris/morris.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/fullcalendar/fullcalendar.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/jqvmap.css') ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url('assets/global/css/components-rounded.min.css'); ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url('assets/global/css/plugins.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url('assets/layouts/layout5/css/layout.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/layouts/layout5/css/custom.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN CONTAINER -->
        <div class="wrapper">
            <!-- BEGIN HEADER -->
            <header class="page-header">
                <nav class="navbar mega-menu" role="navigation">
                    <div class="container-fluid">
                        <div class="clearfix navbar-fixed-top">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="toggle-icon">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </span>
                            </button>
                            <!-- End Toggle Button -->
                            <!-- BEGIN LOGO -->
                        <a id="index" class="page-logo" href="<?php echo site_url(); ?>">
                                <img src="<?php echo base_url('assets/layouts/layout5/img/drag.png') ?>" alt="Logo"> </a>
                            <!-- END LOGO -->
                            <!-- BEGIN SEARCH -->
                    
                            <!-- END SEARCH -->
                            <!-- BEGIN TOPBAR ACTIONS -->
                            <div class="topbar-actions">
                                <!-- BEGIN GROUP NOTIFICATION -->
                              
                                <!-- END GROUP NOTIFICATION -->
                                <!-- BEGIN GROUP INFORMATION -->
                                
                                <!-- END GROUP INFORMATION -->
                                <!-- BEGIN USER PROFILE -->
                           
                                <!-- END USER PROFILE -->
                                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                               
                                <!-- END QUICK SIDEBAR TOGGLER -->
                            </div>
                            <!-- END TOPBAR ACTIONS -->
                        </div>
                        <!-- BEGIN HEADER MENU -->
                        
                        <!-- END HEADER MENU -->
                    <div class="container-fluid">
                <div class="page-content">
                    <!-- BEGIN BREADCRUMBS -->
                    <div class="breadcrumbs" >
                        <h1></h1> 
                        <ol class="breadcrumb">
                            <li class="active"><a href ="<?php echo site_url('welcome/login') ?>" > <button type="submit" class="btn green pull-right"> Dashboard - Login </button> </a>
                            </li>
                             
                        </ol>
                    </div>
            
                    <!-- BEGIN BREADCRUMBS -->
                  
            

                    <!-- END BREADCRUMBS -->
                     <section class="content" align="center">
    <div class="col-lg-3">
    </div>
        <!-- /.col -->
        <div class="col-lg-6">
          <div class="box box-solid">
            <!-- /.box-header -->
            <div class="box-header with-border">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>
                <div class="carousel-inner">
                  <div class="item active" href="<?php echo site_url('welcome/login') ?>" >
                    <img src="<?php echo base_url('assets/global/img/portfolio/1200x900/100.jpg') ?>" alt="First slide" >

                    <div class="carousel-caption">
                      First Slide
                    </div>
                  </div>
                  <div class="item" href="<?php echo site_url('welcome/login') ?>" >
                    <img src="<?php echo base_url('assets/global/img/portfolio/1200x900/19.jpg'); ?>" alt="Second slide" >

                    <div class="carousel-caption">
                      Second Slide
                    </div>
                  </div>
                  <div class="item" href="<?php echo site_url('welcome/login') ?>" >
                    <img src="<?php echo base_url('assets/global/img/portfolio/1200x900/30.jpg'); ?>" alt="Third slide" >

                    <div class="carousel-caption">
                      Third Slide
                    </div>
                  </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
          </div>
        </div>
    <div class="col-lg-3">
    </div>
    </section> 
    <section  >
      
                   <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-microphone font-dark hide"></i>  
                                        <span class="caption-subject bold font-dark uppercase"> </span>
                                    </div>
                                </div>
</div>
  
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->

                       <div class="row">
                            <div class="col-md-6">
                                <div class="portlet box green-steel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                           <h3>
                                            <span class="caption-subject bold font-white"> Konten Terbaru</span></h3>
                                           
                                        </div>
                                        <div class="actions">
                                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="timeline">
                                            <!-- TIMELINE ITEM -->
                                             <div class="timeline-item">
                                                      <?php foreach ($posting as $key => $post) { ?>
                                                            </div>
                                                            <div class="mt-timeline-icon  bg-font-blue border-grey-steel">
                                                            <i class=""></i>
                                                         </div>
                                                        <div class="timeline-body">
                                                            <div class="timeline-body-arrow"> </div>
                                                            <div class="timeline-body-head"> 
                                                            <h3 class="mt-content-title"><?php echo $post->judul_post ?></h3>
                                                                <div class="timeline-body-head-caption">
                                                                   <a href="" class="font-black"><?php echo $post->author; ?></a>
                                                                    <span class="timeline-body-time font-grey-cascade"></span>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="mt-author-notes font-black"><?php echo $post->tgl_post; ?></div> 
                                                             <div class="timeline-body-content">
                                                                <span class="font-grey-cascade"> <?php echo $post->isi_post; ?> </span>    
                                                        </div>
                                                        <?php } ?>
                                                   

                                                    <!-- END TIMELINE ITEM -->
                                                    
                                            <!-- END TIMELINE ITEM -->
                                            <!-- TIMELINE ITEM -->
                                          
                                            </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="portlet box green-steel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                           <h3>
                                            <span class="caption-subject bold font-white"> Quote of The Day</span></h3>
                                        </div>
                                       
                                    </div>
                                    <div class="portlet-body">
                                        <div class="timeline  blue-bg ">
                                            <!-- TIMELINE ITEM -->
                                            <div class="timeline">
                                            <!-- TIMELINE ITEM -->
                                             <div class="timeline-item">
                                                      <?php foreach ($posting as $key => $post) { ?>
                                                            </div>
                                                            <div class="mt-timeline-icon  bg-font-blue border-grey-steel">
                                                            <i class=""></i>
                                                         </div>
                                                        <div class="timeline-body">
                                                            <div class="timeline-body-arrow"> </div>
                                                            <div class="timeline-body-head">
                                                            <h3 class="mt-content-title"><?php echo $post->judul_post ?></h3>
                                                                <div class="timeline-body-head-caption">
                                                                   <a href="" class="font-black"><?php echo $post->author; ?></a>
                                                                    <span class="timeline-body-time font-grey-cascade"></span>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="mt-author-notes font-black"><?php echo $post->tgl_post; ?></div> 
                                                             <div class="timeline-body-content">
                                                                <span class="font-grey-cascade"> <?php echo $post->isi_post; ?> </span>    
                                                        </div>
                                                        <?php } ?>
                                                   
                                                    <!-- TIMELINE ITEM -->
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div> </div> 

                            <!-- END PORTLET-->
                        </div>
                    </div>
                        </div></div>
                    <!-- END PAGE BASE CONTENT -->
                </div></section>
                 <!-- BEGIN QUICK NAV -->
        <nav class="quick-nav">
            <a class="quick-nav-trigger font-black" href="#0">
                <span aria-hidden="true"> </span>
            </a>
            <ul>
                <li>
                    <a href="<?php echo site_url('wall/help') ?>" target="_blank">
                        <span>Petunjuk Website</span>
                        <i class="icon-info"></i>
                    </a>
                </li>
               
            </ul>
            <span aria-hidden="true" class="quick-nav-bg"></span>
        </nav>
        <div class="quick-nav-overlay"></div>
            </header>

    </div>
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="<?php echo base_url('assets/global/plugins/respond.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/global/plugins/excanvas.min.js'); ?>"></script> 
<script src="<?php echo base_url('assets/global/plugins/ie8.fix.min.js'); ?>"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/js.cookie.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/moment.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('/assets/global/plugins/morris/morris.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/morris/raphael-min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/counterup/jquery.waypoints.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/counterup/jquery.counterup.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/amcharts.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/serial.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/pie.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/radar.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/themes/light.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/themes/patterns.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/themes/chalk.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/ammap/ammap.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/amcharts/amstockcharts/amstock.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/fullcalendar/fullcalendar.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/horizontal-timeline/horizontal-timeline.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/flot/jquery.flot.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/flot/jquery.flot.resize.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/flot/jquery.flot.categories.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery.sparkline.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/backstretch/jquery.backstretch.min.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url('assets/global/scripts/app.min.js'); ?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url('assets/pages/scripts/dashboard.min.js'); ?>" type="text/javascript"></script>
         <div class="backstretch" style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; height: 700px; width: 1002px; z-index: -999999; position: fixed;"><img style="position: absolute; margin: 0px; padding: 0px; border: medium none; width: 1002px; height: 751.766px; max-height: none; max-width: none; z-index: -999999; left: 0px; top: -25.8831px;" src="../assets/pages/media/bg/1.jpg"></div>
        <img style="position: absolute; margin: 0px; padding: 0px; border: medium none; width: 1320.33px; height: 700px; max-height: none; max-width: none; z-index: -999999; left: -159.163px; top: 0px;" src="../assets/pages/media/bg/2.jpg">
        <div class =" box box-solid" style=" margin: 100px; padding:bottom;"
        }
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url('assets/layouts/layout5/scripts/layout.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/layouts/global/scripts/quick-sidebar.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/layouts/global/scripts/quick-nav.min.js'); ?>" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
    </html>