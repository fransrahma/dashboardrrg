<?php
//swtiching on off active menu
$controller = strtolower($this->uri->segment(1));

?>            
            <header class="page-header">
                <nav class="navbar mega-menu" role="navigation">
                    <div class="container-fluid">
                        <div class="clearfix navbar-fixed-top">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="toggle-icon">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </span>
                            </button>
                            <!-- End Toggle Button -->
                            <!-- BEGIN LOGO -->
                            <a id="index" class="page-logo" href="<?php echo site_url(); ?>">
                                <img src="<?php echo base_url('assets/layouts/layout5/img/drag.png') ?>" alt="Logo"> </a>
                            <!-- END LOGO -->
                            <!-- BEGIN SEARCH -->
                           
                            <!-- END SEARCH -->
                            <!-- BEGIN TOPBAR ACTIONS -->
                            <div class="topbar-actions">
                                
                                <div class="btn-group-img btn-group">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <span>Hi, <?php echo $this->session->userdata("fullname")?> &nbsp- <?php echo $this->session->userdata("regional"); ?> </span>
                                        <img src="<?php echo base_url('assets/layouts/layout5/img/avatar1.jpg'); ?>" alt=""> </button>
                                    <ul class="dropdown-menu-v2" role="menu">
                                        <li>
                                           <a href="<?php echo site_url('Accountprofil/manage_account') ?>">
                                                <i class="icon-user"></i> My Profile
                                                <span class="badge badge-danger">1</span>
                                            </a>
                                        </li>
                                        <li>
                                            </i> <a href ="<?php echo site_url('login/logout') ?>" >
                                                <i class="icon-key"></i> Log Out </a>
                                        </li>
                                    </ul>
                                    </form>
                                </div>
                            </div>
                            <!-- END TOPBAR ACTIONS -->
                        </div>
                        <!-- BEGIN HEADER MENU -->
                        <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
                            <ul class="nav navbar-nav">
                                <li class="dropdown dropdown-fw dropdown-fw-disabled  <?php echo ($controller === 'portofolio' ? 'active open selected' : '') ?>">
                                    <a href="javascript:;" class="text-uppercase">
                                        <i class="icon-home"></i> Dashboard </a>
                                    <ul class="dropdown-menu dropdown-menu-fw">
                                    <li>
                                            <a href="<?php echo site_url('Portofolio/overview')?>">
                                                <i class="icon-bar-chart"></i> Overview </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('Portofolio')?>">
                                                <i class="icon-bar-chart"></i> Portofolio </a>
                                        </li>
                                        
                                        <!--<li>
                                            <a href="<?php echo site_url('Portofolio/PortoBooking')?>">
                                                <i class="fa fa-money"></i> Pencairan</a>
                                        </li>
                                                                                <li>
                                            <a href="<?php echo site_url('Portofolio/PortoKol2')?>">
                                                <i class="fa fa-tachometer"></i> Kolektibilitas 2</a>
                                        </li>
                                                                                <li>
                                            <a href="<?php echo site_url('Portofolio/PortoNPF')?>">
                                                <i class="fa fa-area-chart"></i> Kolektibilitas NPF</a>
                                        </li>
                                                                                <li>
                                            <a href="<?php echo site_url('Portofolio/PortoUpgrade')?>">
                                                <i class="fa fa-angle-double-up"></i> Upgrade</a>
                                        </li>
                                                                                <li>
                                            <a href="<?php echo site_url('Portofolio/PortoUpgrade')?>">
                                                <i class="fa fa-angle-double-down"></i> Downgrade</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-fw dropdown-fw-disabled  <?php echo ($controller === 'nasabah' ? 'active open selected' : '') ?>">
                                    <a href="javascript:;" class="text-uppercase">
                                        <i class="icon-user"></i> Nasabah </a>
                                    <ul class="dropdown-menu dropdown-menu-fw">
                                        <li>
                                            <a href="<?php echo site_url('Nasabah/Kol2') ?>"> Daftar Nasabah Kol 2 </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('Nasabah/NPF'); ?>"> Daftar Nasabah NPF </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('Nasabah/Upgrade'); ?>"> Daftar Nasabah Upgrade </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('Nasabah/Down'); ?>"> Daftar Nasabah Downgrade </a>
                                        </li>
                                        <li class="dropdown more-dropdown-sub">
                                            <a href="javascript:;"> Nasabah PeKA </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="table_datatables_managed.html"> Regional 1 (Sumatera 1) </a>
                                                </li>
                                                <li>
                                                    <a href="table_datatables_buttons.html"> Regional 2 (Sumatera 2) </a>
                                                </li>
                                                <li>
                                                    <a href="table_datatables_colreorder.html"> Regional 3 (Jakarta) </a>
                                                </li>
                                                <li>
                                                    <a href="table_datatables_rowreorder.html"> Regional 4 (Jawa 1) </a>
                                                </li>
                                                <li>
                                                    <a href="table_datatables_scroller.html"> Regional 5 (Jawa 2) </a>
                                                </li>
                                                <li>
                                                    <a href="table_datatables_fixedheader.html"> Regional 6 (Kalimantan) </a>
                                                </li>
                                                <li>
                                                    <a href="table_datatables_responsive.html"> Regional 7 (Indo. Timur) </a>
                                                </li>
                                            </ul>
                                        </li>
                                        -->
                                    </ul>
                                </li>
                                <!--
                                <li class="dropdown dropdown-fw dropdown-fw-disabled <?php echo ($controller === 'target' ? 'active open selected' : '') ?>">
                                     <a href="javascript:;" class="text-uppercase">
                                        <i class="icon-pie-chart"></i> Portfolio </a>
                                    <ul class="dropdown-menu dropdown-menu-fw">
                                        
                                        <li>
                                            <a href="<?php echo site_url(''); ?>">
                                                <i class="fa fa-area-chart"></i> Daily Report </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url(''); ?>">
                                                <i class="fa fa-area-chart"></i> Monthly Report </a>
                                        </li>
                                    </ul>
                                </li>
                                -->
                                <li class="dropdown dropdown-fw dropdown-fw-disabled <?php echo (( $controller === 'upload' || $controller === 'downloads' ) ? 'active open selected' : '') ?>">
                                     <a href="javascript:;" class="text-uppercase">
                                        <i class="icon-pie-chart"></i> Downloads </a>
                                    <ul class="dropdown-menu dropdown-menu-fw">
                                    <?php if($this->session->userdata('status')){ ?>
                                    <li>
                                        <a href="<?php echo site_url('Upload/'); ?>">
                                                <i class="fa fa-area-chart"></i> Upload Report </a>
                                        </li>   
                                    <?php } ?>                                    
                                        <li>
                                            <a href="<?php echo site_url('downloads'); ?>">
                                                <i class="fa fa-area-chart"></i> Download Report </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-fw dropdown-fw-disabled <?php echo ($controller === 'Nasabah' ? 'active open selected' : '') ?>">
                                     <a href="javascript:;" class="text-uppercase">
                                        <i class="icon-pie-chart"></i> Daftar Nasabah </a>
                                   
                                    <ul class="dropdown-menu dropdown-menu-fw">
                                     <li>
                                        <a href="<?php echo site_url('DaftarNasabah/Review3Pilar'); ?>">
                                                <i class="fa fa-area-chart"></i> Nasabah Review 3 Pilar </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('Nasabah/Kol2'); ?>">
                                                <i class="fa fa-area-chart"></i> Nasabah Kol 2 </a>
                                        </li>                                        
                                        <li>
                                            <a href="<?php echo site_url('Nasabah/NPF'); ?>">
                                                <i class="fa fa-area-chart"></i> Nasabah NPF </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('Nasabah/Down'); ?>">
                                                <i class="fa fa-area-chart"></i> Nasabah Upgrade </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('Nasabah/Upgrade'); ?>">
                                                <i class="fa fa-area-chart"></i> Nasabah Downgrade </a>
                                        </li>
                                    </ul>
                                </li>                                       
                              <!--<?php //if ($this->session->userdata ("id_user") == 2 ){?> -->
                                <li class="dropdown dropdown-fw dropdown-fw-disabled <?php echo ($controller === 'manage_post' ? 'active open selected' : '') ?>">
                                    <a href="javascript:;" class="text-uppercase">

                                        <i class="icon-settings">  </i> Admin </a>
                                    <ul class="dropdown-menu dropdown-menu-fw">
                                        <li>
                                        
                                            <a href="<?php echo site_url('Manage_post')?>">
                                                <i class="icon-bar-chart"></i> Manage Posting </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('Manage_post')?>">
                                                <i class="icon-bar-chart"></i> Manage User </a>
                                        </li>
                                    </ul>
                                </li><!--<?php //}?> -->
                               
                            </ul>
                        </div>
                        <!-- END HEADER MENU -->
                    </div>
                    <!--/container-->
                </nav>
            </header>
            ?>