<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard | RRG</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>font/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<!--Header-->
  <header class="main-header"> 
    <a href="index2.html" class="logo">
      <span class="logo-mini"><b>RR</b>G</span>
      <span class="logo-lg"><b>RetailRisk</b>Group</span>
    </a>
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
      
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url(); ?>dist/img/avatar5.png" class="user-image" alt="User Image">
              <span class="hidden-xs">Sandy Nur Rahmat</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/avatar5.png" class="img-circle" alt="User Image">
                <p>
                  Sandy Nur Rahmat - Admin RRG
                  <small>MIS and Scoring Staff</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

<!--Menu Side Kiri-->
  <aside class="main-sidebar">
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>dist/img/avatar5.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Sandy Nur Rahmat</p>
          <a href="#"></i> MIS and Scoring Staff</a>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU DASHBOARD RRG</li>
        <li>
          <a href="#">
            <i class="fa fa-archive"></i> <span>Portofolio Pembiayaan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/Portofolio"><i class="fa fa-circle-o"></i>Portofolio Segmen</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Portofolio/portoregion"><i class="fa fa-circle-o"></i>Portofolio Regional</a></li>
          </ul>
        </li>
           
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cart-plus"></i> <span>Pencairan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/booking"><i class="fa fa-circle-o"></i>Rekap Pencairan Segmen</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/booking/bookingregion"><i class="fa fa-circle-o"></i>Rekap Pencairan Regional</a></li>
          </ul>
        </li>  

        <li class="treeview">
          <a href="#">
            <i class="fa fa-bar-chart"></i> <span>Kolektibilitas 2</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/Portofolio/Kol2Segmen"><i class="fa fa-circle-o"></i>Rekap Kol 2 Segmen</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Portofolio/Kol2region"><i class="fa fa-circle-o"></i>Rekap Kol 2 Regional</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/Kol2"><i class="fa fa-circle-o"></i>Daftar Nasabah Kol 2</a></li>
          </ul>
        </li> 

        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i> <span>Kolektibilitas NPF</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/Portofolio/NPFSegmen"><i class="fa fa-circle-o"></i>Rekap NPF Segmen</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Portofolio/NPFregion"><i class="fa fa-circle-o"></i>Rekap NPF Regional</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/NPF"><i class="fa fa-circle-o"></i>Daftar Nasabah NPF</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-area-chart"></i> <span>Rekap Downgrade</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/DownUp"><i class="fa fa-circle-o"></i>Rekap Downgrade Segmen</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/DownUp/DownRegion"><i class="fa fa-circle-o"></i>Rekap Downgrade Regional</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/Down"><i class="fa fa-circle-o"></i>Daftar Nasabah Downgrade</a></li>
          </ul>
        </li> 

        <li class="treeview">
          <a href="#">
            <i class="fa fa-line-chart"></i> <span>Rekap Upgrade</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/DownUp/UpgradeSegmen"><i class="fa fa-circle-o"></i>Rekap Upgrade Segmen</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/DownUp/UpgradeRegion"><i class="fa fa-circle-o"></i>Rekap Upgrade Regional</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/Upgrade"><i class="fa fa-circle-o"></i>Daftar Nasabah Upgrade</a></li>
          </ul>
        </li> 

        <li class="treeview">
          <a href="#">
            <i class="fa fa-list-alt"></i> <span>Nasabah PeKA</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/Nasabah/PeKA"><i class="fa fa-circle-o"></i>Regional 1 (Sumatra 1)</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/PeKA"><i class="fa fa-circle-o"></i>Regional 2 (Sumatra 2)</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/PeKA"><i class="fa fa-circle-o"></i>Regional 3 (Jakarta)</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/PeKA"><i class="fa fa-circle-o"></i>Regional 4 (Jawa 1)</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/PeKA"><i class="fa fa-circle-o"></i>Regional 5 (Jawa 2)</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/PeKA"><i class="fa fa-circle-o"></i>Regional 6 (Kalimantan)</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/PeKA"><i class="fa fa-circle-o"></i>Regional 7 (Indo. Timur)</a></li>
          </ul>
        </li> 

        <li class="header">Admin Dashboard RRG</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Email</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Lync</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>EXT.</span></a></li>
      </ul>
    </section>
  </aside>
  <!-- Content page  -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h2> NPF Pembiayaan <Font face="arial" size="5">Posisi 30 April 2017</font>
      </h2>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data NPF</li>
      </ol>
    </section>
    <section class="content-header">
      <h1>All NPF Segmen
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Portofolio s.d. Downgrade-->
      <div class="row">
        <!-- ./col Pembiayaan-->
        <div class="col-lg-2 col-xs-5">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>54.397</h3>
              <p>Pembiayaan</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-archive"></i>
            </div>
            <a href="#" class="small-box-footer">Segmen (Divisi) <i class="fa fa-arrow-circle-right"></i></a>
            <a href="#" class="small-box-footer">Regional <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col Pencairan-->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>1.278</h3>
              <p>Pencairan</p>
            </div>
            <div class="icon">
              <i class="ion ion-archive"></i>
            </div>
            <a href="#" class="small-box-footer">Segmen (Divisi) <i class="fa fa-arrow-circle-right"></i></a>
            <a href="#" class="small-box-footer">Regional <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col Kol 2-->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>4.234</h3>
              <p>Kol 2</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">Segmen (Divisi) <i class="fa fa-arrow-circle-right"></i></a>
            <a href="#" class="small-box-footer">Regional <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col NPF-->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>2.214</h3>
              <p>NPF</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">Segmen (Divisi) <i class="fa fa-arrow-circle-right"></i></a>
            <a href="#" class="small-box-footer">Regional <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col Upgrade-->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>45</h3>
              <p>Upgrade</p>
            </div>
            <div class="icon">
              <i class="ion ion-arrow-graph-up-left"></i>
            </div>
            <a href="#" class="small-box-footer">Segmen (Divisi) <i class="fa fa-arrow-circle-right"></i></a>
            <a href="#" class="small-box-footer">Regional <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

         <!-- ./col Downgrade-->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>227</h3>
              <p>Downgrade</p>
            </div>
            <div class="icon">
              <i class="ion ion-arrow-graph-down-right"></i>
            </div>
            <a href="#" class="small-box-footer">Segmen (Divisi) <i class="fa fa-arrow-circle-right"></i></a>
            <a href="#" class="small-box-footer">Regional <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
      <!-- /.row -->

      <!-- Main row -->



      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2017<a href="http://almsaeedstudio.com"> Retail Risk Group</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(); ?>https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(); ?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url(); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url(); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url(); ?>dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
</body>
</html>
