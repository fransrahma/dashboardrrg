<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Daftar Nasabah Kol. 2</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #5 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url('assets/global/plugins/datatables/datatables.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url('assets/global/css/components-rounded.min.css'); ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url('assets/global/css/plugins.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/css/components.min.css'); ?>" rel="stylesheet" id="style_components" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url('assets/layouts/layout5/css/layout.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/layouts/layout5/css/custom.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN CONTAINER -->
        <div class="wrapper">
            <!-- BEGIN HEADER -->
             <?php echo $header; ?>
            <!-- END HEADER -->
            <div class="container-fluid">
                <div class="page-content">
                    <!-- BEGIN BREADCRUMBS -->
                    <div class="breadcrumbs">
                        <h1>Daftar Nasabah Kol. 2</h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">Home</a>
                            </li>
                            <li><a href="#">Nasabah</a></li>
                            <li class="active">Nasabah Kol. 2</li>
                        </ol>
                    </div>
                    <!-- END BREADCRUMBS -->
                    <!-- PILIH DATA -->
                                            <div class="portlet light form-fit bordered">
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="<?php echo site_url('Nasabah/CariKol2'); ?>" class="form-horizontal form-row-seperated">
                                                    <div class="form-body">
                                                         <div class="form-group">
                                                            <label class="control-label col-md-2">Tgl Data</label>
                                                            <div class="col-md-4">
                                                                <select class="bs-select form-control" data-width="500px">
                                                                    <option><?php  $Date=$TglH[0]->FICMISDATE; $newDate= new datetime ($Date); echo $newDate->format('d-F-Y');?></option>
                                                                </select>
                                                            </div>
                                                            <label class="control-label col-md-2">Regional</label>
                                                            <div class="col-md-4">
                                                                <select class="bs-select form-control" data-width="500px" name="DropReg2">
                                                                      <?php foreach ($dropReg as $p){
                                                                      echo "<option value='$p->Regional'>$p->Regional</option>"; ;}?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-2">Area</label>
                                                            <div class="col-md-4">
                                                                <select class="bs-select form-control" data-width="500px" name="DropArea2">
                                                                    <option value="CMG">All</option>
                                                                <?php foreach ($dropAr as $p){
                                                                      echo "<option value='$p->Area'>$p->Area</option>"; ;}?>
                                                                </select>
                                                            </div>
                                                            <label class="control-label col-md-2">Outlet</label>
                                                            <div class="col-md-4">
                                                                <select class="bs-select form-control" data-width="500px" name="DropOutlet2">
                                                                    <option value="CMG">All</option>
                                                                <?php foreach ($dropOut as $p){
                                                                      echo "<option value='$p->Outlet'>$p->Outlet</option>"; ;}?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-2">Divisi</label>
                                                            <div class="col-md-4">
                                                                <select class="bs-select form-control" data-width="500px" name="DropDiv2">
                                                                    <option value="CMG">CMG</option>
                                                                    <option value="BBG">BBG</option>
                                                                    <option value="MBG">MBG</option>
                                                                    <option value="PWG">PWG</option>
                                                                    <option value="CHG">CHG</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <label class="control-label col-md-2"></label>
                                                            <div class="col-md-4">
                                                                <button type="submit" class="btn green">
                                                                    <i class="fa fa-check"></i> Cari </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                    <!-- BEGIN PAGE BASE CONTENT -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption font-dark">
                                                    <i class="icon-settings font-dark"></i>
                                                    <span class="caption-subject bold uppercase">Nasabah Kol. 2A</span>
                                                </div>
                                                <div class="tools"> </div>
                                            </div>
                                            <div class="portlet-body">
                                                <table class="table table-striped table-bordered table-hover" id="nasabahkol2">
                                                  <thead>
                                                   <tr><th>Noloan</th>
                                                       <th>NomorCIF</th> 
                                                       <th>Nama Nasabah</th> 
                                                       <th>Nama Cabang</th>
                                                       <th>Area</th>
                                                       <th>Regional</th>
                                                       <th>Divisi</th>
                                                       <th>KolLoan</th>
                                                       <th>KolCIF</th>
                                                       <th>OSPokok</th>
                                                       <th>Tunggakan</th>
                                                    </tr>
                                                  </thead>
                                                <tfoot>
                                                   <tr><th>Noloan</th>
                                                       <th>NomorCIF</th> 
                                                       <th>Nama Nasabah</th> 
                                                       <th>Nama Cabang</th>
                                                       <th>Area</th>
                                                       <th>Regional</th>
                                                       <th>Divisi</th>
                                                       <th>KolLoan</th>
                                                       <th>KolCIF</th>
                                                       <th>OSPokok</th>
                                                       <th>Tunggakan</th>
                                                    </tr>
                                                  </tfoot>
                                                  <tbody><?php foreach ($NasKol2A as $p){
                                                     echo "<tr><td>$p->NoLoan</td>
                                                      <td>$p->NomorCIF</td>
                                                      <td>$p->NamaLengkap</td>
                                                      <td>$p->NamaCabangNWG</td>
                                                      <td>$p->Area</td>
                                                      <td>$p->Regional</td>
                                                      <td>$p->Divisi</td>
                                                      <td style='text-align: center'>$p->KolLoan</td>
                                                      <td style='text-align: center'>$p->KolCIF</td>
                                                      <td style='text-align: right'>"; echo number_format($p->OSPokokConversion,2); echo "</td>
                                                      <td style='text-align: right'>"; echo number_format($p->TunggakanGrossConversion,2); echo "</td></tr>";
                                                      ;} ?>
                                                  </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END EXAMPLE TABLE PORTLET-->
                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->

                                        <!-- END EXAMPLE TABLE PORTLET-->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption font-dark">
                                                    <i class="icon-settings font-dark"></i>
                                                    <span class="caption-subject bold uppercase">Nasabah Kol. 2B</span>
                                                </div>
                                                <div class="tools"> </div>
                                            </div>
                                            <div class="portlet-body">
                                                <table class="table table-striped table-bordered table-hover" id="nasabahkol2B">
                                                  <thead>
                                                   <tr><th>Noloan</th>
                                                       <th>NomorCIF</th> 
                                                       <th>Nama Nasabah</th> 
                                                       <th>Nama Cabang</th>
                                                       <th>Area</th>
                                                       <th>Regional</th>
                                                       <th>Divisi</th>
                                                       <th>KolLoan</th>
                                                       <th>KolCIF</th>
                                                       <th>OSPokok</th>
                                                       <th>Tunggakan</th></tr>
                                                  </thead>
                                                  <tbody><?php foreach ($NasKol2B as $p){
                                                echo "<tr><td>$p->NoLoan</td>
                                                      <td>$p->NomorCIF</td>
                                                      <td>$p->NamaLengkap</td>
                                                      <td>$p->NamaCabangNWG</td>
                                                      <td>$p->Area</td>
                                                      <td>$p->Regional</td>
                                                      <td>$p->Divisi</td>
                                                      <td style='text-align: center'>$p->KolLoan</td>
                                                      <td style='text-align: center'>$p->KolCIF</td>
                                                      <td style='text-align: right'>"; echo number_format($p->OSPokokConversion,2); echo "</td>
                                                      <td style='text-align: right'>"; echo number_format($p->TunggakanGrossConversion,2); echo "</td></tr>";
                                                      ;} ?>
                                                  </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END EXAMPLE TABLE PORTLET-->
                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->

                                        <!-- END EXAMPLE TABLE PORTLET-->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption font-dark">
                                                    <i class="icon-settings font-dark"></i>
                                                    <span class="caption-subject bold uppercase">Nasabah Kol. 2C</span>
                                                </div>
                                                <div class="tools"> </div>
                                            </div>
                                            <div class="portlet-body">
                                                <table class="table table-striped table-bordered table-hover" id="nasabahkol2C">
                                                  <thead>
                                                   <tr><th>Noloan</th>
                                                       <th>NomorCIF</th> 
                                                       <th>Nama Nasabah</th> 
                                                       <th>Nama Cabang</th>
                                                       <th>Area</th>
                                                       <th>Regional</th>
                                                       <th>Divisi</th>
                                                       <th>KolLoan</th>
                                                       <th>KolCIF</th>
                                                       <th>OSPokok</th>
                                                       <th>Tunggakan</th></tr>
                                                  </thead>
                                                  <tbody><?php foreach ($NasKol2C as $p){
                                                echo "<tr><td>$p->NoLoan</td>
                                                      <td>$p->NomorCIF</td>
                                                      <td>$p->NamaLengkap</td>
                                                      <td>$p->NamaCabangNWG</td>
                                                      <td>$p->Area</td>
                                                      <td>$p->Regional</td>
                                                      <td>$p->Divisi</td>
                                                      <td style='text-align: center'>$p->KolLoan</td>
                                                      <td style='text-align: center'>$p->KolCIF</td>
                                                      <td style='text-align: right'>"; echo number_format($p->OSPokokConversion,2); echo "</td>
                                                      <td style='text-align: right'>"; echo number_format($p->TunggakanGrossConversion,2); echo "</td></tr>";
                                                      ;} ?>
                                                  </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END EXAMPLE TABLE PORTLET-->
                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->

                                        <!-- END EXAMPLE TABLE PORTLET-->
                                    </div>
                                </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- BEGIN FOOTER -->
                <p class="copyright"> 2016 &copy; Metronic Theme By
                    <a target="_blank" href="http://keenthemes.com">Keenthemes</a> &nbsp;|&nbsp;
                    <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
                </p>
                <a href="#index" class="go2top">
                    <i class="icon-arrow-up"></i>
                </a>
                <!-- END FOOTER -->
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN QUICK SIDEBAR -->


        <!-- END QUICK SIDEBAR -->
        <!--[if lt IE 9]>
<script src="<?php echo base_url('assets/global/plugins/respond.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/global/plugins/excanvas.min.js'); ?>"></script> 
<script src="<?php echo base_url('assets/global/plugins/ie8.fix.min.js'); ?>"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/js.cookie.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url('assets/global/scripts/datatable.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/datatables/datatables.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url('assets/global/scripts/app.min.js'); ?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
         <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url('assets/pages/scripts/components-bootstrap-select.min.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url('assets/layouts/layout5/scripts/layout.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/layouts/global/scripts/quick-sidebar.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/layouts/global/scripts/quick-nav.min.js'); ?>" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <!-- DATATABLE SCRIPT -->
        <script type="text/javascript">
                var initTableNasabahKol2 = function () {
                var table = $('#nasabahkol2 , #nasabahkol2B, #nasabahkol2C');

                var oTable = table.dataTable({

                    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
                    "language": {
                        "aria": {
                            "sortAscending": ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        },
                        "emptyTable": "No data available in table",
                        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                        "infoEmpty": "No entries found",
                        "infoFiltered": "(filtered1 from _MAX_ total entries)",
                        "lengthMenu": "_MENU_ entries",
                        "search": "Search:",
                        "zeroRecords": "No matching records found"
                    },

                    // Or you can use remote translation file
                    //"language": {
                    //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
                    //},


                    buttons: [
                        //{ extend: 'print', className: 'btn dark btn-outline' },
                        //{ extend: 'copy', className: 'btn red btn-outline' },
                        //{ extend: 'pdf', className: 'btn green btn-outline' },
                        { extend: 'excel', className: 'btn green btn-outline' },
                       //{ extend: 'csv', className: 'btn purple btn-outline ' },
                        { extend: 'colvis', className: 'btn purple btn-outline', text: 'Columns'}
                    ],

                    // setup responsive extension: http://datatables.net/extensions/responsive/
                    responsive: true,

                    //"ordering": false, disable column ordering 
                    //"paging": false, disable pagination

                    "order": [
                        [0, 'asc']
                    ],
                    
                    "lengthMenu": [
                        [5, 10, 15, 20, -1],
                        [5, 10, 15, 20, "All"] // change per page values here
                    ],
                    // set the initial value
                    "pageLength": 10,

                    "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

                    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
                    // So when dropdowns used the scrollable div should be removed. 
                    //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
                });
            }

            $(function(){
                initTableNasabahKol2();
            });

        </script>
    </body>

</html>