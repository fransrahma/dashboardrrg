<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Daftar Nasabah NPF</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>font/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 

  <header class="main-header"> 
    <a href="index2.html" class="logo">
      <span class="logo-mini"><b>RR</b>G</span>
      <span class="logo-lg"><b>RetailRisk</b>Group</span>
    </a>
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
      
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url(); ?>dist/img/avatar5.png" class="user-image" alt="User Image">
              <span class="hidden-xs">Sandy Nur Rahmat</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/avatar5.png" class="img-circle" alt="User Image">
                <p>
                  Sandy Nur Rahmat - Admin RRG
                  <small>MIS and Scoring Staff</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

<!--Menu Side Kiri-->
  <aside class="main-sidebar">
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>dist/img/avatar5.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Sandy Nur Rahmat</p>
          <a href="#"></i> MIS and Scoring Staff</a>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU DASHBOARD RRG</li>
        <li>
          <a href="#">
            <i class="fa fa-archive"></i> <span>Portofolio Pembiayaan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/Portofolio"><i class="fa fa-circle-o"></i>Portofolio Segmen</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Portofolio/portoregion"><i class="fa fa-circle-o"></i>Portofolio Regional</a></li>
          </ul>
        </li>
           
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cart-plus"></i> <span>Pencairan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/booking"><i class="fa fa-circle-o"></i>Rekap Pencairan Segmen</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/booking/bookingregion"><i class="fa fa-circle-o"></i>Rekap Pencairan Regional</a></li>
          </ul>
        </li>  

        <li class="treeview">
          <a href="#">
            <i class="fa fa-bar-chart"></i> <span>Kolektibilitas 2</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/Portofolio/Kol2Segmen"><i class="fa fa-circle-o"></i>Rekap Kol 2 Segmen</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Portofolio/Kol2region"><i class="fa fa-circle-o"></i>Rekap Kol 2 Regional</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/Kol2"><i class="fa fa-circle-o"></i>Daftar Nasabah Kol 2</a></li>
          </ul>
        </li> 

        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i> <span>Kolektibilitas NPF</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/Portofolio/NPFSegmen"><i class="fa fa-circle-o"></i>Rekap NPF Segmen</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Portofolio/NPFregion"><i class="fa fa-circle-o"></i>Rekap NPF Regional</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/NPF"><i class="fa fa-circle-o"></i>Daftar Nasabah NPF</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-area-chart"></i> <span>Rekap Downgrade</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/DownUp"><i class="fa fa-circle-o"></i>Rekap Downgrade Segmen</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/DownUp/DownRegion"><i class="fa fa-circle-o"></i>Rekap Downgrade Regional</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/Down"><i class="fa fa-circle-o"></i>Daftar Nasabah Downgrade</a></li>
          </ul>
        </li> 

        <li class="treeview">
          <a href="#">
            <i class="fa fa-line-chart"></i> <span>Rekap Upgrade</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/DownUp/UpgradeSegmen"><i class="fa fa-circle-o"></i>Rekap Upgrade Segmen</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/DownUp/UpgradeRegion"><i class="fa fa-circle-o"></i>Rekap Upgrade Regional</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/Upgrade"><i class="fa fa-circle-o"></i>Daftar Nasabah Upgrade</a></li>
          </ul>
        </li> 

        <li class="treeview">
          <a href="#">
            <i class="fa fa-list-alt"></i> <span>Nasabah PeKA</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/Nasabah/PeKA"><i class="fa fa-circle-o"></i>Regional 1 (Sumatra 1)</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/PeKA"><i class="fa fa-circle-o"></i>Regional 2 (Sumatra 2)</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/PeKA"><i class="fa fa-circle-o"></i>Regional 3 (Jakarta)</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/PeKA"><i class="fa fa-circle-o"></i>Regional 4 (Jawa 1)</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/PeKA"><i class="fa fa-circle-o"></i>Regional 5 (Jawa 2)</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/PeKA"><i class="fa fa-circle-o"></i>Regional 6 (Kalimantan)</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Nasabah/PeKA"><i class="fa fa-circle-o"></i>Regional 7 (Indo. Timur)</a></li>
          </ul>
        </li> 

        <li class="header">Admin Dashboard RRG</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Email</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Lync</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>EXT.</span></a></li>
      </ul>
    </section>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Data Nasabah NPF<small>Posisi 30 April 2017</small></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Nasabah</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header"><h3 class="box-title">Daftar Nasabah Kol 3</h3></div><!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
              <thead>
               <tr><th>Noloan</th>
                   <th>NomorCIF</th> 
                   <th>Nama Nasabah</th> 
                   <th>Nama Cabang</th>
                   <th>Area</th>
                   <th>Regional</th>
                   <th>Divisi</th>
                   <th>KolBulanLalu</th>
                   <th>KolLoan</th>
                   <th>KolCIF</th>
                   <th>OSPokok</th>
                   <th>Tunggakan</th></tr>
              </thead>
              <tbody><?php foreach ($NasDown as $p){
                 echo "<tr><td>$p->NoLoan</td>
                  <td>$p->NomorCIF</td>
                  <td>$p->NamaLengkap</td>
                  <td >$p->NamaCabangNWG</td>
                  <td>$p->Area</td>
                  <td>$p->Region</td>
                  <td>$p->Divisi</td>
                  <td style='text-align: center'>$p->KolBulanlalu</td>
                  <td style='text-align: center'>$p->KolLoan</td>
                  <td style='text-align: center'>$p->KolCIF</td>
                  <td style='text-align: right'>"; echo number_format($p->OSPokokConversion,2); echo "</td>
                  <td style='text-align: right'>"; echo number_format($p->TunggakanGrossConversion,2); echo "</td></tr>";
                  ;} ?>
              </tbody>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div> <!-- /.row -->
    </section>    
      
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $("#example2").DataTable();
    $('#example3').DataTable();//{"paging": true, "lengthChange": true,"searching": true,"ordering": true,"info": true,"autoWidth": true});
  });
</script>
</body>
</html>
