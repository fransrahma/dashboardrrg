<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Dashboard Retail Risk Group</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
      
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assets/global/plugins/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assets/global/plugins/simple-line-icons/simple-line-icons.min.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assets/global/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"); ?>" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url("assets/global/plugins/select2/css/select2.min.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assets/global/plugins/select2/css/select2-bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url("assets/global/css/components.min.css"); ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url("assets/global/css/plugins.min.css"); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo base_url("assets/pages/css/login-4.css"); ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
         <div class="logo" align="center">

        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<?php echo site_url('login/aksi_login'); ?>" method="post">
                <h3 class="form-title">Login ke Akun Anda </h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                </div>
                <div class="form-actions">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" /> Ingat saya
                        <span></span>
                    </label>
                    <button type="submit" class="btn green pull-right"> Masuk </button>
                </div>
                <div class="login-options">
                    
                </div>
                <div class="forget-password">
                    <h4>Lupa password anda ?</h4>
                    <p> Jangan Khawatir, klik
                        <a href="javascript:;" id="forget-password" > disini  </a> untuk reset password anda. </p>
                </div>
                <div class="create-account">
                    <p> Belum mempunyai akun  ?&nbsp;
                        <a href="javascript:;" id="register-btn"> Buat akun </a>
                    </p>
                </div>
            </form>

            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" action="<?php echo site_url('Recovery/forgot_password'); ?>" method="post">
                <p> Lupa Password ? </p>
                <h3>Silahkan hubungi</h3>
                <h4>1. Sandy Nur Rahmat (3531)</h4>
                <h4>2. Rahma Fitri (3531)</h4>
             
               
               <!--  <div class="form-group">
              
                    ie8, ie9 does not support html5 placeholder, so we just show field title for that
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>  
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" value="<?php echo set_value ('email');?>" /> </div> 
                </div>
                <input type="submit" name="btnSubmit" class="btn green pull-right" value="Submit" /></p>
                -->

                
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn red btn-outline">Kembali </button>
                    
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
            <!-- BEGIN REGISTRATION FORM -->
            <form class="register-form" action="<?php echo site_url('welcome/cek_regis'); ?>" method="post">

                <h3>Daftar</h3>
                <p> Masukan data pribadi anda: </p>
                
                 <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">NIP</label>
                    <div class="input-icon">
                        <i class="fa fa-joomla"></i>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="NIP" name="nip" type="text"  value="<?php echo set_value ('id');?>" /> </div>
                        
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Nama Lengkap</label>
                    <div class="input-icon">
                        <i class="fa fa-font"></i>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Nama Lengkap" name="fullname" type="text"  value="<?php echo set_value ('fullname');?>"/> </div>
                        
                </div>
               <div class="form-group">
               <div class="margin-top-10">
               
               <select class="form-control input-large" id="object_tagsinput_continent"  name="jeniskelamin" > <label class="control-label ">Jenis Kelamin</label>
                                  <option value="">Jenis Kelamin</option>
                                  <option value="Laki-laki">Laki-laki</option>
                                  <option value="Perempuan">Perempuan</option>
                            
                                  </select></div></div>
              <div class="form-group">
                                <div class="margin-top-10">

                                </select></div></div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" type="text"  value="<?php echo set_value ('email');?>" /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Nomor Telepon</label>
                    <div class="input-icon">
                        <i class="fa fa-mobile-phone"></i>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Nomor Telepon" name="phone" type="text"  value="<?php echo set_value ('phone');?>"/> </div>
                </div>
                 <div class="form-group">
                <div class="margin-top-10">
                <select class="form-control input-large" id="object_tagsinput_continent" name="jabatan" > <label class="control-label ">Jabatan</label>
                        <option value="">Jabatan</option>
                        <option value="Retail Risk Group Head">Retail Risk Group Head</option>
                        <option value="Analytical Decision Department Head">Analytical Decision Department Head</option>
                        <option value="Product Risk Management Department Head">Product Risk Management Department Head</option>
                        <option value="Scoring Management & MIS Department Head">Scoring Management & MIS Department Head</option>
                        <option value="Analytical Decision Team Leader">Analytical Decision Team Leader</option>
                        <option value="Analytical Decision Officer - Business & Micro Banking">Analytical Decision Officer - Business & Micro Banking</option>
                        <option value="Analytical Decision Officer - Consumer, Hajj, & Pawning">Analytical Decision Officer - Consumer, Hajj, & Pawning</option>
                        <option value="Analytical Decision Management Staff">Analytical Decision Management Staff</option>
                        <option value="Product Risk Team Leader - Business & Micro Banking">Product Risk Team Leader - Business & Micro Banking</option>
                        <option value="Product Risk Team Leader - Consumer, Hajj, & Pawning">Product Risk Team Leader - Consumer, Hajj, & Pawning</option>
                        <option value="Product Risk Officer - Business & Micro Banking">Product Risk Officer - Business & Micro Banking</option>
                        <option value="Product Risk Officer - Consumer, Hajj, & Pawning">Product Risk Officer - Consumer, Hajj, & Pawning</option>
                        <option value="Product Risk Management Staff">Product Risk Management Staff</option>
                        <option value="Management Information System Team Leader">Management Information System Team Leader</option>
                        <option value="Management Information System Officer">Management Information System Officer</option>
                        <option value="Scoring Officer">Scoring Officer</option>
                        <option value="Scoring Management & MIS Staff">Scoring Management & MIS Staff</option>
                        <option value="Regional Financing Risk & Recovery Manager">Regional Financing Risk & Recovery Manager</option>
                        <option value="Regional Financing Risk & Recovery Supervisor">Regional Financing Risk & Recovery Supervisor</option>
                        <option value="Regional Financing Risk & Recovery Staff">Regional Financing Risk & Recovery Staff</option>
                        <option value="Area Financing Risk Manager">Area Financing Risk Manager</option>
                        <option value="Retail Risk Officer">Retail Risk Officer</option>
                        <option value="Business Banking Verification Staff">Business Banking Verification Staff</option>
                        <option value="Consumer Verification Staff">Consumer Verification Staff</option>
                        <option value="Micro Analyst">Micro Analyst</option>
                </select>
                </div>
                </div>

                <div class="form-group">
                <div class="margin-top-10">
                <select class="form-control input-large" id="object_tagsinput_continent" name="Regional" > <label class="control-label ">Regional</label>
                        <option value="">Regional</option>
                        <option value="Kantor Pusat">Kantor Pusat</option>
                        <option value="Sumatra 1 (Medan)">Sumatra 1 (Medan)</option>
                        <option value="Sumatra 2 (Palembang)">Sumatra 2 (Palembang)</option>
                        <option value="Jakarta">Jakarta</option>
                        <option value="Jawa 1 (Bandung)">Jawa 1 (Bandung)</option>
                        <option value="Jawa 2 (Surabaya)">Jawa 2 (Surabaya)</option>
                        <option value="Kalimantan (Banjarmasin)">Kalimantan (Banjarmasin)</option>
                        <option value="Indonesia Timur (Makassar)">Indonesia Timur (Makassar)</option>
                </select>
               </div></div>

                <div class="form-group">
                <div class="margin-top-10">
                <select class="form-control input-large" id="object_tagsinput_continent" name="Area"> <label class="control-label"> Area </label>
                        <option value="">Area</option>
                        <option value="Kantor Pusat">Kantor Pusat</option>
                        <option value="Area Aceh">Area Aceh</option>
                        <option value="Area Batam">Area Batam</option>
                        <option value="Area Medan Ahmad Yani">Area Medan Ahmad Yani</option>
                        <option value="Area Medan Gajah Mada">Area Medan Gajah Mada</option>
                        <option value="Area Pekanbaru">Area Pekanbaru</option>
                        <option value="Area Pematangsiantar">Area Pematangsiantar</option>
                        <option value="Area Bandarlampung">Area Bandarlampung</option>
                        <option value="Area Jambi">Area Jambi</option>
                        <option value="Area Padang">Area Padang</option>
                        <option value="Area Palembang">Area Palembang</option>
                        <option value="Area Bekasi">Area Bekasi</option>
                        <option value="Area Bogor">Area Bogor</option>
                        <option value="Area Jakarta Hasanudin">Area Jakarta Hasanudin</option>
                        <option value="Area Jakarta Kebon jeruk">Area Jakarta Kebon jeruk</option>
                        <option value="Area Jakarta Kelapa Gading">Area Jakarta Kelapa Gading</option>
                        <option value="Area Jakarta Mayestik">Area Jakarta Mayestik</option>
                        <option value="Area Jakarta Pondok Kelapa">Area Jakarta Pondok Kelapa</option>
                        <option value="Area Jakarta Thamrin">Area Jakarta Thamrin</option>
                        <option value="Area Bandung Ahmad Yani">Area Bandung Ahmad Yani</option>
                        <option value="Area Bandung Dago">Area Bandung Dago</option>
                        <option value="Area Cirebon">Area Cirebon</option>
                        <option value="Area Semarang">Area Semarang</option>
                        <option value="Area Solo">Area Solo</option>
                        <option value="Area yogyakarta">Area yogyakarta</option>
                        <option value="Area Denpasar">Area Denpasar</option>
                        <option value="Area Jember">Area Jember</option>
                        <option value="Area Malang">Area Malang</option>
                        <option value="Area Surabaya Darmo">Area Surabaya Darmo</option>
                        <option value="Area Surabaya Jemur Handayani">Area Surabaya Jemur Handayani</option>
                        <option value="Area Balikpapan">Area Balikpapan</option>
                        <option value="Area Banjarmasin">Area Banjarmasin</option>
                        <option value="Area Pontianak">Area Pontianak</option>
                        <option value="Area Jayapura">Area Jayapura</option>
                        <option value="Area Makassar">Area Makassar</option>
                        <option value="Area Palu">Area Palu</option>
                </select>
                

              
                                                                
                    </div>
                <p> Masukkan detail akun anda: </p>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" type="text"  value="<?php echo set_value ('username');?>"  /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" type="text"  value="<?php echo set_value ('password');?>"/> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Ketik ulang password anda</label>
                    <div class="controls">
                        <div class="input-icon">
                            <i class="fa fa-check"></i>
                            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Ketik ulang password anda" name="rpassword" /> </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="tnc" /> Saya menyetujui
                        <a href="javascript:;">Syarat & Ketentuan </a> 
                        <span></span>
                    </label>
                    <div id="register_tnc_error"> </div>
                </div>
                <div class="form-actions">
                    <button id="register-back-btn" type="button" class="btn red btn-outline"> Kembali </button>
                    <button type="submit" id="register-submit-btn" class="btn green pull-right" > Daftar </button>
                </div>
            </form>
            <!-- END REGISTRATION FORM -->
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright"> Retail Risk  Group 2017 </div>
        <!-- END COPYRIGHT -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/js.cookie.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/additional-methods.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/select2/js/select2.full.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/backstretch/jquery.backstretch.min.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url('assets/global/scripts/app.min.js'); ?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
       
        <script src="<?php echo base_url('assets/pages/scripts/login-4.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
       
    </body>

</html>