<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Review Nasabah | RRG</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #5 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
         <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet'); ?>" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/global/plugins/bootstrap-summernote/summernote.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url('assets/global/css/components.min.css'); ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url('assets/global/css/plugins.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url('assets/layouts/layout5/css/layout.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/layouts/layout5/css/custom.min.css'); ?>" rel="stylesheet" type="text/css" />
       
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN CONTAINER -->
        <div class="wrapper">
            <!-- BEGIN HEADER -->
           <?php echo $header; ?>
            <!-- END HEADER MENU -->
                    <div class="container-fluid">
                <div class="page-content">
                    <!-- BEGIN BREADCRUMBS -->
                    <div class="breadcrumbs">
                        <h1>Nasabah 3 Pilar</h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">Daftar Nasabah</a>
                            </li>
                            <li>
                                <a href="#">Nasabah 3 Pilar</a>
                            </li>
                            <li class="active"> <a href ="<?php echo site_url('#') ?>" >Review </a></li>
                        </ol>
                    </div>

                    <!-- END BREADCRUMBS -->
                 <!-- BEGIN PAGE TITLE-->
                        
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabbable-line boxless tabbable-reversed">
                                  
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_0">
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i>Review Nasabah </div>
                                                    
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form class="form-horizontal" action="<?php echo site_url('DaftarNasabah/proceed_review/'.$Data->JenisPiutangPembiayaan); ?>" id="formreview" method="post">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">No Loan</label>
                                                                <div class="col-md-10">
                                                                <input class="form-control input-circle" readonly placeholder="Noloan" name="NoLoan" type="text"  value="<?php echo $Data->NoLoan; ?>"/>
                                            
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Nama Lengkap</label>
                                                                <div class="col-md-10">
                                                                <input class="form-control input-circle" readonly placeholder="NamaLengkap" name="NamaLengkap" type="text"  value="<?php echo $Data->NamaLengkap; ?>"/> 
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Skema Pembiayaan</label>
                                                                <div class="col-md-10">
                                                                <input class="form-control input-circle" readonly placeholder="Skema Pembiayaan" name="SkemaPembiayaan" type="text"  value="<?php echo $Data->JenisPiutangPembiayaan; ?>"/> 
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Faktor Penilaian</label>
                                                                <div class="col-md-10">
                                                                <div class="portlet box yellow">
                                                                    <div class="portlet-title">
                                                                                    <div class="caption">
                                                                                        <i class="fa fa-comments"></i>Prospek Usaha </div>
                                                                                    <div class="tools">
                                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="portlet-body">
                                                                                    <div class="table-scrollable">
                                                                                        <table class="table table-bordered table-hover">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th> # </th>
                                                                                                    <th> Faktor Penilaian </th>
                                                                                                    <?php foreach ($value_labels as $key => $value) { ?>      
                                                                                                    <th> <?php echo $value->alias ?></th>
                                                                                                    <?php } ?>
                                                                                                    <th>Catatan</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <?php foreach ($factors_1 as $key => $value) { ?>
                                                                                                <tr>
                                                                                                    <td> <?php echo $key+1 ?> </td>
                                                                                                    <td><?php echo $value->factor_point; ?></td>
                                                                                                    <?php foreach ($value->options as $key1 => $opt) { ?>
                                                                                                        <td>
                                                                                                        <?php foreach ($opt as $key2 => $value2) { ?>
                                                                                                        <input type="radio" name="<?php echo "prospek_".$value->scheme_id."_".$value->factor_id."_".$value->id ?>" value="<?php echo $value2['value']."_".$value->id."_".$value2['id'] ?>"><?php echo $value2['label'] ?></br></br>
                                                                                                        <?php } ?>
                                                                                                        </td>
                                                                                                     <?php  }?>
                                                                                                     <td><textarea name="<?php echo "noteprospek_".$value->scheme_id."_".$value->factor_id."_".$value->id ?>"></textarea></td>
                                                                                            <?php } ?>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- END SAMPLE TABLE PORTLET-->
                                                                            <div class="portlet box yellow">
                                                                    <div class="portlet-title">
                                                                                    <div class="caption">
                                                                                        <i class="fa fa-comments"></i>Kinerja (Performance) Nasabah </div>
                                                                                    <div class="tools">
                                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="portlet-body">
                                                                                    <div class="table-scrollable">
                                                                                    <table class="table table-bordered table-hover">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th> # </th>
                                                                                                    <th> Faktor Penilaian </th>
                                                                                                    <?php foreach ($value_labels as $key => $value) { ?>                                                                                               
                                                                                                    <th> <?php echo $value->alias ?></th>
                                                                                                    <?php } ?>
                                                                                                     <th>Catatan</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <?php foreach ($factors_2 as $key => $value) { ?>
                                                                                                <tr>
                                                                                                    <td> <?php echo $key+1 ?> </td>
                                                                                                    <td><?php echo $value->factor_point; ?></td>
                                                                                                    <?php foreach ($value->options as $key1 => $opt) { ?>
                                                                                                        <td>
                                                                                                        <?php foreach ($opt as $key2 => $value2) { ?>
                                                                                                        <input type="radio" name="<?php echo "kinerja_".$value->scheme_id."_".$value->factor_id."_".$value->id ?>" value="<?php echo $value2['value']."_".$value->id."_".$value2['id'] ?>"><?php echo $value2['label'] ?></br></br>
                                                                                                        <?php } ?>
                                                                                                        </td>
                                                                                                     <?php  }?>
                                                                                                     <td><textarea name="<?php echo "notekinerja_".$value->scheme_id."_".$value->factor_id."_".$value->id ?>"></textarea></td>
                                                                                            <?php } ?>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- END SAMPLE TABLE PORTLET-->
                                                                            <div class="portlet box yellow">
                                                                    <div class="portlet-title">
                                                                                    <div class="caption">
                                                                                        <i class="fa fa-comments"></i>Kemampuan Bayar </div>
                                                                                    <div class="tools">
                                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="portlet-body">
                                                                                    <div class="table-scrollable">
                                                                                    <table class="table table-bordered table-hover">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th> # </th>
                                                                                                    <th> Faktor Penilaian </th>
                                                                                                    <?php foreach ($value_labels as $key => $value) { ?>                                                                                               
                                                                                                    <th> <?php echo $value->alias ?></th>
                                                                                                    <?php } ?>
                                                                                                    <th>Catatan</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <?php foreach ($factors_3 as $key => $value) { ?>
                                                                                                <tr>
                                                                                                    <td> <?php echo $key+1 ?> </td>
                                                                                                    <td><?php echo $value->factor_point; ?></td>
                                                                                                    <?php foreach ($value->options as $key1 => $opt) { ?>
                                                                                                        <td>
                                                                                                        <?php foreach ($opt as $key2 => $value2) { ?>
                                                                                                        <input type="radio" name="<?php echo "bayar_".$value->scheme_id."_".$value->factor_id."_".$value->id ?>" value="<?php echo $value2['value']."_".$value->id."_".$value2['id'] ?>"><?php echo $value2['label'] ?></br></br>
                                                                                                        <?php } ?>
                                                                                                        </td>
                                                                                                     <?php  }?>
                                                                                                     <td><textarea name="<?php echo "notebayar_".$value->scheme_id."_".$value->factor_id."_".$value->id ?>"></textarea></td>
                                                                                            <?php } ?>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- END SAMPLE TABLE PORTLET-->
                                                                </div>
                                                            </div>                                                            
                                                            <div class="form-actions fluid">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn green">Submit</button>
                                                                    <a href="<?php echo site_url('DaftarNasabah/Review3Pilar') ?>" class="btn default">Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                        <!-- END EXTRAS PORTLET-->
                                    </div>
                                </div>
                                <!-- END PAGE BASE CONTENT -->
                        
                    
  
        <!-- END QUICK NAV -->
        <div class="modal fade" id="reviewmodal" tabindex="-1" role="basic" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                <h4 class="modal-title">Review Nasabah</h4>
                                                            </div>
                                                            <div class="modal-body"> Review hanya bisa dilakukan sekali. Pastikan data yang anda masukan benar. </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
                                                                <button type="button" class="btn green" onclick="submit_review()">Submit</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <div class="modal fade" id="responsemodal" tabindex="-1" role="basic" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                <h4 class="modal-title">Review Nasabah</h4>
                                                            </div>
                                                            <div class="modal-body"> </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Ok</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>                                                
        <!--[if lt IE 9]>
        <script src="<?php echo base_url('assets/global/plugins/respond.min.js'); ?>"</script>
        <script src="<?php echo base_url('assets/global/plugins/excanvas.min.js'); ?>"</script> 
        <script src="<?php echo base_url('assets/global/plugins/ie8.fix.min.js'); ?>"</script> 
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/js.cookie.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/ladda/spin.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/ladda/ladda.min.js');?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url('assets/global/scripts/app.min.js'); ?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url('assets/layouts/layout5/scripts/layout.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/layouts/global/scripts/quick-sidebar.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/layouts/global/scripts/quick-nav.min.js'); ?>" type="text/javascript"></script>

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script type="text/javascript">
            
        var modalState = 0;

        $("#responsemodal").on('hidden.bs.modal',function(){
            if(modalState==1){
                window.location="<?php echo site_url('DaftarNasabah/Review3Pilar') ?>";
            }
        });

        $(document).ready(function(){
            if (!("FormData" in window)) {
                alert('update  browser anda untuk menggunakan aplikasi ini');
            }

            $("#formreview").submit(function(evt){
                evt.preventDefault();
                $("#reviewmodal").modal("show");
            });

        });

        function submit_review(){

            $("#reviewmodal").modal("hide");

            var data = $("#formreview").serialize();
            var url = $("form#formreview").attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType : 'json',
                success: function(data) {
                    $("#responsemodal").find(".modal-body").html(data.message);
                    $("#responsemodal").modal("show");

                    if( data.statusCode=="00" ){
                        modalState = 1;
                    }
                }
            });  
 
        }


            
        </script>
    </body>
    </html>