<?php

if (!function_exists('parse_review_value')) {

    function parse_review_value($prospek_vals,$kinerja_vals,$bayar_vals){
		$prospek_realvals = array();
		$kinerja_realvals = array();
		$bayar_realvals = array();

		//parse prospek usaha
		array_walk($prospek_vals,function($value,$key) use(&$prospek_realvals){
			$tmp = explode("_",$value)[0];
			array_push($prospek_realvals,$tmp);
		});

		//parse kinerja
		array_walk($kinerja_vals,function($value,$key) use(&$kinerja_realvals){
			$tmp = explode("_",$value)[0];
			array_push($kinerja_realvals,$tmp);
		});

		//parse kemampuan bayar
		array_walk($bayar_vals,function($value,$key) use(&$bayar_realvals){
			$tmp = explode("_",$value)[0];
			array_push($bayar_realvals,$tmp);
		});
		
		//descending sort all values to get highest value
		rsort($prospek_realvals);
		rsort($kinerja_realvals);
		rsort($bayar_realvals);

		//compare all value from factors to get lower Kol
		$compare = array(
			$prospek_realvals[0],$kinerja_realvals[0],$bayar_realvals[0]
		);

		rsort($compare);

		//return final kol
		return $compare[0];

	}


	function get_kol_summary($prospek_vals,$kinerja_vals,$bayar_vals){
		$prospek_realvals = array();
		$kinerja_realvals = array();
		$bayar_realvals = array();

		//parse prospek usaha
		array_walk($prospek_vals,function($value,$key) use(&$prospek_realvals){
			$tmp = explode("_",$value)[0];
			array_push($prospek_realvals,$tmp);
		});

		//parse kinerja
		array_walk($kinerja_vals,function($value,$key) use(&$kinerja_realvals){
			$tmp = explode("_",$value)[0];
			array_push($kinerja_realvals,$tmp);
		});

		//parse kemampuan bayar
		array_walk($bayar_vals,function($value,$key) use(&$bayar_realvals){
			$tmp = explode("_",$value)[0];
			array_push($bayar_realvals,$tmp);
		});
		
		//descending sort all values to get highest value
		rsort($prospek_realvals);
		rsort($kinerja_realvals);
		rsort($bayar_realvals);

		//compare all value from factors to get lower Kol
		$compare = array(
			$prospek_realvals[0],$kinerja_realvals[0],$bayar_realvals[0]
		);

		rsort($compare);

		//return final kol
		return [
			'pilar1' => $prospek_realvals[0],
			'pilar2' => $kinerja_realvals[0],
			'pilar3' => $bayar_realvals[0],
			'final' => $compare[0]
		];

	}

}