<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Page extends CI_Controller{

	function p404(){
		$this->load->view('errors/page_system_404_2');
	}

	function p500(){
		$this->load->view('errors/page_system_500_2');
	}

	function maintenance(){
		$this->load->view('errors/page_system_coming_soon');
	}
}