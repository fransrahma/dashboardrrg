<?php

class Nasabah extends CI_Controller {

	
	public function index()
	{
		$this->load->helper('url');
		$this->load->view('Nasabah');
		
	}
	public function Kol2()
	{
		
		$this->load->helper('url');
		$this->load->model('M_Nasabah');

		$noreg = $this->session->userdata("akses");
		if ($noreg=='Akses Area'){
			$where2A    = array('Area'=>$this->session->userdata("area"),'KolCIF AS KolCIF2A'=>'2A');
			$where2B    = array('Area'=>$this->session->userdata("area"),'KolCIF AS KolCIF2B'=>'2B');
			$where2C    = array('Area'=>$this->session->userdata("area"),'KolCIF AS KolCIF2C'=>'2C');
		} else if($noreg=='Akses Region'){
			$where2A    = array('Regional'=>$this->session->userdata("regional"),'KolCIF AS KolCIF2A'=>'2A');
			$where2B    = array('Regional'=>$this->session->userdata("regional"),'KolCIF AS KolCIF2B'=>'2B');
			$where2C    = array('Regional'=>$this->session->userdata("regional"),'KolCIF AS KolCIF2C'=>'2C');
		} else if($noreg=='Akses All'){
			$where2A    = array('KolCIF AS KolCIF2A'=>'2A');
			$where2B    = array('KolCIF AS KolCIF2B'=>'2B');
			$where2C    = array('KolCIF AS KolCIF2C'=>'2C');
		}else if($noreg=='Admin'){
			$where2A    = array('KolCIF AS KolCIF2A'=>'2A');
			$where2B    = array('KolCIF AS KolCIF2B'=>'2B');
			$where2C    = array('KolCIF AS KolCIF2C'=>'2C');
		}else {
			$where2A    = array('KolCIF AS KolCIF2A'=>'2A');
			$where2B    = array('KolCIF AS KolCIF2B'=>'2B');
			$where2C    = array('KolCIF AS KolCIF2C'=>'2C');
		}

		$select['dash']   ="dropdown dropdown-fw dropdown-fw-disabled";
		$select['nas']    ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		$select['Tar']    ="dropdown dropdown-fw dropdown-fw-disabled";
		$Data['Judul']  ="Daftar Nasabah Kol. 2";
		$Data['Judul1'] ="Daftar Nasabah Kol. 2A";
		$Data['Judul2'] ="Daftar Nasabah Kol. 2B";
		$Data['Judul3'] ="Daftar Nasabah Kol. 2C";
		$Data['TglH']     = $this->M_Nasabah->Portoharian()->result();
		$Data['NasKol2A'] = $this->M_Nasabah->Daftar2A("DaftarNasabahLoan AS DaftarNasabahLoan2A",$where2A)->result();
		$Data['NasKol2B'] = $this->M_Nasabah->Daftar("DaftarNasabahLoan AS DaftarNasabahLoan2B",$where2B)->result();
		$Data['NasKol2C'] = $this->M_Nasabah->Daftar("DaftarNasabahLoan AS DaftarNasabahLoan2C" ,$where2C)->result();
		//$Data['allPIL']     = $allReg;

		$Data['header'] = $this->load->view('templates/headerV2',null,true);
		$this->load->view('nasabahkol',$Data);
		
	}

	
	public function NPF()
	{
		$this->load->helper('url');
		$this->load->model('M_Nasabah');

		$noreg = $this->session->userdata("akses");
		if ($noreg=='Akses Area'){
			$where3    = array('Area'=>$this->session->userdata("area"),'KolCIF AS KolCIF3'=>'3');
			$where4    = array('Area'=>$this->session->userdata("area"),'KolCIF AS KolCIF4'=>'4');
			$where5    = array('Area'=>$this->session->userdata("area"),'KolCIF AS KolCIF5'=>'5');
		} else if($noreg=='Akses Region'){
			$where3    = array('Regional'=>$this->session->userdata("regional"),'KolCIF AS KolCIF3'=>'3');
			$where4    = array('Regional'=>$this->session->userdata("regional"),'KolCIF AS KolCIF4'=>'4');
			$where5    = array('Regional'=>$this->session->userdata("regional"),'KolCIF AS KolCIF5'=>'5');
		} else if($noreg=='Akses All'){
			$where3    = array('KolCIF AS KolCIF3'=>'3');
			$where4    = array('KolCIF AS KolCIF4'=>'4');
			$where5    = array('KolCIF AS KolCIF5'=>'5');
		}else if($noreg=='Admin'){
			$where3    = array('KolCIF AS KolCIF3'=>'3');
			$where4    = array('KolCIF AS KolCIF4'=>'4');
			$where5    = array('KolCIF AS KolCIF5'=>'5');
		}else {
			$where3    = array('KolCIF'=>'2');
			$where4    = array('KolCIF'=>'2');
			$where5    = array('KolCIF'=>'2');
		}

		$select['dash']   ="dropdown dropdown-fw dropdown-fw-disabled";
		$select['nas']    ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		$select['Tar']    ="dropdown dropdown-fw dropdown-fw-disabled";
		$Data['Judul']  ="Daftar Nasabah Kol. NPF";
		$Data['Judul1'] ="Daftar Nasabah Kol. 3";
		$Data['Judul2'] ="Daftar Nasabah Kol. 4";
		$Data['Judul3'] ="Daftar Nasabah Kol. 5";
		$Data['TglH']     = $this->M_Nasabah->Portoharian()->result();
		$Data['NasKol2A'] = $this->M_Nasabah->Daftar("DaftarNasabahLoan AS DaftarNasabahLoan2A",$where3)->result();
		$Data['NasKol2B'] = $this->M_Nasabah->Daftar("DaftarNasabahLoan AS DaftarNasabahLoan2A",$where4)->result();
		$Data['NasKol2C'] = $this->M_Nasabah->Daftar("DaftarNasabahLoan AS DaftarNasabahLoan2A",$where5)->result();
		//$Data['allPIL']     = $allReg;

		$Data['header'] = $this->load->view('templates/headerV2',null,true);
		$this->load->view('nasabahkol',$Data);
		
	}
	public function Down()
	{
		$this->uri->segment(2);
		$this->load->helper('url');
		$this->load->model('M_Nasabah');
		$select['dash'] ="dropdown dropdown-fw dropdown-fw-disabled";
		$select['nas']  ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		$select['Tar']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$Data['NasDown']     =$this->M_Nasabah->MNasabahDown()->result();
		$Data['header'] = $this->load->view('templates/headerV2',null,true);
		$this->load->view('nasabahdown',$Data);
	}
	public function Upgrade()
	{ 
		$this->uri->segment(2);
		$this->load->helper('url');
		$this->load->model('M_Nasabah');
		$select['dash'] ="dropdown dropdown-fw dropdown-fw-disabled";
		$select['nas']  ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		$select['Tar']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$Data['NasUpg']     =$this->M_Nasabah->MNasabahUpg()->result();
		$Data['header'] = $this->load->view('templates/headerV2',null,true);
		$this->load->view('nasabahupgrade',$Data);
	}
	public function PeKA()
	{
		$this->uri->segment(2);
		$this->load->helper('url');
		$select['dash'] ="dropdown dropdown-fw dropdown-fw-disabled";
		$select['nas']  ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		$select['Tar']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$header = $this->load->view('templates/headerV2',null,true);
		$this->load->view('NasabahPeKA');
		
	}
}
