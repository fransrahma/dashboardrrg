<?php 
class Update_post extends CI_Controller
{
	public function __construct ()
	{
		parent::__construct()
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('M_editor/edit_post');
	}

	function index()
	{
		$data('post') = $this->M_editor->get_post();
		
	}
	
}
