<?php

class Produk extends CI_Controller {
   
   
	public function Kol2d()
	{
		$this->load->helper('url');
		$this->load->model('M_Nasabah');

		$noreg = $this->session->userdata("akses");
		if ($noreg=='Akses Area'){
			$where2A    = array('Area'=>$this->session->userdata("area"),'KolCIF'=>'2A');
			$where2B    = array('Area'=>$this->session->userdata("area"),'KolCIF'=>'2B');
			$where2C    = array('Area'=>$this->session->userdata("area"),'KolCIF'=>'2C');
			$dropRegion = array('Regional'=>$this->session->userdata("regional"),'no'=>$this->session->userdata("id_regional"));
			$dropArea   = array('Area'=>$this->session->userdata("area"));
			$dropOut    = array('Regional'=>$this->session->userdata("regional"),'Area'=>$this->session->userdata("area"));
			//$allreg     ="";
		} else if($noreg=='Akses Region'){
			$where2A    = array('Regional'=>$this->session->userdata("regional"),'KolCIF'=>'2A');
			$where2B    = array('Regional'=>$this->session->userdata("regional"),'KolCIF'=>'2B');
			$where2C    = array('Regional'=>$this->session->userdata("regional"),'KolCIF'=>'2C');
			$dropRegion = array('Regional'=>$this->session->userdata("regional"),'no'=>$this->session->userdata("id_regional"));
			$dropArea   = array('Regional'=>$this->session->userdata("regional"));
			$dropOut    = array('Regional'=>$this->session->userdata("regional"),'UnitKerja'=>'RegionalOffice');
			//$allreg     ="";
		} else if($noreg=='Akses All'){
			$where2A    = array('KolCIF'=>'2A');
			$where2B    = array('KolCIF'=>'2B');
			$where2C    = array('KolCIF'=>'2C');
			$dropRegion = array('UnitKerja'=>'RegionalOffice');
			$dropArea   = array('UnitKerja'=>'RegionalOffice');
			$dropOut    = array('UnitKerja'=>'RegionalOffice');
			//$allReg     = "<option value='Allreg'>All Regional</option>";
		}else if($noreg=='Admin'){
			$where2A    = array('KolCIF'=>'2A');
			$where2B    = array('KolCIF'=>'2B');
			$where2C    = array('KolCIF'=>'2C');
			$dropRegion = array('UnitKerja'=>'RegionalOffice','no'=>$this->session->userdata("id_regional"));
			$dropArea   = array('UnitKerja'=>'RegionalOffice');
			$dropOut    = array('UnitKerja'=>'RegionalOffice');
			//$allReg     = "<option value='Allreg'>All Regional</option>";
		}else {
			$where2A    = array('KolCIF'=>'2');
			$where2B    = array('KolCIF'=>'2');
			$where2C    = array('KolCIF'=>'2');
		}

		$select['dash']   ="dropdown dropdown-fw dropdown-fw-disabled";
		$select['nas']    ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		$select['Tar']    ="dropdown dropdown-fw dropdown-fw-disabled";
		$Data['TglH']     = $this->M_Nasabah->Portoharian()->result();
		$Data['dropReg']  = $this->M_Nasabah->Daftar("ListRegional",$dropRegion)->result();
		$Data['dropAr']   = $this->M_Nasabah->Daftar("ListRegionArea",$dropArea)->result();
		$Data['dropOut']  = $this->M_Nasabah->Daftar("V_ListCabang",$dropOut)->result();
		$Data['NasKol2A'] = $this->M_Nasabah->Daftar("DaftarNasabahLoan",$where2A)->result();
		$Data['NasKol2B'] = $this->M_Nasabah->Daftar("DaftarNasabahLoan",$where2B)->result();
		$Data['NasKol2C'] = $this->M_Nasabah->Daftar("DaftarNasabahLoan",$where2C)->result();
		//$Data['allPIL']     = $allReg;

		$Data['header'] = $this->load->view('templates/headerV2',null,true);
		$this->load->view('old/BacaDatabase',$Data);
		
	}

	public function CariKol2d()
	{
		$this->load->helper('url');
		$this->load->model('M_Nasabah');

		$GetDropRegion = $this->input->get('DropReg2');
		$GetDropArea   = $this->input->get('DropArea2');
		$GetDropOutlet = $this->input->get('DropOutlet2');
		$GetDropDivisi = $this->input->get('DropDiv2');

		$where2A2    = array('Regional'=>$GetDropRegion,'Area'=>$GetDropArea,'NamaCabangNWG'=>$GetDropOutlet,'Divisi',$GetDropDivisi,'KolCIF'=>'2A');
		$where2B2    = array('Regional'=>$GetDropRegion,'Area'=>$GetDropArea,'NamaCabangNWG'=>$GetDropOutlet,'Divisi',$GetDropDivisi,'KolCIF'=>'2B');
		$where2C2    = array('Regional'=>$GetDropRegion,'Area'=>$GetDropArea,'NamaCabangNWG'=>$GetDropOutlet,'Divisi',$GetDropDivisi,'KolCIF'=>'2C');


		//$Data['NasKol2A'] = $this->M_Nasabah->Daftar("DaftarNasabahLoan",$where2A2)->result();
		//$Data['NasKol2B'] = $this->M_Nasabah->Daftar("DaftarNasabahLoan",$where2B2)->result();
		//$Data['NasKol2C'] = $this->M_Nasabah->Daftar("DaftarNasabahLoan",$where2C2)->result();

		//$Data['header'] = $this->load->view('templates/headerV2',null,true);
		//$this->load->view('nasabahkol2new',$Data);
		$this->load->view('old/Report',$where2A2);

	}
}
	?>

