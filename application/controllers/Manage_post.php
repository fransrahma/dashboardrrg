<?php

class Manage_post extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->helper('url');
		$this->load->model('M_post');
		
	}

	public function index()
	{
		
		$data['Posting']     =$this->M_post->Manage_post()->result();
		//var_dump($data);
		$data['header'] = $this->load->view('templates/headerV2',null,true);
		$this->load->view('manage_post/list',$data);
		
	}

	public function new_post(){
		$header = $this->load->view('templates/headerV2',null,true);
		$this->load->view('manage_post/add',compact('header'));
	}

	public function save()
	{
		//$this->load->view('Manage_post',$data);
		
		$this->load->model('M_editor');
		$this->form_validation->set_rules('judul_post','judul_post','required');
		$this->form_validation->set_rules('isi_post','isi post','required');
		//$this->form_validation->set_rules('tgl_post','tgl_post','required');
		//$this->form_validation->set_rules('author','author','required');
	

		if($this->form_validation->run() == FALSE)
		{	
			//var_dump($_POST);
			validation_errors();
		}
		else
		{
			//$data['id_post']	= null;
			$data['judul_post'] = $this->input->post('judul_post');
			$data['isi_post'] 	= $this->input->post('isi_post');
			$data['tgl_post'] 	= date('Y-m-d H:i:s ');
			$data['author'] 	= $this->session->userdata('fullname');

			$header = $this->load->view('templates/headerV2',null,true);

			$this->M_editor->post_baru($data);
			redirect('Manage_post');
		}
		
	}

	function viewAccount()
	{
		$this->form_validation->set_rules('judul_post','judul_post','required');
		$this->form_validation->set_rules('isi_post','isi post','required');
		//$this->form_validation->set_rules('tgl_post','tgl_post','required');
		//$this->form_validation->set_rules('author','author','required');
	

		if($this->form_validation->run() == FALSE)
		{	
			//var_dump($_POST);
			validation_errors();
		}
		else
		{
			//$data['id_post']	= null;
			$data['judul_post'] = $this->input->post('judul_post');
			$data['isi_post'] 	= $this->input->post('isi_post');
			//$data['tgl_post'] 	= date('Y-m-d H:i:s ');
			//$data['author'] 	= $this->session->userdata('fullname');

			$header = $this->load->view('templates/headerV2',null,true);

			$this->M_post->updatePost($data,$this->input->post('id_post'));
			redirect('Manage_post');
		}
	}

	public function edit($id){
		$post = $this->M_post->getBlogPostByID($id)[0];
		$header = $this->load->view('templates/headerV2',null,true);

		if( $post != FALSE){
			$this->load->view('manage_post/edit',compact('header','post'));
		}
		else{
			$this->session->set_flashdata('return_page',site_url('manage_post'));
			redirect('error404');
		}


	}

	public function delete($id){
		$this->M_post->deletePost($id);
		redirect('manage_post');
	}
}