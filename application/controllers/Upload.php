<?php

class Upload extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                //$this->load->helper(array('form', 'url'));
                $this->load->model(array('M_Report'));
        }

        public function index()
        {
              
                $data['header'] = $this->load->view('templates/headerV2',null,true);
                $this->load->view('upload_report',$data);
        }

        public function do_upload()
        {
                //ini yang benar

                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'xls|xlsx|pdf';
                $config['max_size']             = 1000;

                $this->load->library('upload');
                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('fileToUpload'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $error['header'] = $this->load->view('templates/headerV2',null,true);
                        $this->load->view('upload_report', $error);
                }
                else
                {

                        $attribute['tipe'] = $this->input->post('tipe');
                        $attribute['desc'] = $this->input->post('desc'); 
                        $attribute['filename'] = $this->upload->data('file_name'); 
                        $attribute['path'] = base_url("//uploads//".$this->upload->data('file_name')); 

                        $this->M_Report->persist($attribute);

                        $data = array('upload_data' => $this->upload->data());
                        $data['header'] = $this->load->view('templates/headerV2',null,true);

                        $this->load->view('upload_success', $data);
                }
        }
}
?>