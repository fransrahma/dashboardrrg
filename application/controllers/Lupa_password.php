<?php 
class Lupa_password extends CI_Controller
{
	public function index()
	{
		$this->form_validation->set_rules('email','Email','required|valid_email');
		if($this->form_validation->run() == FALSE){
			$data['title'] = 'Halaman Reset Password';
			$this->load->view('account/lupa_password',$data);
		}
		else{
			$email = $this->input->post('email');
			$clean = $this->security->xss_clean($email);
			$userInfo = $this->m_account->getUserInfobyEmail($clean);
			if (!userInfo){
				$this->session->set_flashdata('sukses','email address salah, silahkan coba lagi.');
				redirect (site_url('login'),'refresh');
			}
			//build token
			$token = $this->m_account->insertToken($userInfo->nip);
			$qstring = $this->base64url_encode($token);
			$url = site_url().'/lupa_password/reset_password/token'.$qstring;
			$link = '<a href="'.$url.'">'.$url.'</a>';
			$message = '';
			$message .='<strong> Silahkan klik link ini:</strong>'.$link;
			echo $message; //send this through mail
			exit;
		}
	}
	function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->model('m_account');
	}
	public function Reset_password()
	{
		$token = $this->base64_decode($this->uri->segment(4));
		$cleanToken = $this->security->xss_clean($token);
		$userInfo = $this->m_account->isTokenValid($cleanToken);//either false or array();
		if (!userInfo){
				$this->session->set_flashdata('sukses','Token tidak valid atau kadaluarsa.');
				redirect (site_url('login'),'refresh');
		}
				$data = array (
				'title' => 'Halaman Reset Password',
				'nama' => $userInfo->fullname,
				'email' =>$userInfo->email,
				'token' =>$this->base64_encode($token)
			);
				$this->form_validation->set_rules('password','Password','required|min_length[5]');
				$this->form_validation->set_rules('passconf','Password confirmation','required|matches[password]');
				if ($this->form_validation->run() == FALSE) {
					$this->load->view('account/reset_password',$data);
				}else{
					$post = $this->input->post(NULL, TRUE);
					$cleanPost = $this->security->xss_clean($post);
					$hashed = md5($cleanPost['password']);
					$cleanPost ['password'] = $hashed;
					$cleanPost ['nip'] = $userInfo->nip;
					unset($cleanPost['passconf']);
					if(!$this->m_account->update_Password($cleanPost)){
						$this->session->set_flashdata('sukses','Password anda sudah diperbaharui. Silahkan login.');
					}
					redirect(site_url('login'),'refresh');
				}
	}
	public function base64url_encode($data)
	{
		return rtrim(strtr(base64url_encode($data),'+/','-_'),'=');
	}
	public function base64url_decode($data)
		{
		return base64_decode(str_pad(strtr($data,'-_','+/'), strlen($data) % 4,'=',STR_PAD_RIGHT));
		}
}
