<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->helper('url');
		$this->load->view('account/berandalog');
	}

	public function login()
	{
		$this->load->helper('url');
		$this->load->view('account/berandalog');
	
	}
	public function registrasi ()
	{
		$this->load->helper('url');
		$this->load->view('account/v_success');
	}

	 function cek_regis ()

	{
		$this->load->library(array('form_validation'));
		$this->load->model('m_account');
		$this->form_validation->set_rules('nip','nip','required');
		$this->form_validation->set_rules('fullname','FULLNAME','required');
		$this->form_validation->set_rules('jeniskelamin','jeniskelamin','required');
		$this->form_validation->set_rules('email','EMAIL','required|valid_email');
		$this->form_validation->set_rules('phone','PHONE','required');
		$this->form_validation->set_rules('jabatan','JABATAN','required');
		$this->form_validation->set_rules('Regional','REGIONAL','required');
		$this->form_validation->set_rules('Area','AREA','required');
		$this->form_validation->set_rules('username','USERNAME','required');
		$this->form_validation->set_rules('password','PASSWORD','required');



	if($this->form_validation->run() == FALSE)
	{
		
		echo validation_errors();
	}

	else
	{
		$data['nip']			= $this->input->post('nip');
		$data['fullname']		= $this->input->post('fullname');
		$data['jeniskelamin']	= $this->input->post('jeniskelamin');
		$data['email']			= $this->input->post('email');
		$data['phone']			= $this->input->post('phone');
		$data['jabatan']		= $this->input->post('jabatan');
		$data['Regional']		= $this->input->post('Regional');
		$data['Area']			= $this->input->post('Area');
		$data['username']		= $this->input->post('username');
		$data['password']		= $this->input->post('password');
		//$data['is_permited'] = 0;

		//var_dump($data['Regional'] =$this->input->post('Regional'));
		($this->m_account->daftar_login($data));
		$pesan ['message'] = "Pendaftaran berhasil";
		$this->load->view(('account/v_success'),$pesan);
	}
	function Wall()
	{
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('M_Data');
		$select['dash'] ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		$select['nas']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$select['Tar']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$DataOS['RekapSegmen'] =$this->M_Data->TargetSegmen()->result();
		$DataOS['RekapRegion'] =$this->M_Data->TargetRegional()->result();
		$DataOS['nilai']       =$this->M_Data->GrafikPorto()->result();
		$DataOS['nilaiW']      =$this->M_Data->GrafikPortoWholesale()->result();
		$DataOS['nilaiR']      =$this->M_Data->GrafikPortoRetail()->result();
		$DataOS['portoB']      =$this->M_Data->Portobulanan()->result();
		$DataOS['portoH']      =$this->M_Data->Portoharian()->result();
		$DataOS['header']	  =$this->load->view('templates/headerV2',null,true);
		$this->load->view('Wall',$DataOS);
	}
}
}
