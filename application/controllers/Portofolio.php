<?php

class Portofolio extends CI_Controller {

	
	public function index()
	{
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('M_Data');
		//$select['dash'] ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		//$select['nas']  ="dropdown dropdown-fw dropdown-fw-disabled";
		//$select['Tar']  ="dropdown dropdown-fw dropdown-fw-disabled";
		//$DataOS['RekapSegmen'] =$this->M_Data->TargetSegmen()->result();
		//$DataOS['RekapRegion'] =$this->M_Data->TargetRegional()->result();
		//$DataOS['nilai']       =$this->M_Data->GrafikPorto()->result();
		//$DataOS['nilaiW']      =$this->M_Data->GrafikPortoWholesale()->result();
		//$DataOS['nilaiR']      =$this->M_Data->GrafikPortoRetail()->result();
		//$DataOS['portoB']      =$this->M_Data->Portobulanan()->result();
		//$DataOS['portoH']      =$this->M_Data->Portoharian()->result();
		
		$currentDate = date("Y-m-d");
		$date = DateTime::createFromFormat('Y-m-d', $currentDate);
		$date->modify('-1 day');
		$previousDate =  $date->format('Y-m-d');

		$DataOS['PFAndNPF']  	= json_encode($this->M_Data->getPFAndNPF($previousDate),JSON_NUMERIC_CHECK);
		$DataOS['Bankwide']  	= json_encode($this->M_Data->getPortoBankwide($previousDate),JSON_NUMERIC_CHECK);
		$DataOS['Retail']  	    = json_encode($this->M_Data->getPortoRetail($previousDate),JSON_NUMERIC_CHECK);
		$DataOS['Regional']  	    = json_encode($this->M_Data->getPortoRegional($previousDate),JSON_NUMERIC_CHECK);
		$DataOS['UpgradeDowngrade'] = json_encode($this->M_Data->getUpgradeDowngrade($previousDate),JSON_NUMERIC_CHECK);
		$DataOS['header']	    =$this->load->view('templates/headerV2',null,true);
		$this->load->view('Front',$DataOS);
	}

	public function overview ()
	{
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('M_Data');
		$select['dash'] ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		$select['nas']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$select['Tar']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$DataOS['RekapSegmen'] =$this->M_Data->TargetSegmen()->result();
		$DataOS['RekapRegion'] =$this->M_Data->TargetRegional()->result();
		$DataOS['nilai']       =$this->M_Data->GrafikPorto()->result();
		$DataOS['nilaiW']      =$this->M_Data->GrafikPortoWholesale()->result();
		$DataOS['nilaiR']      =$this->M_Data->GrafikPortoRetail()->result();
		$DataOS['portoB']      =$this->M_Data->Portobulanan()->result();
		$DataOS['portoH']      =$this->M_Data->Portoharian()->result();
		$DataOS['header']	  =$this->load->view('templates/headerV2',null,true);
		$this->load->view('Overview',$DataOS);
		
	}
	public function PFNPF ()
	{
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('M_Data');

		$currentDate = date("Y-m-d");
		$date = DateTime::createFromFormat('Y-m-d', $currentDate);
		$date->modify('-1 day');
		$previousDate =  $date->format('Y-m-d');

		$DataOS['PFNPF']  	= json_encode($this->M_Data->getPFAndNPF($previousDate),JSON_NUMERIC_CHECK);
		$DataOS['header']	  	=$this->load->view('templates/headerV2',null,true);
		$this->load->view('Front',$DataOS);
	}
		public function Bankwide ()
	{
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('M_Data');

		$currentDate = date("Y-m-d");
		$date = DateTime::createFromFormat('Y-m-d', $currentDate);
		$date->modify('-1 day');
		$previousDate =  $date->format('Y-m-d');

		$DataOS['Bankwide']  	= json_encode($this->M_Data->getPortoBankwide($previousDate),JSON_NUMERIC_CHECK);
		$DataOS['header']	  	=$this->load->view('templates/headerV2',null,true);
		$this->load->view('Front',$DataOS);
	}
	public function Regional ()
	{
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('M_Data');

		$currentDate = date("Y-m-d");
		$date = DateTime::createFromFormat('Y-m-d', $currentDate);
		$date->modify('-1 day');
		$previousDate =  $date->format('Y-m-d');

		$DataOS['Bankwide']  	= json_encode($this->M_Data->getPortoRegional($previousDate),JSON_NUMERIC_CHECK);
		$DataOS['header']	  	=$this->load->view('templates/headerV2',null,true);
		$this->load->view('Front',$DataOS);
	}

	public function UpgradeDowngrade ()
	{
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('M_Data');
		$currentDate = (ENVIRONMENT !== 'production') ? "2018-01-12" : date("Y-m-d");
		$DataOS['UpgradeDowngrade']  	= json_encode($this->M_Data->getUpgradeDowngrade($currentDate),JSON_NUMERIC_CHECK);
		$DataOS['header']	  	=$this->load->view('templates/headerV2',null,true);
		$this->load->view('Front',$DataOS);
	} 	


		public function Retail ()
	{
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('M_Data');
		$currentDate = (ENVIRONMENT !== 'production') ? "2018-01-12" : date("Y-m-d");
		$DataOS['Retail']  	= json_encode($this->M_Data->getPortoRetail($currentDate),JSON_NUMERIC_CHECK);
		$DataOS['header']	  	=$this->load->view('templates/headerV2',null,true);
		$this->load->view('Front',$DataOS);
	}

	public function PortoKol2(){
		$this->uri->segment(2);
		$this->load->helper('url');
		$select['dash'] ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		$select['nas']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$select['Tar']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$header = $this->load->view('templates/headerV2',null,true);
		$this->load->view('PortoKol2');
		
	}
	public function PortoNPF()
	{
		$this->uri->segment(2);
		$this->load->helper('url');
		$select['dash'] ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		$select['nas']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$select['Tar']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$header = $this->load->view('templates/headerV2',null,true);
		$this->load->view('PortoNPF');
		
	}

	public function PortoUpgrade()
	{
		$this->uri->segment(2);
		$this->load->helper('url');
		$select['dash'] ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		$select['nas']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$select['Tar']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$header = $this->load->view('templates/headerV2',null,true);
		$this->load->view('PortoUpgrade');
		
	}
	public function PortoDowngrade()
	{
		$this->uri->segment(2);
		$this->load->helper('url');
		$select['dash'] ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		$select['nas']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$select['Tar']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$header = $this->load->view('templates/headerV2',null,true);
		$this->load->view('PortoDown');
		
	}
	public function PortoBooking()
	{
		$this->load->helper('url');
		$select['dash'] ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		$select['nas']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$select['Tar']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$header = $this->load->view('templates/headerV2',null,true);
		$this->load->view('Booking');
		
	}

}

