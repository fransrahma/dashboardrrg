<?php
Class Wall extends CI_Controller {

	
	public function index()
	{
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('M_Data');
		$select['dash'] ="dropdown dropdown-fw dropdown-fw-disabled active open selected";
		$select['nas']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$select['Tar']  ="dropdown dropdown-fw dropdown-fw-disabled";
		$DataOS['RekapSegmen'] =$this->M_Data->TargetSegmen()->result();
		$DataOS['RekapRegion'] =$this->M_Data->TargetRegional()->result();
		$DataOS['nilai']       =$this->M_Data->GrafikPorto()->result();
		$DataOS['nilaiW']      =$this->M_Data->GrafikPortoWholesale()->result();
		$DataOS['nilaiR']      =$this->M_Data->GrafikPortoRetail()->result();
		$DataOS['portoB']      =$this->M_Data->Portobulanan()->result();
		$DataOS['portoH']      =$this->M_Data->Portoharian()->result();
		$DataOS['header']	  =$this->load->view('templates/headerV2',null,true);
		
		$this->load->model('M_post');
		$DataOS['posting']   =$this->M_post->Manage_post()->result();
		$this->load->view('Wall',$DataOS);
	}
		function help()
	{
	
		$this->load->view('Help');
	}	
}