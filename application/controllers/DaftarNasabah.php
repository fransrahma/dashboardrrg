<?php

/**
 * @author Rahma Fitri
 * 
 * 
 */
class DaftarNasabah extends CI_Controller {

	function __construct(){
        parent::__construct();
		$this->load->model(array('M_Nasabah3Pilar','M_OjkConfiguration'));  
		$this->load->helper(array('form', 'url','tigapilar_helper'));
		$this->load->library(array('form_validation'));
    }
    
	public function Review3Pilar ()
	{

		$data['header'] = $this->load->view('templates/headerV2',null,true);
		$data['Data'] = $this->M_Nasabah3Pilar->getNasabah3Pilar(
			$this->session->userdata('regional'),
			$this->session->userdata('area')
		);

        $this->load->view('Nasabah3Pilar',$data);

		
	}

	public function Edit3Pilar ($noloan)
	{
		
		$loan = $this->M_Nasabah3Pilar->GetReviewRecord(urldecode($noloan));

		if( is_object($loan) ){
			echo "<script type='text/javascript'>alert('Review hanya bisa dilakukan satu kali!');</script>";
		}
		else{
			$data['header'] = $this->load->view('templates/headerV2',null,true);
			$data['Data'] = $this->M_Nasabah3Pilar->ReviewNasabah3Pilar(urldecode($noloan));
			$data['value_labels'] = $this->M_OjkConfiguration->get_value_labels();
			$data['factors_1'] = $this->M_OjkConfiguration->get_assesment_values($data['Data']->JenisPiutangPembiayaan,'1');
			$data['factors_2'] = $this->M_OjkConfiguration->get_assesment_values($data['Data']->JenisPiutangPembiayaan,'2');
			$data['factors_3'] = $this->M_OjkConfiguration->get_assesment_values($data['Data']->JenisPiutangPembiayaan,'3');
	
			$this->load->view('ReviewNasabah3Pilar',$data);
		}
		
	}

	private function build_validation_rules($scheme){

		$validation_rules = array();

		$factors1 = $this->M_OjkConfiguration->get_assesment_values(urldecode($scheme),'1');

		foreach ($factors1 as $key => $value) {
			$config = array(
				'field' => 'prospek_'.$value->scheme_id."_".$value->factor_id."_".$value->id,
				'label' => $value->factor_point,
				'rules' => 'required'
			);

			array_push($validation_rules, $config);
		}

		
		$factors2 = $this->M_OjkConfiguration->get_assesment_values(urldecode($scheme),'2');

		foreach ($factors2 as $key => $value) {
			$config = array(
				'field' => 'kinerja_'.$value->scheme_id."_".$value->factor_id."_".$value->id,
				'label' => $value->factor_point,
				'rules' => 'required'
			);

			array_push($validation_rules, $config);
		}
		$config = array(
			'field' => 'prospek_'.$value->scheme_id."_".$value->factor_id."_".$value->id,
			'label' => $value->factor_point,
			'rules' => 'required'
		);
		$factors3 = $this->M_OjkConfiguration->get_assesment_values(urldecode($scheme),'3');

		foreach ($factors3 as $key => $value) {
			$config = array(
				'field' => 'bayar_'.$value->scheme_id."_".$value->factor_id."_".$value->id,
				'label' => $value->factor_point,
				'rules' => 'required'
			);

			array_push($validation_rules, $config);
		}
		


		return $validation_rules;
	}

	public function proceed_review($scheme){

		$validation_rules = $this->build_validation_rules(urldecode($scheme));
		$this->form_validation->set_rules($validation_rules);

		$response = array();

		if( $this->form_validation->run() == FALSE ){
			$data['header'] = $this->load->view('templates/headerV2',null,true);
			$response = array('statusCode'=>'99','message'=>validation_errors());
		}
		else{
			
			$factors1 = $this->M_OjkConfiguration->get_assesment_values(urldecode($scheme),'1');
			$factors2 = $this->M_OjkConfiguration->get_assesment_values(urldecode($scheme),'2');
			$factors3 = $this->M_OjkConfiguration->get_assesment_values(urldecode($scheme),'3');
			$prefix = "prospek_";
			$delim="_";
			
			$prospek_values = array();
			$kinerja_values = array();
			$kemampuanBayar_values = array();

			$prospek_notes = array();
			$kinerja_notes = array();
			$kemampuanBayar_notes = array();

			$no_loan = $this->input->post('NoLoan');
			

			foreach ($factors1 as $key => $value) {
				$var_name = $prefix.$value->scheme_id.$delim.$value->factor_id.$delim.$value->id;
				$$var_name = $this->input->post($var_name);

				$notes_var_name = "note".$prefix.$value->scheme_id.$delim.$value->factor_id.$delim.$value->id;
				$$notes_var_name = $this->input->post($notes_var_name);

				array_push($prospek_values,$$var_name);
				array_push($prospek_notes,['loanid'=>$no_loan,'key'=>$$var_name,'notes'=>$$notes_var_name]);
			}

			$prefix = "kinerja_";
			foreach ($factors2 as $key => $value) {
				$var_name = $prefix.$value->scheme_id.$delim.$value->factor_id.$delim.$value->id;
				$$var_name = $this->input->post($var_name);

				$notes_var_name = "note".$prefix.$value->scheme_id.$delim.$value->factor_id.$delim.$value->id;
				$$notes_var_name = $this->input->post($notes_var_name);

				array_push($kinerja_values,$$var_name);
				array_push($kinerja_notes,['loanid'=>$no_loan,'key'=>$$var_name,'notes'=>$$notes_var_name]);
			}
			

			$prefix = "bayar_";
			foreach ($factors3 as $key => $value) {
				$var_name = $prefix.$value->scheme_id.$delim.$value->factor_id.$delim.$value->id;
				$$var_name = $this->input->post($var_name);

				$notes_var_name = "note".$prefix.$value->scheme_id.$delim.$value->factor_id.$delim.$value->id;
				$$notes_var_name = $this->input->post($notes_var_name);

				array_push($kemampuanBayar_values,$$var_name);
				array_push($kemampuanBayar_notes,['loanid'=>$no_loan,'key'=>$$var_name,'notes'=>$$notes_var_name]);
			}
			

			
			$nama = $this->input->post('NamaLengkap');
			$prospek_string_val = implode("#",$prospek_values);
			$kinerja_string_val = implode("#",$kinerja_values);
			$bayar_string_val = implode("#",$kemampuanBayar_values);
			
			//$result = parse_review_value($prospek_values,array(),array());
			$result = parse_review_value($prospek_values,$kinerja_values,$kemampuanBayar_values);

			//$this->M_OjkConfiguration->insert_review($no_loan,$prospek_string_val,'#','#');
			$this->M_OjkConfiguration->insert_review($no_loan,$prospek_string_val,$kinerja_string_val,$bayar_string_val);
			$this->M_OjkConfiguration->insert_reviewnotes($no_loan,$prospek_notes,$kinerja_notes,$kemampuanBayar_notes);


			$hasil = "Penilaian selesai. Hasil nya adalah Kol.".$result;
			
			$response = array('statusCode'=>'00','message'=>$hasil);

		}


		header('Content-Type: application/json');
		echo json_encode($response);

	}


	public function getReviewSummary($loanid){

		$loan = $this->M_Nasabah3Pilar->GetReviewRecord(urldecode($loanid));

		if( is_array($loan) ){
			header('Content-Type: application/json');
			echo json_encode(array('statusCode'=>'99','message'=>'not yet reviewed'));
			exit(0);
		}
		else{

			$dataNasabah = $this->M_Nasabah3Pilar->ReviewNasabah3Pilar(urldecode($loanid));
			$factor1 = $this->M_OjkConfiguration->get_factor_scheme($dataNasabah->JenisPiutangPembiayaan,'1');
			$factor2 = $this->M_OjkConfiguration->get_factor_scheme($dataNasabah->JenisPiutangPembiayaan,'2');
			$factor3 = $this->M_OjkConfiguration->get_factor_scheme($dataNasabah->JenisPiutangPembiayaan,'3');
			$nilaireview = $this->M_Nasabah3Pilar->GetReviewSummary(urldecode($loanid));
			$summary = $this->M_Nasabah3Pilar->getKolValue(urldecode($loanid));

			$ojkfactors = compact('factor1','factor2','factor3','nilaireview','summary');

			$view = $this->load->view("detailhasilreview",$ojkfactors,true);	


			header('Content-Type: application/json');
			echo json_encode(array('statusCode'=>'00','message'=>$view));
		
		}

		
	}

}
