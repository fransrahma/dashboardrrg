<?php

class Downloads extends CI_Controller {

    
    function __construct(){
        parent::__construct();
        $this->load->model('M_Report');    
    }
    
    public function index()
    {
        
        $data['header'] = $this->load->view('templates/headerV2',null,true);
        $data['items'] = $this->M_Report->file_list();
        $this->load->view('Downloads',$data);
        
    }


        

}