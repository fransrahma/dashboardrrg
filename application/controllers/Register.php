<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Register extends CI_Controller
{
	public function index()
	{
		$this->load->view('Wall');
	}
	function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->model('m_account');
	}
}
	function cek_regis ()
{
	$this->form_validation->set_rules('id','NIP','required');
	$this->form_validation->set_rules('fullname','FULLNAME','required');
	$this->form_validation->set_rules('email','EMAIL','required|valid_email');
	$this->form_validation->set_rules('phone','PHONE','required');
	$this->form_validation->set_rules('username','USERNAME','required');
	$this->form_validation->set_rules('password','PASSWORD','required');

	if($this->form_validation->run() == FALSE)
	{
		var_dump($this->load->view('login'));

	}
	else
	{
		$data['NIP'] = $this->input->post('id');
		$data['FULLNAME'] = $this->input->post('fullname');
		$data['EMAIL'] = $this->input->post('email');
		$data['PHONE'] = $this->input->post('phone');
		$data['username'] = $this->input->post('username');
		$data['password'] = $this->input->post('password');

		$this->m_account->daftar_login($data);
		$pesan ['message'] = "Pendaftaran berhasil";
		$this->load->view(('account/v_success'),$pesan);
	}
}

