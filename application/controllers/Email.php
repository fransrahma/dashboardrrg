<?php 
class Email extends CI_Controller
{
	function __construct(){
		parent::__construct();
		if ($this->session->userdata('logged')<> 1){
			redirect (site_url('auth'));
		}
	}