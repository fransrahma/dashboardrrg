<?php

class acl {

    private $allowed_maps = array(
        array(
            'class' => 'wall',
            'method' => 'index'
        ),
         array(
            'class' => 'wall',
            'method' => 'help'
        ),
        array(
            'class' => 'welcome',
            'method' => 'index'
        ),
        array(
            'class' => 'login',
            'method' => 'index'
        ),
        array(
            'class' => 'login',
            'method' => 'aksi_login'
        ),
        array(
            'class' => 'login',
            'method' => 'logout'
        ),
        array(
            'class' => 'welcome',
            'method' => 'cek_regis'
        )
    );

    public function __construct() {

        //$this->group_field = md5('group_id');
    }

    public function auth() {
		
        $CI = &get_instance();

        
        $current_page = strtolower(implode("/", array(
            $CI->uri->rsegment(1, 'welcome'),
            $CI->uri->rsegment(2, 'index')
        )));

        //add uri to bypass here
        $bypass = array();

        if (in_array($current_page, $bypass)) {
            return;
        }

        if (!isset($CI->session)) {
            $CI->load->library('session');
        }

        if (!isset($CI->router)) {
            $CI->load->library('router');
        }

        $class = $CI->router->fetch_class();
        $method = $CI->router->fetch_method();

        $is_allowed_to_all = false;

        foreach ($this->allowed_maps as $rule) {
            if ($rule['class'] == $class && $rule['method'] == $method) {
                $is_allowed_to_all = true;
            }
        }

        if ($is_allowed_to_all == FALSE) {
            if ($CI->session->has_userdata("nama") == FALSE) {#if user is logged in
                //die('aku ada disini x');
                redirect('login');
                //if (isset($CI->acl) == FALSE) {
                //    $CI->load->model('acl_model', 'acl');
                //}

                //$is_allowed = $CI->acl->getPerms($CI->session->userdata($this->group_field), $class, $method);

                //if ($is_allowed == FALSE) {
                //    show_error("You're not allowed to access this page", 403);
                //}
            } else {
                //die('aku ada disini');
                return;
            }
        } else {
            if ($CI->session->has_userdata('name')) {
                if ($class == "login") {
                    if ($method == "aksi_login" || $method == "index") {
                        redirect('');
                    }
                }
            }
            else
            {
                return true;
            }
        }
    }

}
