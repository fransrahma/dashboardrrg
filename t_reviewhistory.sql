CREATE TABLE RetailRiskGroup.dbo.t_reviewhistory (
	id int NOT NULL IDENTITY(1,1),
	LoanId varchar(100),
	Prospek_value varchar(100),
	Kinerja_value varchar(100),
	KemampuanBayar_value varchar(100),
	add_time DATETIME,
	update_time DATETIME
)