CREATE TABLE RetailRiskGroup.dbo.t_report_files (
	id int NOT NULL IDENTITY(1,1),
	filename varchar(200) NOT NULL,
	[path] text,
	[desc] varchar(100),
	tipe tinyint NOT NULL,
	upload_time datetime2 DEFAULT (getdate()) NOT NULL
) go;
