USE [RetailRiskGroup]
GO

/****** Object:  View [dbo].[V_Nasabah3Pilar]    Script Date: 09/04/2018 09:57:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











ALTER VIEW [dbo].[V_Nasabah3Pilar]
AS
select *
from (

/****** Script for SelectTopNRows command from SSMS  ******/
select [FICMISDATE]
,NoLoan
,NomorCIF
,NamaLengkap
,Divisi
,[Nama Cabang di NWG] NamaCabang
,Area
,Regional
,JenisPiutangPembiayaan  
,sum([OSPOKOKCONVERSION]) OS
,KolLoan

from [RetailRiskGroup].[dbo].[Database 3 pilar 31-05-18]  
WHERE Divisi IN ('BBG-1','BBG-2','MBG') AND [Pencairan Flag Limit]='>Rp 10 M'
GROUP  BY FicMisDate, NoLoan,NomorCIF,NamaLengkap,Divisi,[Nama Cabang di NWG],Area,Regional,JenisPiutangPembiayaan,KolLoan

union all

select [FICMISDATE]
,NoLoan
,NomorCIF
,NamaLengkap
,Divisi
,[Nama Cabang di NWG]
,Area
,Regional
,JenisPiutangPembiayaan  
,sum([OSPOKOKCONVERSION]) OS
,KolLoan

from [RetailRiskGroup].[dbo].[Database 3 pilar 31-05-18]  
WHERE Divisi IN ('CFG-C','CFG-H','PWG') AND [Pencairan Flag Limit]='>Rp 1 M'
GROUP  BY FicMisDate, NoLoan,NomorCIF,NamaLengkap,Divisi,[Nama Cabang di NWG],Area,Regional,JenisPiutangPembiayaan,KolLoan



/****** Script for SelectTopNRows command from SSMS  ******/

) x










GO


