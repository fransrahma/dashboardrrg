USE [RetailRiskGroup]
GO

/****** Object:  Table [dbo].[Database 3 pilar 31-05-18]    Script Date: 07/05/2018 09:05:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Database 3 pilar 31-05-18]') AND type in (N'U'))
DROP TABLE [dbo].[Database 3 pilar 31-05-18]
GO

USE [RetailRiskGroup]
GO

/****** Object:  Table [dbo].[Database 3 pilar 31-05-18]    Script Date: 07/05/2018 09:05:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Database 3 pilar 31-05-18](
	[FicMisDate] [datetime] NULL,
	[NoLoan] [varchar](50) NULL,
	[NomorCIF] [varchar](50) NULL,
	[NamaLengkap] [varchar](250) NULL,
	[KodeCabangBaru] [varchar](50) NULL,
	[NamaCabang] [varchar](50) NULL,
	[Nama Cabang di NWG] [varchar](250) NULL,
	[Area] [varchar](250) NULL,
	[Regional] [varchar](250) NULL,
	[JenisPiutangPembiayaan] [varchar](50) NULL,
	[Data Restru WFR] [varchar](250) NULL,
	[Periode Restru WFR] [varchar](250) NULL,
	[Sektor Ekonomi] [varchar](250) NULL,
	[Kelompok Sektor Ekonomi (LKMS)] [varchar](250) NULL,
	[TgllPencairan] [datetime] NULL,
	[TglJtTempo] [datetime] NULL,
	[DayPastDue] [int] NULL,
	[Divisi] [varchar](50) NULL,
	[Segmentasi] [nvarchar](255) NULL,
	[Produk] [nvarchar](255) NULL,
	[Pekerjaan] [varchar](250) NULL,
	[Jenis Pekerjaan] [varchar](250) NULL,
	[LoanType] [varchar](50) NULL,
	[Tenor] [varchar](50) NULL,
	[Jangka Waktu Pembiayaan] [varchar](50) NULL,
	[RestructFlag] [varchar](50) NULL,
	[RestructDate] [datetime] NULL,
	[KolCIF 31-12-17] [varchar](250) NULL,
	[KolCIF 30-04-18] [varchar](250) NULL,
	[KolLoan] [varchar](50) NULL,
	[KolCIF] [varchar](50) NULL,
	[Penyebab] [varchar](250) NULL,
	[Penyebab_Breakdown] [varchar](250) NULL,
	[Penyebab CIF Divisi] [varchar](250) NULL,
	[Keterangan Mutasi] [varchar](250) NULL,
	[OSPokokConversion] [decimal](28, 4) NULL,
	[OSMarginConversion] [decimal](28, 4) NULL,
	[OSGrossConversion] [decimal](28, 4) NULL,
	[Selisih Pokok Akhir Bulan] [decimal](28, 4) NULL,
	[Penambahan OS] [decimal](28, 4) NULL,
	[OSPokokConversion PSAK] [decimal](28, 4) NULL,
	[Nominal Accrue] [decimal](28, 4) NULL,
	[Selisih Pokok Akhir Bulan PSAK] [decimal](28, 4) NULL,
	[Penambahan OS PSAK] [decimal](28, 4) NULL,
	[TunggakanPokokConversion] [decimal](28, 4) NULL,
	[TunggakanMarginConversion] [decimal](28, 4) NULL,
	[TunggakanGrossConversion] [decimal](28, 4) NULL,
	[Pencairan Flag Limit] [varchar](250) NULL,
	[Pencairan Flag] [decimal](28, 4) NULL,
	[PencairanPokokConversion] [decimal](28, 4) NULL,
	[PencairanMarginConversion] [decimal](28, 4) NULL,
	[PencairanGrossConversion] [decimal](28, 4) NULL,
	[Realisasi_BagiHasil] [decimal](28, 4) NULL,
	[Proyeksi_BagiHasil] [decimal](28, 4) NULL,
	[Akumulasi_RBH_PBH] [decimal](28, 4) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


