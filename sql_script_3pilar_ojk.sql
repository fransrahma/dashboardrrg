CREATE TABLE RetailRiskGroup.dbo.t_factorpoint2values (
	id tinyint NOT NULL IDENTITY(1,1),
	factorpoint_id tinyint,
	label varchar(500),
	value varchar(100),
	idx tinyint
) go
CREATE INDEX t_factorpoint2value_order_IDX ON RetailRiskGroup.dbo.t_factorpoint2values (idx) go;

CREATE TABLE RetailRiskGroup.dbo.t_ojk_factors (
	id tinyint NOT NULL IDENTITY(1,1),
	factor_name varchar(200)
) go;

CREATE TABLE RetailRiskGroup.dbo.t_ojk_labels2value (
	id tinyint NOT NULL IDENTITY(1,1),
	label varchar(100),
	value tinyint DEFAULT ((1)),
	alias varchar(100)
) go;

CREATE TABLE RetailRiskGroup.dbo.t_ojk_scheme (
	scheme_id tinyint NOT NULL IDENTITY(1,1),
	scheme_name varchar(100),
	scheme_to_jenispiutang varchar(250)
) go;

CREATE TABLE RetailRiskGroup.dbo.t_scheme2factorpoint (
	id tinyint NOT NULL IDENTITY(1,1),
	scheme_id tinyint,
	factor_id tinyint,
	factor_point varchar(500),
	[desc] varchar(100),
	factor_parent tinyint
) go
CREATE INDEX t_scheme2factorpoint_id_IDX ON RetailRiskGroup.dbo.t_scheme2factorpoint (scheme_id,factor_id,id,factor_point) go;
