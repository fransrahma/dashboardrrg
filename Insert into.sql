INSERT INTO [RetailRiskGroup].[dbo].[Database 3 pilar 31-05-18]
           ([FicMisDate]
           ,[NoLoan]
           ,[NomorCIF]
           ,[NamaLengkap]
           ,[KodeCabangBaru]
           ,[NamaCabang]
           ,[Nama Cabang di NWG]
           ,[Area]
           ,[Regional]
           ,[JenisPiutangPembiayaan]
           ,[Data Restru WFR]
           ,[Periode Restru WFR]
           ,[Sektor Ekonomi]
           ,[Kelompok Sektor Ekonomi (LKMS)]
           ,[TgllPencairan]
           ,[TglJtTempo]
           ,[DayPastDue]
           ,[Divisi]
           ,[Segmentasi]
           ,[Produk]
           ,[Pekerjaan]
           ,[Jenis Pekerjaan]
           ,[LoanType]
           ,[Tenor]
           ,[Jangka Waktu Pembiayaan]
           ,[RestructFlag]
           ,[RestructDate]
           ,[KolCIF 31-12-17]
           ,[KolCIF 30-04-18]
           ,[KolLoan]
           ,[KolCIF]
           ,[Penyebab]
           ,[Penyebab_Breakdown]
           ,[Penyebab CIF Divisi]
           ,[Keterangan Mutasi]
           ,[OSPokokConversion]
           ,[OSMarginConversion]
           ,[OSGrossConversion]
           ,[Selisih Pokok Akhir Bulan]
           ,[Penambahan OS]
           ,[OSPokokConversion PSAK]
           ,[Nominal Accrue]
           ,[Selisih Pokok Akhir Bulan PSAK]
           ,[Penambahan OS PSAK]
           ,[TunggakanPokokConversion]
           ,[TunggakanMarginConversion]
           ,[TunggakanGrossConversion]
           ,[Pencairan Flag Limit]
           ,[Pencairan Flag]
           ,[PencairanPokokConversion]
           ,[PencairanMarginConversion]
           ,[PencairanGrossConversion]
           ,[Realisasi_BagiHasil]
           ,[Proyeksi_BagiHasil]
           ,[Akumulasi_RBH_PBH])
     VALUES
           (<FicMisDate, datetime,>
           ,<NoLoan, varchar(50),>
           ,<NomorCIF, varchar(50),>
           ,<NamaLengkap, varchar(250),>
           ,<KodeCabangBaru, varchar(50),>
           ,<NamaCabang, varchar(50),>
           ,<Nama Cabang di NWG, varchar(250),>
           ,<Area, varchar(250),>
           ,<Regional, varchar(250),>
           ,<JenisPiutangPembiayaan, varchar(50),>
           ,<Data Restru WFR, varchar(250),>
           ,<Periode Restru WFR, varchar(250),>
           ,<Sektor Ekonomi, varchar(250),>
           ,<Kelompok Sektor Ekonomi (LKMS), varchar(250),>
           ,<TgllPencairan, datetime,>
           ,<TglJtTempo, datetime,>
           ,<DayPastDue, int,>
           ,<Divisi, varchar(50),>
           ,<Segmentasi, nvarchar(255),>
           ,<Produk, nvarchar(255),>
           ,<Pekerjaan, varchar(250),>
           ,<Jenis Pekerjaan, varchar(250),>
           ,<LoanType, varchar(50),>
           ,<Tenor, varchar(50),>
           ,<Jangka Waktu Pembiayaan, varchar(50),>
           ,<RestructFlag, varchar(50),>
           ,<RestructDate, datetime,>
           ,<KolCIF 31-12-17, varchar(250),>
           ,<KolCIF 30-04-18, varchar(250),>
           ,<KolLoan, varchar(50),>
           ,<KolCIF, varchar(50),>
           ,<Penyebab, varchar(250),>
           ,<Penyebab_Breakdown, varchar(250),>
           ,<Penyebab CIF Divisi, varchar(250),>
           ,<Keterangan Mutasi, varchar(250),>
           ,<OSPokokConversion, decimal(28,4),>
           ,<OSMarginConversion, decimal(28,4),>
           ,<OSGrossConversion, decimal(28,4),>
           ,<Selisih Pokok Akhir Bulan, decimal(28,4),>
           ,<Penambahan OS, decimal(28,4),>
           ,<OSPokokConversion PSAK, decimal(28,4),>
           ,<Nominal Accrue, decimal(28,4),>
           ,<Selisih Pokok Akhir Bulan PSAK, decimal(28,4),>
           ,<Penambahan OS PSAK, decimal(28,4),>
           ,<TunggakanPokokConversion, decimal(28,4),>
           ,<TunggakanMarginConversion, decimal(28,4),>
           ,<TunggakanGrossConversion, decimal(28,4),>
           ,<Pencairan Flag Limit, varchar(250),>
           ,<Pencairan Flag, decimal(28,4),>
           ,<PencairanPokokConversion, decimal(28,4),>
           ,<PencairanMarginConversion, decimal(28,4),>
           ,<PencairanGrossConversion, decimal(28,4),>
           ,<Realisasi_BagiHasil, decimal(28,4),>
           ,<Proyeksi_BagiHasil, decimal(28,4),>
           ,<Akumulasi_RBH_PBH, decimal(28,4),>)
GO


