﻿INSERT INTO RetailRiskGroup.dbo.t_scheme2factorpoint (scheme_id,factor_id,factor_point,[desc],factor_parent) VALUES 
(1,1,'Potensi Pertumbuhan Usaha',NULL,NULL)
,(1,1,'Kondisi pasar dan posisi nasabah dalam persaingan',NULL,NULL)
,(1,1,'Kualitas manajemen (independensi, pengalaman, serta kompetensi) dan permasalahan tenaga kerja',NULL,NULL)
,(1,1,'Dukungan dari grup atau afiliasi',NULL,NULL)
,(1,1,'Upaya yang dilakukan nasabah dalam rangka memelihara lingkungan hidup (sesuai dengan peraturan perundang-undangan yang berlaku).',NULL,NULL)
,(1,2,'Perolehan laba',NULL,NULL)
,(1,2,'Struktur permodalan',NULL,NULL)
,(1,2,'Arus kas',NULL,NULL)
,(1,2,'Sensitivitas terhadap risiko pasar',NULL,NULL)
,(1,3,'Terdapat pembayaran angsuran pokok',NULL,12)
;
INSERT INTO RetailRiskGroup.dbo.t_scheme2factorpoint (scheme_id,factor_id,factor_point,[desc],factor_parent) VALUES 
(1,3,'Tidak terdapat pembayaran angsuran pokok',NULL,12)
,(1,3,'Ketepatan pembayaran pokok dan bagi hasil',NULL,NULL)
,(1,3,'Ketersediaan dan keakuratan inforamsi keuangan nasabah',NULL,NULL)
,(1,3,'Kelengkapan dokumen pembiayaan',NULL,NULL)
,(1,3,'Kepatuhan terhadap perjanjian pembiayaan',NULL,NULL)
,(1,3,'Kesesuaian penggunaan fasilitas',NULL,NULL)
,(1,3,'Kewajaran sumber pembayaran kewajiban',NULL,NULL)
;