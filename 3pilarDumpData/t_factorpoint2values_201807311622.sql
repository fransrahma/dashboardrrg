INSERT INTO RetailRiskGroup.dbo.t_factorpoint2values (factorpoint_id,label,value,idx) VALUES 
(1,'Potensi pertumbuhan kegiatan usaha nasabah baik.','1',1)
,(1,'Potensi pertumbuhan kegiatan usaha nasabah terbatas','2',1)
,(1,'Potensi pertumbuhan kegiatan usaha nasabah sangat terbatas atau tidak mengalami pertumbuhan.','3',1)
,(1,'Kegiatan usaha nasabah menurun.','4',1)
,(1,'Kelangsungan usaha nasabah sangat diragukan dan sulit untuk pulih kembali/','5',1)
,(1,'Kemungkinan besar kegiatan usaha akan terhenti','5',2)
,(2,'Pasar yang stabil dan tidak dipengaruhi oleh perubahan kondisi perekonomian.','1',1)
,(2,'Posisi di pasar baik, tidak banyak dipengaruhi oleh perubahan kondisi perekonomian.','2',1)
,(2,'Pasar dipengaruhi oleh perubahan kondisi perekonomian.','3',1)
,(2,'Pasar sangat dipengaruhi oleh perubahan kondisi perekonomian.','4',1)
;
INSERT INTO RetailRiskGroup.dbo.t_factorpoint2values (factorpoint_id,label,value,idx) VALUES 
(2,'Kehilangan pasar sejalan dengan kondisi perekonomian yang menurun.','5',1)
,(2,'Persaingan yang terbatas, termasuk posisi yang kuat dalam pasar','1',2)
,(2,'Pangsa pasar sebanding dengan pesaing.','2',2)
;